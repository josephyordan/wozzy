<div class="row">
    <div class="form-group col-sm-6">
        <label class="control-label">From</label>
        <div class='input-group date breaks breakFrom'>
            <input type='text' class="form-control" name="break_from[]" value="{{ orHelper($starts_at, '') }}" required/>
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
        </div>
    </div>
    <div class="form-group col-sm-6">
        <label class="control-label">To</label>
        <div class='input-group date breaks breakTo'>
            <input type='text' class="form-control" name="break_to[]" value="{{ orHelper($ends_at, '') }}" required/>
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
        </div>
    </div>
</div>