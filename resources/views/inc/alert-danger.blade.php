@if (count($errors) > 0)
    <div class="alert alert-danger" style="margin-top: -18px;">
        <div class="container">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <p><strong>Oh Snap!</strong> Please fix the following errors:</p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif