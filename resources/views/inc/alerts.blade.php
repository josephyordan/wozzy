@foreach (Alert::getMessages() as $type => $messages)
    <div style="margin-top: -18px;">
        @foreach ($messages as $message)
            <div class="alert alert-{{ $type }}">
                <div class="container">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    {{ $message }}
                </div>
            </div>
        @endforeach
    </div>
@endforeach

@if (session('status'))
    <div class="alert alert-success" style="margin-top: -18px;">
        <div class="container">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            {{ session('status') }}
        </div>
    </div>
@endif