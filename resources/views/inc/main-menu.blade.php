<ul class="nav navbar-nav navbar-left">
    <li><a href="{{ route('home') }}">Home</a></li>
    <li><a href="{{ route('app.businesses') }}">{{ Config::get('settings.business_text') }}</a></li>
</ul>