@role('merchant')
<a href="{{ route('settings.business') }}" class="list-group-item">{{ Config::get('settings.business_text') }}</a>
<a href="{{ route('settings.appointments') }}" class="list-group-item">Appointments</a>
<a href="{{ route('settings.schedules') }}" class="list-group-item">Schedules</a>
<a href="{{ route('settings.services') }}" class="list-group-item">Services</a>
<a href="{{ route('settings.portfolio') }}" class="list-group-item">Portfolio</a>
<a href="{{ route('settings.leaves') }}" class="list-group-item">Leaves</a>
@if(Auth::check())
    @can('owner', Auth::user()->getBusiness())
        <a href="{{ route('settings.subscribe') }}" class="list-group-item">Subscription</a>
    @endcan
    @can('teamOwner', Auth::user()->getBusiness())
        <a href="{{ route('settings.members') }}" class="list-group-item">Members</a>
    @endcan
@endif
@endrole