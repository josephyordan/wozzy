<ul class="nav navbar-nav navbar-right">
    @if (Auth::guest())
        <li>
            <a href="{{ route('login') }}"><i class="fa fa-lock"></i> <span class="hidden-md">Sign In</span></a>
        </li>
        <li>
            <a href="{{ route('register') }}"><i class="fa fa-user"></i> <span class="hidden-md">Sign Up</span></a>
        </li>
    @else

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                @if(Gate::check('admin') || Gate::check('superadmin'))
                    <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
                @endif
                @role('merchant')
                <li><a href="{{ route('app.business', Auth::user()->getBusiness()->id) }}"><i class="fa fa-tv"></i> <span>My {{ Config::get('settings.business_text') }}</span></a></li>
                @endrole
                @if(count(Auth::user()->appointments))
                    <li>
                        <a href="{{ route('app.appointments') }}"><i class="fa fa-envelope-o"></i> <span class="hidden-md">Appointments</span></a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('settings.user') }}"><i class="fa fa-user"></i> <span class="hidden-md">Settings</span></a>
                </li>
                @role('merchant')
                    <li>
                        <a href="{{ route('settings') }}"><i class="fa fa-gear"></i> <span class="hidden-md">{{ Config::get('settings.business_text') }} Settings</span></a>
                    </li>
                @endrole
                <li>
                    <a href="{{ route('logout2') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-btn fa-sign-out"></i>
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}"
                          method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif
</ul>