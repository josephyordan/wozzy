@if ($errors->has($error))
    <p class="text-danger">
        <strong>{{ $errors->first($error) }}</strong>
    </p>
@endif