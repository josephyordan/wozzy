<div class="page-topbar">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">

                <h3>@yield('title')</h3>

            </div>
            <div class="col-sm-8 hidden-xs">
                <ol class="breadcrumb">
                    @yield('breadcrumb')
                </ol>
                {{-- Sample --}}
                {{--<ol class="breadcrumb">--}}
                    {{--<li><a href="#">Home</a></li>--}}
                    {{--<li class="active">Sign In</li>--}}
                {{--</ol>--}}
            </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
</div>