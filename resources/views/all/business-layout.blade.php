@extends('layouts.client-page')

@section('title', 'Business')

@section('breadcrumb')
    <li><a href="{{ route('app.businesses') }}">Businesses</a></li>
    <li class="active">Business</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="profile__aside">

                    <div class="profile__img">
                        <a href="{{ $business->picture->url() }}" data-lightbox="businessProfile{{ $business->id }}">
                            <img src="{{ $business->picture->url() }}" class="img-responsive profilePicture" alt="...">
                        </a>
                    </div>

                    <h4 class="profile__name">{{ $business->name ?: 'Business Name' }}</h4>

                    <p class="lead"><i class="fa fa-location-arrow"></i> Location</p>
                    @if($business->hasAddress())
                        <p class="text-muted">{{ implode(', ', $business->getPrimaryAddress()->address->toArray()) }}</p>
                    @endif

                    <div class="lead"><i class="fa fa-calendar"></i> Schedules</div>
                    @foreach($business->schedules as $schedule)
                        <div class="form-group">
                            <div class="text-muted">
                                {{ implode(', ', getDayByIndex($schedule->days)) }}
                            </div>
                            {{ $schedule->starts_at }} - {{ $schedule->ends_at }} {{ \Carbon\Carbon::now($business->timezone)->format('T') }}
                        </div>
                    @endforeach

                    <br>
                    <br>

                    <div class="clearfix">
                        <ul class="blog__tags">
                            @foreach($business->categories as $category)
                                <li><a href="{{ route('app.search', ['category_id' => $category->id]) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <br>

                    <ul class="social-icons social-icons_sm">
                        <li class="facebook">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="google-plus">
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </li>
                    </ul>

                    <hr/>

                    @if(Auth::guest() || \Gate::allows('appointment', $business))
                        <p ng-clock>
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment_form">
                                <i class="fa fa-envelope-o"></i> Book an Appointment
                            </a>
                        </p>
                    @endif

                </div> <!-- / .profile__aside -->
            </div>
            <div class="col-sm-9">

                <!-- Profile nav -->
                <nav class="clearfix">
                    <ul class="profile__nav">
                        @can('edit', $business)
                            <li {{ route('app.business', $business->id) == url()->current() ? 'class=active' : '' }}>
                                <a href="{{ route('app.business', $business->id) }}">
                                    <i class="fa fa-user"></i> Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('settings.business.edit') }}">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                            </li>
                        @endcan
                        {{--@if(Auth::guest() || \Gate::allows('appointment', $business))--}}
                        {{--<li {{ route('app.business.appointment', $business->id) == url()->current() ? 'class=active' : '' }}>--}}
                        {{--<a href="{{ route('app.business.appointment', $business->id) }}">--}}
                        {{--<i class="fa fa-envelope-o"></i> Book--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--@endif--}}
                    </ul>
                </nav>

                <div class="profile__body">
                    @yield('profile-body')
                </div> <!-- / .profile__body -->

            </div>
        </div> <!-- / .row -->
    </div>
@endsection

@section('angular')
    <script>
        var app = angular.module("app", [
            'angularMoment', 'ui.sortable', 'angular.filter', 'ui.bootstrap.datetimepicker'
        ]);
        app.controller("controller", function ($scope, $http, $filter, BusinessSchedule) {
            var __construct = function () {
                $scope.timezone = '{!! $business->timezone !!}';
                $scope.offers = {!! $business->getJSOffers()->toJson() !!};
                $scope.services = [];
                $scope.daySchedules = {};
                $scope.startDate = moment().tz($scope.timezone).startOf('day');
                $scope.updateScheduleTime();
                $scope.loading = 0;
            };

            var httpUpdateScheduleDay = function (response) {
                $scope.daySchedules = response.data;
                $scope.updateScheduleTime();
            };

            var httpFinalFunction = function () {
                $scope.loading--;
            };

            var displayLoading = function () {
                $scope.loading++;
                $scope.selectedTime = null;
            };

            $scope.startDateBeforeRender = function ($dates) {
                const todaySinceMidnight = moment().startOf('day').add(-1, 'days');
                $dates.filter(function (date) {
                    return date.utcDateValue < todaySinceMidnight.valueOf();
                }).forEach(function (date) {
                    date.selectable = false;
                });
            };

            $scope.getDaySchedule = function () {
                var startDate = moment($scope.startDate);
                $scope.timeSchedules = [];
                displayLoading();
                BusinessSchedule({
                    date: startDate.format('YYYY-MM-DD'),
                    type: 'day'
                })
                    .then(httpUpdateScheduleDay)
                    .finally(httpFinalFunction);
            };

            $scope.selectTime = function (time) {
                $scope.selectedTime = time;
            };

            $scope.addService = function (item) {
                $scope.services.push(item);
            };

            $scope.subService = function (index) {
                $scope.services.splice(index, 1);
            };

            $scope.updateScheduleTime = function () {
                $scope.selectedTime = null;
                $scope.timeSchedules = $filter('availableTime')(
                    $scope.daySchedules.times || [], $scope.services
                );
            };

            $scope.updateServices = function () {
                $scope.timeSchedules = $filter('availableTime')(
                    $scope.daySchedules.times || [], $scope.services
                );
            };

            $scope.submitAppointment = function () {
                $('#appointmentFormContent').html('');
                var inputs = [];
                var inputObject = {
                    type: 'text',
                    name: '',
                    value: ''
                };

                inputs.push($('<input>', $.extend(true, inputObject, {
                    name: 'appointment_at',
                    value: $scope.selectedTime
                })));

                angular.forEach($scope.services, function (service) {
                    inputs.push($('<input>', $.extend(true, inputObject, {
                        name: 'offers[]',
                        value: service.id
                    })));
                });

                $.each(inputs, function (key, input) {
                    $('#appointmentFormContent').append(input);
                });

                $('#appointmentForm').submit();
            };

            $scope.$watch('[services, daySchedules]', function () {
                $scope.updateScheduleTime();
            }, true);

            $scope.$watch('[startDate]', function () {
                $scope.getDaySchedule();
            }, true);

            __construct();
        });

        app.factory('BusinessSchedule', function ($http) {
            return function (parameters) {
                return $http.get("/api/schedules/{{ $business->id }}", {
                    params: parameters
                });

            }
        });

        app.filter('existsInArray', function ($filter) {
            return function (list, arrayFilter, element) {
                if (arrayFilter) {
                    element = element || 'id';
                    arrayFilter = $filter("map")(arrayFilter, element);
                    return $filter("filter")(list, function (listItem) {
                        return arrayFilter.indexOf(listItem[element]) < 0;
                    });
                }
            };
        });

        app.filter('availableTime', function () {
            return function (items, services) {
                if (services) {
                    var filtered = [];
                    angular.forEach(items, function (item, key) {
                        var isUnavailable = false;

                        if (moment.unix(item.time).isSameOrBefore(moment())) {
                            isUnavailable = true;
                            return true;
                        }

                        angular.forEach(services, function (service) {
                            if ($.inArray(service.id, item.unavailable_offers || []) >= 0) {
                                isUnavailable = true;
                                return true;
                            }
                        });

                        searchKey = key;
                        angular.forEach(services, function (service) {
                            var lookAhead = Math.ceil(service.duration_minute / 15);
                            for (var i = searchKey; i <= searchKey + lookAhead; i++) {
                                if (
                                    (searchKey + lookAhead) >= items.length
                                    || items[i]
                                    && $.inArray(service.id, items[i].unavailable_offers || []) >= 0
                                ) {
                                    isUnavailable = true;
                                    return true;
                                }
                            }
                            if (isUnavailable) {
                                return true;
                            }
                            searchKey += lookAhead;
                        });

                        if (!isUnavailable) {
                            filtered.push(item);
                        }
                    });
                    return filtered;
                }
                else {
                    return items;
                }
            };
        });

        app.filter('contains', function () {
            return function (array, needle) {
                if (!array) {
                    return false;
                }
                return array.indexOf(needle) >= 0;
            };
        });

        app.filter('minutesToHour', [function () {
            return function (minutes) {
                if (minutes < 60) {
                    return (minutes) + 'm';
                }
                else if ((minutes % 60) == 0) {
                    return (minutes - minutes % 60) / 60 + 'h';
                }
                else {
                    return ((minutes - minutes % 60) / 60 + 'h' + ' ' + minutes % 60 + 'm');
                }
            };
        }]);

        app.filter('sumByKey', function () {
            return function (data, key) {
                if (typeof(data) === 'undefined' || typeof(key) === 'undefined') {
                    return 0;
                }

                var sum = 0;
                for (var i = data.length - 1; i >= 0; i--) {
                    sum += parseInt(data[i][key]);
                }

                return sum;
            };
        })
    </script>
@endsection

@push('javascript')
<div class="modal fade" id="appointment_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Set an appointment</h4>
            </div>
            <div class="modal-body">
                <form>
                    @if(Auth::guest() || $businessMember)
                        <div class="alert alert-info">
                            <div class="container">
                                @if($businessMember)
                                    Business owner or member is not allowed to submit an appointment to there business.
                                @else
                                    Please <a href="{{ route('login') }}">login</a> or <a href="{{ route('register') }}">register</a> before you continue.
                                @endif
                            </div>
                        </div>
                    @endif
                    <div class="form-group row">
                        {{--

                        Month and Year with timezone Starts Here

                        --}}
                        <div class="col-xs-12 text-center">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4 text-center">
                                    <div class="dropdown1">
                                        <label>Selected Date</label> <br>
                                        <a class="dropdown-toggle btn btn-primary btn-lg" id="dLabel" role="button" data-toggle="dropdown" data-target=".dropdown1" href="#">
                                            <i class="glyphicon glyphicon-calendar"></i> @{{ startDate | amDateFormat:'dddd, Do MMMM YYYY' }}
                                        </a>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                            <datetimepicker
                                                    data-ng-model="startDate"
                                                    data-datetimepicker-config="{ startView:'day', minView: 'day', dropdownSelector: '.dropdown-toggle' }" data-before-render="startDateBeforeRender($dates)">
                                            </datetimepicker>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {{--

                        Time Buttons Starts Here

                        --}}
                        <div class="col-xs-12">
                            <div class="row text-center">

                                <div class="col-xs-12">
                                    <hr>
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-4" ng-repeat="a in timeSchedules" ng-if="!loading">
                                    <button type="button"
                                            ng-class="(selectedTime == a.time ? 'active btn-primary': 'btn-default')"
                                            ng-click="selectTime(a.time)"
                                            class="btn btn-xs btn-default form-control"
                                            style="margin-bottom: 2px">
                                        @{{ a.time | amFromUnix | amTimezone: timezone | amDateFormat:'h:mm a' }}
                                    </button>
                                </div>

                                <div class="col-xs-12" ng-if="(timeSchedules | isEmpty) && !loading" ng-hide="loading">
                                    <br>
                                    <br>
                                    <p class="lead">No time available for new appointment.</p>
                                    <br>
                                </div>

                                <div class="col-xs-12" ng-hide="!loading">
                                    <br>
                                    <br>
                                    <p class="lead"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></p>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--

                    Selected Time Starts Here

                    --}}
                    <div class="panel panel-gray" ng-if="selectedTime">
                        <div class="panel-body">
                            <p class="text-center">
                                <label>Selected Time and Date</label>
                            </p>
                            <div class="row">
                                <div class="col-sm-5 col-xs-4 text-right">Store Time:</div>
                                <div class="col-sm-7 col-xs-8">@{{ selectedTime | amFromUnix | amTimezone: timezone | amDateFormat:'dddd, Do MMMM YYYY, h:mm a z' }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5 col-xs-4 text-right">Local Time:</div>
                                <div class="col-sm-7 col-xs-8">@{{ selectedTime | amFromUnix | amLocal | amDateFormat:'dddd, Do MMMM YYYY, h:mm a z' }}</div>
                            </div>
                        </div>
                    </div>
                    {{--

                    Selected Services Starts Here

                    --}}
                    <div class="panel panel-gray">
                        <div class="panel-body">
                            <div class="form-group">
                                <div ui-sortable="{axis:'y'}" ng-model="services" style="cursor: move;">
                                    <div class="row" style="margin-bottom: 20px;" ng-repeat="s in services track by $index">
                                        <div class="col-sm-6">
                                            <span class="lead"><small>@{{ $index + 1 }}.</small> @{{ s.name }}</span>
                                            <p ng-if="daySchedules.unavailable_offers | contains:s.id" class="text-primary">This service is not available for this day.</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row text-right">
                                                <div class="col-xs-5">@{{ s.amount }}</div>
                                                <div class="col-xs-5">
                                                    @{{ s.duration }}
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" style="margin-right: 10px" class="close" ng-click="subService($index)"><span aria-hidden="true">×</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center" ng-if="services | isEmpty">
                                    <h3>No Services Selected</h3>
                                </div>

                                <hr>
                                <div class="row" style="margin-bottom: 20px;">
                                    <div class="col-sm-6">
                                        <div class="col-xs-6">
                                        </div>
                                        <div class="col-xs-6 text-right hidden-xs">
                                            Total :
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row text-right">
                                            <div class="col-xs-5">
                                                @{{ services | sumByKey:'amount_number' | currency }}
                                            </div>
                                            <div class="col-xs-5">
                                                @{{ services | sumByKey:'duration_minute' | minutesToHour }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--

                    Offered Other Services Starts Here

                    --}}
                    <div class="panel panel-default collapse" id="servicesCollapse">
                        <div class="panel-body">
                            <div class="row" style="margin-bottom: 20px;" ng-repeat="s in offers">
                                <div class="col-sm-6"><span class="lead">@{{ s.name }}</span></div>
                                <div class="col-sm-6 text-right">

                                    <span style="margin-right: 10px">@{{ s.amount }}</span>
                                    <span style="margin-right: 10px">@{{ s.duration }}</span>
                                    <button type="button" class="btn btn-primary" ng-click="addService(s)">
                                        <i class="fa fa-envelope-o"></i> Book now
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-default form-control" data-toggle="collapse" data-target="#servicesCollapse">
                            Services
                        </button>
                    </div>

                    <button type="button" class="btn btn-primary" ng-disabled="(!selectedTime) ||  (services | isEmpty)" ng-click="submitAppointment()">Send message</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </form>
                <form action="{{ route('app.business.appointment.store', $business->id) }}" method="POST" class="hidden action-confirm" id="appointmentForm">
                    {{ csrf_field() }}
                    <div id="appointmentFormContent"></div>
                </form>
            </div>
        </div> <!-- / .modal-content -->
    </div> <!-- / .moda-dialog -->
</div> <!-- / .modal -->
@endpush

@push('css')
<style>
    .profilePicture {
        border: 1px solid #e9e9e9;
    }
</style>
@endpush