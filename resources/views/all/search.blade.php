@extends('all.businesses')

@section('title', 'Search Results')

@section('empty-business', 'Searched item not found.')