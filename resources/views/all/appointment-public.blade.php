@extends('layouts.client-page')

@section('title', 'Appointment Request')

@section('content')
    <div class="container">
        <div class="row">
            @php
                $s = 'col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3';
                if(Auth::check()) {
                    $s = 'col-sm-8';
                }
            @endphp
            <div class="col-sm-4">
                <div class="panel panel-default">

                    <!-- Default panel contents -->
                    <div class="panel-heading">Appointments</div>

                    <!-- List group -->
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($appointments as $item)
                                <a href="{{ route('app.appointment.public', $item->id) }}" class="list-group-item">
                                    @if(\Carbon\Carbon::now()->lt($item->appointment_at))
                                        <span class="text-info">{{ $item->business->name }}</span>
                                    @else
                                        <span class="text-muted">{{ $item->business->name }}</span>
                                    @endif
                                    <br>
                                    <small>{{ $item->appointment_at->diffForHumans() }}</small>
                                </a>
                            @endforeach
                        </div>
                        {{ $appointments->links() }}
                    </div>

                </div>
            </div>
            <div class="{{ $s }}">
                <div class="panel panel-default">
                    <div class="panel-body" style="padding-left: 40px; padding-right: 40px;">
                        <h1 class="block-header alt">
                            <span>Appointment Request</span>
                        </h1>
                        <form>
                            <div class="form-group">
                                <label class="control-label">Business</label>
                                <div class="input-lg text-muted">
                                    <a href="{{ route('app.business', $appointment->business->id) }}"
                                       class="lead" target="_blank">
                                        {{ $appointment->business->name }}
                                    </a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Address</label>
                                <div class="input-lg text-muted">
                                    {{ implode(', ', $appointment->business->getPrimaryAddress()->address->toArray()) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Preferred Time and Date</label>
                                <div class="input-lg text-muted">
                                    {{ $appointment->appointmentTimezone()->format('l, jS \\of F Y h:i A T') }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Customer Name</label>
                                <div class="input-lg text-muted">
                                    {{ $appointment->user->name }}
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label class="control-label">Purpose / Message</label>--}}
                                {{--<div class="input-lg text-muted">--}}
                                    {{--{{ $appointment->message }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label class="control-label">Services Availed</label>
                                <div class="row">
                                    @php($duration = 0)
                                    @php($amount = 0)
                                    @foreach($appointment->offers as $offer)
                                        @php($amount += $offer->getAmountFloat())
                                        @php($duration += $offer->getDurationInMinutes())
                                        <div class="col-xs-12">
                                            <span class="lead">{{ $offer->service->name }}</span>
                                            <div class="row">
                                                <div class="col-xs-8 text-right">{{ $offer->getAmount(true) }}</div>
                                                <div class="col-xs-4 text-right">{{ $offer->getDuration() }}</div>
                                            </div>
                                            <br>
                                        </div>
                                    @endforeach
                                    @unless(count($appointment->offers))
                                        <div class="col-xs-12">
                                            <br>
                                            <div class="lead">No Service availed</div>
                                        </div>
                                    @endunless
                                    <div class="col-xs-12">
                                        <hr>
                                    </div>
                                    <div class="col-xs-4"><strong>Total</strong></div>
                                    <div class="col-xs-4 text-right">{{ intToCurrency($amount) }}</div>
                                    <div class="col-xs-4 text-right">{{ durationToString($duration) }}</div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <p class="text-right">
                    <a href="{{ route('home') }}" class="lead">Return Home</a>
                </p>
            </div>
        </div>
    </div>
@endsection

@push('css')
<style>
    .input-lg {
        height: auto;
        min-height: 40px;
    }
</style>
@endpush