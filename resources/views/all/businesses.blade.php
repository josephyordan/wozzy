@extends('layouts.client-page')

@section('title', 'Businesses')

@php($search = orHelper($search, collect()))
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-9">

                @forelse($businesses as $business)
                    <div class="blog__item clearfix">

                        <div class="blog__header">
                            <h1 class="blog__title">
                                <a href="{{ route('app.business', $business->id) }}">
                                    {{ $business->name }}
                                </a>
                            </h1>
                            <ul class="blog__info">
                                <li>
                                    <i class="fa fa-location-arrow"></i> <a href="#">{{ implode(', ', $business->getPrimaryAddress()->address->toArray()) }}</a>
                                </li>
                            </ul>
                        </div> <!-- / .blog__header -->

                        <div class="blog__body">
                            @if($business->picture_file_name)
                                <a href="{{ $business->picture->url() }}" data-lightbox="businessProfile{{ $business->id }}">
                                    <img src="{{ $business->picture->url('gallery') }}" alt="" class="img-responsive img-article pull-right">
                                </a>
                            @endif
                            {{ limitStr($business->description, 500) }}
                        </div> <!-- / .blog__body -->

                        <div class="blog__footer">
                            <ul class="blog__tags">
                                @foreach($business->categories as $category)
                                    <li><a href="{{ route('app.search', ['category_id' => $category->id]) }}">{{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                        </div> <!-- / .blog__footer -->

                    </div> <!-- / .blog__item -->
                @empty
                    <p class="lead">@yield('empty-business', 'Businesses is Empty')</p>
            @endforelse


            <!-- Pagination -->
                <nav class="text-right">
                    {{ $businesses->links() }}
                </nav>

            </div>
            <div class="col-sm-3">

                <h1 class="block-header alt">
                    <span>Search</span>
                </h1>
                <form action="{{ route('app.search') }}">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" placeholder="Search" name="all" value="{{ $search->get('all') }}">
                    </div>

                    <button type="submit" class="btn btn-primary submit">
                        Submit
                    </button>
                </form>

                <h1 class="block-header alt">
                    <span>Advance Search</span>
                </h1>
                <form action="{{ route('app.search') }}">

                    <div class="form-group">
                        <label class="control-label">Business Name</label>
                        <input type="text" class="form-control input-lg" placeholder="Business Name" name="name" value="{{ $search->get('all') ?: $search->get('name') }}">
                    </div>

                    <div class="form-group">
                        <label class="control-label">State</label>
                        <select class="form-control input-lg" id="addressState" name="state_id">
                            <option value="">Select state</option>
                            @foreach(\GeoLocation::getCountry('US')->getChildren() as $state)
                                @if($search->get('state_id') == $state->id)
                                    <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                                @else
                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">City</label>
                        <select class="form-control input-lg" id="addressCity" name="city_id">
                            <option value="">Select city</option>
                            @if(\GeoLocation::find($search->get('state_id')))
                                @foreach(\GeoLocation::find($search->get('state_id'))->getChildren() as $city)
                                    @if($search->get('city_id') == $city->id)
                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                    @else
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Categories</label>
                        @foreach(\App\Models\Category::getBySort() as $category)
                            <div class="checkbox">
                                <input type="checkbox" name="categories_id[]" value="{{ $category->id }}" class="form-check-input" id="category{{ $category->id }}" {{ in_array($category->id, (array)$search->get('categories_id') ?: []) ? 'checked' : '' }}>
                                <label class="form-check-label" for="category{{ $category->id }}">{{ $category->name }}</label>
                            </div>
                        @endforeach
                    </div>

                    <button type="submit" class="btn btn-primary submit">
                        Submit
                    </button>
                </form>

            </div>
        </div> <!-- / .row -->
    </div>
@endsection

@include('scripts.address')

@push('css')
<link href="{{ asset('assets/plugins/lightbox/css/lightbox.css') }}" rel="stylesheet">
@endpush
@push('javascript')
<script src="{{ asset('assets/plugins/lightbox/js/lightbox.min.js') }}"></script>
@endpush