@extends('all.business-layout')

@section('profile-body')
    <h1 class="block-header alt">
        <span>Appointment</span>
    </h1>

    <form action="{{ route('app.business.appointment.store', $business->id) }}" method="POST" id="payment-form">
        {{ csrf_field() }}

        <div class="form-group">
            <label class="control-label">Full name</label>
            @if(!Auth::check())
                <input type="text" class="form-control input-lg" placeholder="Full name" name='name' value="{{ old('name', $user->name) }}" required>
                @include('inc.help-block-danger', ['error' => 'name'])
            @else
                <div class="input-lg text-muted">{{ $user->name }}</div>
            @endif
        </div>

        <div class="form-group">
            <label class="control-label">Email address</label>
            @if(!Auth::check())
                <input type="email" class="form-control input-lg" placeholder="Email" name='email' value="{{ old('email', $user->email) }}" required>
                @include('inc.help-block-danger', ['error' => 'email'])
            @else
                <div class="input-lg text-muted">{{ $user->email }}</div>
            @endif
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-sm-6">
                <label class="control-label">State</label>
                @if(!Auth::check() or !$valid)
                    <select class="form-control input-lg" id="addressState" name="state_id" required>
                        <option>Select state</option>
                        @foreach(\GeoLocation::getCountry('US')->getChildren() as $state)
                            @if(old('state_id', $address->state_id) == $state->id)
                                <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                            @else
                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                            @endif
                        @endforeach
                    </select>
                    @include('inc.help-block-danger', ['error' => 'city_id'])
                @else
                    <div class="input-lg text-muted">{{ $address->state }}</div>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label class="control-label">City</label>
                @if(!Auth::check() or !$valid)
                    <select class="form-control input-lg" id="addressCity" name="city_id" required>
                        <option>Select city</option>
                        @if(\GeoLocation::find(old('state_id', $address->state_id)))
                            @foreach(\GeoLocation::find(old('state_id', $address->state_id))->getChildren() as $city)
                                @if(old('city_id', $address->city_id) == $city->id)
                                    <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                @else
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                    @include('inc.help-block-danger', ['error' => 'city_id'])
                @else
                    <div class="input-lg text-muted">{{ $address->city }}</div>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="control-label">Postal / Zip Code</label>
            @if(!Auth::check() or !$valid)
                <input type="text" class="form-control input-lg" placeholder="Postal / Zip Code" name="post_code" value="{{ old('post_code', $address->post_code) }}" required>
                @include('inc.help-block-danger', ['error' => 'post_code'])
            @else
                <div class="input-lg text-muted">{{ $address->post_code }}</div>
            @endif
        </div>

        <div class="form-group">
            <label class="control-label">Address Line1</label>
            @if(!Auth::check() or !$valid)
                <input type="text" class="form-control input-lg" placeholder="Address Line1" name="line1" value="{{ old('line1', $address->line1) }}" required>
                <span class="help-block small">Street address, P.O. box, company name, c/o</span>
                @include('inc.help-block-danger', ['error' => 'line1'])
            @else
                <div class="input-lg text-muted">{{ $address->line1 }}</div>
            @endif
        </div>

        <div class="form-group">
            <label class="control-label">Address Line2</label>
            @if(!Auth::check() or !$valid)
                <input type="text" class="form-control input-lg" placeholder="Address Line2" name="line2" value="{{ old('line2', $address->line2) }}">
                <span class="help-block small">Apartment, suite, unit, building, floor, etc.</span>
                @include('inc.help-block-danger', ['error' => 'line2'])
            @else
                <div class="input-lg text-muted">{{ $address->line2 }}</div>
            @endif
        </div>

        <div class="row">
            <div class="form-group col-sm-6">
                <label class="control-label">Primary Contact</label>
                @if(!Auth::check() or !$valid)
                    <input type="text" class="form-control input-lg contact1" placeholder="Contact Number" name="primary_contact" value="{{ old('primary_contact', $primaryContact->number) }}" required>
                    @include('inc.help-block-danger', ['error' => 'primary_contact'])
                @else
                    <div class="input-lg text-muted">{{ $primaryContact->number }}</div>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label class="control-label">Secondary Contact</label>
                @if(!Auth::check() or !$valid)
                    <input type="text" class="form-control input-lg contact2" placeholder="Contact Number" name="secondary_contact" value="{{ old('secondary_contact', $secondaryContact->number) }}">
                    @include('inc.help-block-danger', ['error' => 'secondary_contact'])
                @else
                    <div class="input-lg text-muted">{{ $secondaryContact->number }}</div>
                @endif
            </div>
        </div>

        @if(Auth::check())
            <div class="form-group">
                <p class="text-right">
                    <a class="text-primary" href="{{ route('settings.user.edit') }}"> Edit Info </a>
                </p>
            </div>
        @endif

        <hr>

        @if($user->hasStripeId() && $user->hasCardOnFile())
            <div class="row">
                <div class='col-xs-8 form-group card required'>
                    <label class='control-label'>Card Owner</label>
                    <div class="input-lg text-muted">{{ $user->name }}</div>
                </div>
                <div class='form-row'>
                    <div class='col-xs-8 form-group card required'>
                        <label class='control-label'>Card Number</label>
                        <div class="input-lg text-muted">**** **** **** {{ $user->card_last_four }}</div>
                    </div>
                </div>
                <div class='col-xs-4 form-group cvc required'>
                    <label class='control-label'>Card Brand</label>
                    <div class="input-lg text-muted">{{ $user->card_brand }}</div>
                </div>
            </div>
            <div class='form-group'>
                <p class="text-right"><a href="{{ route('settings.user.edit') }}#card">Edit Card</a></p>
            </div>
        @else
            <h3 class="text-center">
                <span class="payment-errors label label-danger"></span>
            </h3>

            <div class="row">
                <div class='form-row'>
                    <div class='col-xs-12 form-group card required'>
                        <label class='control-label'>Card Number</label>
                        <input autocomplete='off' class='form-control card-number input-lg' data-stripe="number" size='20' type='text' required placeholder="{{ !app()->environment('production') ? '4242 4242 4242 4242' : '**** **** **** ****' }}">
                    </div>
                </div>
                <div class='form-row'>
                    <div class='col-xs-6 form-group cvc required'>
                        <label class='control-label'>CVC</label>
                        <input autocomplete='off' class='form-control card-cvc input-lg' placeholder='ex. 311' data-stripe="cvc" size='4' type='text' required>
                    </div>
                    <div class='col-xs-6 form-group expiration required'>
                        <label class='control-label'>Expiration MM / YY</label>
                        <input class='form-control card-expiry input-lg' placeholder='MM / YY' data-stripe="exp" type='text' required>
                    </div>
                </div>
            </div>
        @endif

        <hr>

        <div class="form-group">
            <label class="control-label">Preffered Date and Time</label>
            <div class='input-group date' id='appointment_at'>
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                <input type='text' class="form-control" name="appointment_at" value="{{ old('appointment_at') }}" required/>
            </div>
            @include('inc.help-block-danger', ['error' => 'appointment_at'])
        </div>

        <div class="form-group">
            <label class='control-label'>Message / Purpose</label>
            <textarea class="form-control input-lg" rows="1" placeholder="Message / Purpose"
                      name="message" required>{{ old('message') }}</textarea>
            @include('inc.help-block-danger', ['error' => 'message'])
        </div>

        <button type="submit" class="btn btn-primary submit">
            Save changes
        </button>
    </form>
@endsection

@if(!Auth::check() or !$valid or !($user->hasStripeId() && $user->hasCardOnFile()))
    @include('scripts.address')
    @include('scripts.form-formatter')
    @unless($user->hasStripeId() && $user->hasCardOnFile())
        @include('scripts.billing')
    @endunless
    @push('javascript')
    <script>
        @unless($valid)
            new Cleave('.contact1', {
            phone: true,
            phoneRegionCode: 'us'
        });

        new Cleave('.contact2', {
            phone: true,
            phoneRegionCode: 'us'
        });
        @endunless

        @unless($user->hasStripeId() && $user->hasCardOnFile())
            new Cleave('.card-number', {
            creditCard: true
        });
        new Cleave('.card-expiry', {
            date: true,
            datePattern: ['m', 'y']
        });
        @endunless
    </script>
    @endpush
@endif

@push('css')
<link rel="stylesheet" href="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('javascript')
<script src="{{ asset('components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $(function () {
        var __contruct = function () {
            var minDate = new Date();
            minDate.setHours(0, 0, 0, 0);
            var setDateTime = {
                defaultDate: new Date(),
                minDate: minDate,
                format: 'LL hh:00 a',
                allowInputToggle: true
            };

            $('#appointment_at').each(function () {
                var dateValue = $(this).find('input').val();
                $(this).datetimepicker(setDateTime);
                $(this).data("DateTimePicker").date(dateValue);
            });
        };

        $("form textarea").on({
            focus: function () {
                if (!$(this).val()) {
                    $(this).data("original-height", $(this).outerHeight());
                }
                $(this).animate({"height": "100px"}, 300);
            },
            blur: function () {
                if (!$(this).val()) {
                    $(this).animate({"height": $(this).data("original-height")}, 300);
                }
            }
        });

        __contruct();
    })
</script>
@endpush