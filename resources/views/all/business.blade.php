@extends('all.business-layout')

@section('profile-body')
    <h1 class="block-header alt">
        <span>Description</span>
    </h1>
    <div class="text-muted">
        {!! $business->description !!}
    </div>

    <h1 class="block-header alt">
        <span>Images</span>
    </h1>
    <div class="row">
        @foreach($business->getImages() as $document)
            @include('logged.settings-portfolio-image', compact('document'))
        @endforeach
    </div> <!-- / .row -->

    <h1 class="block-header alt">
        <span>Videos</span>
    </h1>

    <div class="row">
        @foreach($business->getVideos() as $document)
            @include('logged.settings-portfolio-video', compact('document'))
        @endforeach
    </div> <!-- / .row -->

    <h1 class="block-header alt">
        <span>Services</span>
    </h1>

    <div class="text-center" ng-if="offers | isEmpty" ng-cloak>
        <h3>No services offered</h3>
    </div>

    <div class="row" style="margin-bottom: 20px" ng-repeat="x in offers" ng-cloak>
        <div class="col-sm-6"><span class="lead">@{{ x.name }}</span></div>
        <div class="col-sm-6">
            <div class="row text-right">
                <div class="col-xs-4">@{{ x.amount }}</div>
                <div class="col-xs-4">@{{ x.duration }}</div>
                <div class="col-xs-4"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#appointment_form" ng-click="addService(x)">
                        <i class="fa fa-envelope-o"></i> Book now
                    </a></div>
            </div>
        </div>
    </div> <!-- / .row -->
@endsection

@push('javascript')
<script src="{{ asset('assets/plugins/lightbox/js/lightbox.min.js') }}"></script>
@endpush

@push('css')
<link href="{{ asset('assets/plugins/lightbox/css/lightbox.css') }}" rel="stylesheet">
@endpush

@include('scripts.html5lightbox')