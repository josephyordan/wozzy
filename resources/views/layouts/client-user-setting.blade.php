@extends('layouts.client-page')

@section('title')
    Settings -  @yield('setting-title')
@endsection

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
        </div> <!-- / .row -->
    </div>
@overwrite