@extends('layouts.client-page')

@section('title')
    Settings -  @yield('setting-title')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            @hasanyrole(\Spatie\Permission\Models\Role::all())
            <div class="col-sm-3">

                <div class="panel panel-default">

                    <!-- Default panel contents -->
                    <div class="panel-heading">Settings</div>

                    <!-- List group -->
                    <div class="panel-body">
                        <div class="list-group">
                            @include('inc.sidebar-client-setting')
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-sm-9">
                @yield('content')
            </div>
            @else
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @yield('content')
                        </div>
                    </div>
                </div>
            @endhasanyrole
        </div> <!-- / .row -->
    </div>
@overwrite