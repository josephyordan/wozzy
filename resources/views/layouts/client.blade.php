<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" ng-app="@yield('angular-app', 'app')"
      ng-controller="@yield('angular-controller', 'controller')">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('assets/favicon/favicon.ico') }}">

    <title>{{ config('app.name', 'Laravel') }} | @section('title') @show</title>

    <!-- CSS Plugins -->
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" >

    <!-- CSS Global -->
    <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='{{ asset('components/open-sans-fontface/open-sans.css') }}' rel='stylesheet' type='text/css'>

    @stack('css')

</head>

<body>

<!-- NAVIGATION
================================================== -->

<!-- Navbar -->
<nav class="navbar navbar-default">
    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_main" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{{ route('home') }}">{{ config('app.brand', 'Laravel') }}</a>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar_main">

            @include('inc.main-menu')

            @include('inc.user-menu')

            <form action="{{ route('app.search') }}" class="navbar-form navbar-search navbar-right">
                <div class="form-group">
                    <input type="text" name="all" class="form-control" placeholder="&#xF002; &nbsp; Search">
                </div>
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-search"></i> Search
                </button>
            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>

<!-- Header -->
@yield('header')

@include('inc.alerts')
@include('inc.alert-danger')


<!-- CONTENT
================================================== -->
@yield('content')
<!-- / .container -->


<!-- FOOTER
================================================== -->
<footer>
    <div class="container">

        <div class="row">
            <div class="col-sm-6">

                <p>
                    Company 2017. All rights reserved.
                </p>

            </div>
        </div> <!-- / .row -->

    </div> <!-- / .container -->
</footer>


<!-- JavaScript
================================================== -->

<!-- JS Global -->
<script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('components/moment/moment.js') }}"></script>
<script src="{{ asset('components/moment-timezone/builds/moment-timezone-with-data.min.js') }}"></script>

<!-- JS Plugins -->
<script src="{{ asset('assets/plugins/scrolltopcontrol/scrolltopcontrol.js') }}"></script>
@if (trim($__env->yieldContent('angular')))
    <script src="{{ asset('components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('components/angular/angular.min.js') }}"></script>
    <script src="{{ asset('components/angular-moment/angular-moment.min.js') }}"></script>
    <script src="{{ asset('components/angular-ui-sortable/sortable.min.js') }}"></script>
    <script src="{{ asset('components/angular-filter/dist/angular-filter.min.js') }}"></script>
    <script src="{{ asset('components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js') }}"></script>
    <script src="{{ asset('components/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js') }}"></script>
    <link href="{{ asset('components/angular-bootstrap-datetimepicker/src/css/datetimepicker.css') }}" rel="stylesheet" >
@endif
@yield('angular')
@stack('javascript')

<!-- JS Custom -->
<script src="{{ asset('assets/js/custom.js') }}"></script>

</body>
</html>