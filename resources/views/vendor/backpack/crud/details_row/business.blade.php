<div class="m-t-10 m-b-10 p-l-10 p-r-10 p-t-10 p-b-10">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box">
                <div class="box-body row">
                    <form>
                        <div class="form-group row col-xs-12">
                            <label class="col-sm-2">Name :</label>
                            <div class="col-sm-10">
                                {{ $entry->name }}
                            </div>
                        </div>
                        <div class="form-group row col-xs-12">
                            <label class="col-sm-2">Contact :</label>
                            <div class="col-sm-10">
                                {{ $entry->contact }}
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>