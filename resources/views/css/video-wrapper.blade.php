@push('css')
<style>
    .video-wrapper {
        position: relative;
        display: block;
    }

    .video-wrapper:before {
        content: ' ';
        position: absolute;
        height: 100%;
        width: 100%;
        z-index: 999;
        display: block;
        background-repeat: no-repeat;
        background-image: url('http://explicit.dev/assets/img/play-button.png');
        background-size: cover;
        background-position: center;
    }
</style>
@endpush