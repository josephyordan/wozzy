@push('javascript')
<script>
    $(function () {
        var addressCity = $('#addressCity');
        $('#addressState').on('change', function () {
            var id = this.value;
            addressCity.prop('disabled', true);
            var defaultOption = $('<option>', {'html': 'Select city'});
            addressCity.html(defaultOption);
            $.get('/api/geo/children/' + id, function (data) {
                $.each(data,function(key, value)
                {
                    addressCity.append('<option value=' + value.id + '>' + value.name + '</option>');
                });
            }).always(function () {
                addressCity.prop('disabled', false);
            });
        });
    })
</script>
@endpush