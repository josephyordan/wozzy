@push('javascript')
<script>
    $(".delete-confirm").submit(function() {
        var c = confirm("Are you sure? \nYou won't be able to revert this!");
        return c; //you can just return c because it will be true or false
    });

    $(".unsubscribe-confirm").submit(function() {
        var c = confirm("Are you sure you want to unsubscribe?");
        return c; //you can just return c because it will be true or false
    });

    $(".action-confirm").submit(function() {
        var c = confirm("Are you sure you want to continue?");
        return c; //you can just return c because it will be true or false
    });
</script>
@endpush