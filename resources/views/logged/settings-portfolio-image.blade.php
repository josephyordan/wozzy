@php($deleteFile = orHelper($deleteFile, false))
<div class="col-xs-6 col-sm-4 col-lg-4 isotope-item ">

    @if($deleteFile)
        <a href="#" data-id="{{ $document->id }}" class="btn-sm text-danger delete-file" style="float: right; overflow: hidden"><i class="glyphicon glyphicon-remove"></i></a>
    @endif
    <a href="{{ $document->file->url() }}" class="thumbnail alt" data-lightbox="...">
        <img src="{{ $document->file->url('gallery') }}" alt="...">
    </a>

</div>