@extends('logged.settings-member')

@section('form-content')
    <form action="{{ route('settings.member.update', ['schedule' => $user->id]) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        @include('logged.settings-member-form')

        <button type="submit" class="btn btn-primary submit">
            Update
        </button>

        <a href="{{ route('settings.members') }}" class="btn btn-default">
            Cancel
        </a>
    </form>
@endsection