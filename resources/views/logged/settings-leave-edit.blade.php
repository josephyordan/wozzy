@extends('logged.settings-leave')

@section('form-content')
    <form action="{{ route('settings.leave.update', ['absent' => $absent->id]) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        @include('logged.settings-leave-form')
    </form>
@endsection