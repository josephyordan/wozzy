@extends('layouts.client-setting')

@section('setting-title', 'Member')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li><a href="{{ route('settings.members') }}">Members</a></li>
    <li class="active">Member</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Member</span>
        </h1>

        @yield('form-content')
    </div>
@endsection