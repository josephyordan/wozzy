@extends('layouts.client-setting')

@section('setting-title', 'Schedules')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Leaves</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Leaves</span>
        </h1>

        <p>
            <a href="{{ route('settings.leave.create') }}" class="btn btn-lg btn-primary">
                Create
            </a>
        </p>

        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Absent at</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- Single event in a single day -->
                @foreach($absence as $absent)
                    <tr>
                        <td>
                            {{ $absent->user->name }}
                        </td>
                        <td>
                            {{ $absent->absent_at->toFormattedDateString() }}
                        </td>
                        <td>
                            <form action="{{ route('settings.leave.delete', $absent->id) }}" method="POST" class="delete-confirm">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('settings.leave', $absent->id) }}"
                                       class="btn btn-secondary btn-default">
                                        Edit
                                    </a>
                                    <input type="submit" class="btn btn-secondary btn-danger" value="Delete">
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('scripts.action-confirm')