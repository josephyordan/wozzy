@extends('layouts.client-setting')

@section('setting-title', 'Subscribe')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Subscribe</li>
@endsection

@section('content')
    <div>
        @include('logged.settings-plans-content')

        <span id="link"></span>

        @if( $subscription )
            @if( $subscription->onGracePeriod() )

                <div class="alert alert-warning">
                    <p class="lead">Subscription expiring at {{ $subscription->ends_at->toFormattedDateString() }}</p>
                </div>

                <form method="post" action="{{ route('settings.subscribe.resume') }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Resume Subscription</button>
                </form>
                <br>

            @else
                <form method="post" action="{{ route('settings.subscribe.cancel') }}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default">Cancel Subscription</button>
                </form>
            @endif
        @endif

    </div>
@endsection

@include('scripts.action-confirm')

