@extends('layouts.client-setting')

@section('setting-title', 'Schedules')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Schedules</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Services</span>
        </h1>

        <p>
            <a href="{{ route('settings.service.create') }}" class="btn btn-lg btn-primary">
                Create
            </a>
        </p>

        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Duration</th>
                    <th>Users</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- Single event in a single day -->
                @foreach($offers as $offer)
                    <tr>
                        <td>
                            {{ $offer->service->name }}
                        </td>
                        <td>
                            {{ $offer->getAmount(true) }}
                        </td>
                        <td>
                            {{ $offer->getDuration() }}
                        </td>
                        <td>
                            @if($offer->getUsers())
                                {!! implode('<br> ', $offer->getUsers()->pluck('name')->toArray()) !!}
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('settings.service.delete', $offer->id) }}" method="POST" class="delete-confirm">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('settings.service', $offer->id) }}"
                                       class="btn btn-secondary btn-default">
                                        Edit
                                    </a>
                                    <input type="submit" class="btn btn-secondary btn-danger" value="Delete">
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('scripts.action-confirm')