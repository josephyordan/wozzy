@extends('layouts.client-setting')

@section('setting-title', 'Schedules')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Schedules</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Schedules</span>
        </h1>

        <p>
            <a href="{{ route('settings.schedule.create') }}" class="btn btn-lg btn-primary">
                Create
            </a>
        </p>

        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Days</th>
                    <th>Time</th>
                    {{--<th>Breaks</th>--}}
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- Single event in a single day -->
                @foreach($schedules as $schedule)
                    <tr>
                        <td>
                            @foreach($schedule->days as $day)
                                <div>{{ $days[$day] }}</div>
                            @endforeach
                        </td>
                        <td>
                            {{ $schedule->starts_at }} - {{ $schedule->ends_at }}
                        </td>
                        {{--<td>--}}
                            {{--@forelse($schedule->breaks as $break)--}}
                                {{--<div>{{ $break->starts_at }} - {{ $break->ends_at }}</div>--}}
                            {{--@empty--}}
                                {{--None--}}
                            {{--@endforelse--}}
                        {{--</td>--}}
                        <td>
                            <form action="{{ route('settings.schedule.delete', $schedule->id) }}" method="POST" class="delete-confirm">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('settings.schedule', $schedule->id) }}"
                                       class="btn btn-secondary btn-default">
                                        Edit
                                    </a>
                                    <input type="submit" class="btn btn-secondary btn-danger" value="Delete">
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('scripts.action-confirm')