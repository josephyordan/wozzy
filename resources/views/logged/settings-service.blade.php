@extends('layouts.client-setting')

@section('setting-title', 'Service')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li><a href="{{ route('settings.services') }}">Services</a></li>
    <li class="active">Service</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Service</span>
        </h1>

        @yield('form-content')
    </div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@include('scripts.form-formatter')

@push('javascript')
<script src="{{ asset('components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
    $(function () {
        var __construct = function () {
            $('#service').on('change', function () {
                setOtherValue();
            });

            $('#duration').each(function () {
                var dateValue = $(this).find('input').val();
                $(this).datetimepicker(setTimeOptions);
                $(this).data("DateTimePicker").date(dateValue);
            });

            setOtherValue();
        };

        var setOtherValue = function () {
            if ($('#service').val() === 'other') {
                $('#otherService').show();
            } else {
                $('#otherService').hide();
            }
        };

        var setTimeOptions = {
            format: 'H:mm',
            defaultDate: moment().startOf('day'),
            allowInputToggle: true
        };

        new Cleave('.amount', {
            numeral: true,
            numeralDecimalScale: 2,
            stripLeadingZeroes: true,
            numeralThousandsGroupStyle: 'thousand'
        });

        __construct();
    });
</script>
@endpush