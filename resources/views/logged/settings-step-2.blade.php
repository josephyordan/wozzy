@extends('logged.settings-step')

@section('step', '2')
@section('content-title', 'Business Schedule')

@section('content')
    @include('logged.settings-schedule-form')

    <button type="submit" class="btn btn-primary submit">
        Save
    </button>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@push('javascript')
<script src="{{ asset('components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
    $(function () {
        var __construct = function () {
            $('#startTime, #endTime').each(function () {
                var dateValue = $(this).find('input').val();
                $(this).datetimepicker(setTimeOptions);
                $(this).data("DateTimePicker").date(dateValue);
            });

            $("#startTime").on("dp.change", function (e) {
                $('#endTime').data("DateTimePicker").minDate(e.date);
            });

            $("#endTime").on("dp.change", function (e) {
                $('#startTime').data("DateTimePicker").maxDate(e.date);
            });

            setBreakDatePicker();
        };

        var setTimeOptions = {
            format: 'h:00 a',
            allowInputToggle: true
        };

        var setBreakDatePicker = function () {
            $('#breaks').children().each(function () {
                var fromTo = ['breakFrom', 'breakTo'];
                var breakContent = $(this);

                $.each(fromTo, function (key, value) {
                    var dateTimePicker = breakContent.find('.' + value);
                    if (!dateTimePicker.data('DateTimePicker')) {
                        var dateValue = dateTimePicker.find('input').val();
                        dateTimePicker.datetimepicker(setTimeOptions)
                        dateTimePicker.data('DateTimePicker').date(dateValue);
                    }
                });

                breakContent.find('.' + fromTo[0]).on("dp.change", function (e) {
                    breakContent.find('.' + fromTo[1]).data("DateTimePicker").minDate(e.date);
                });

                breakContent.find('.' + fromTo[1]).on("dp.change", function (e) {
                    breakContent.find('.' + fromTo[0]).data("DateTimePicker").maxDate(e.date);
                });
            });

            if ($('#breaks').children().length) {
                $('#subBreak').show();
            } else {
                $('#subBreak').hide();
            }
        };

        $('#addBreak').click(function () {
            $('#originalBreak').children().first().clone()
                .appendTo('#breaks');

            setBreakDatePicker();
        });

        $('#subBreak').click(function () {
            $('#breaks').children().last().remove();
            __construct();
        });

        __construct();
    });
</script>
@endpush