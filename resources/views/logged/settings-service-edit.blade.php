@extends('logged.settings-service')

@section('form-content')
    <form action="{{ route('settings.service.update', ['offer' => $offer->id]) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        @include('logged.settings-service-form')
    </form>
@endsection