@extends('logged.settings-service')

@section('form-content')
    <form action="{{ route('settings.service.store') }}" method="POST">
        {{ csrf_field() }}
        @include('logged.settings-service-form')
    </form>
@endsection