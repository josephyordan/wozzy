@extends('layouts.client-setting')

@section('setting-title', 'Appointments')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Appointments</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Appointments</span>
        </h1>

        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Services</th>
                    <th>Appointment At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <!-- Single event in a single day -->
                @foreach($appointments as $appointment)
                    <tr>
                        <td> {{ $appointment->user->name }} </td>
                        <td> {{ $appointment->user->email }} </td>
                        <td> {!! $appointment->offerNames()->implode('<br>') !!} </td>
                        <td> {{ $appointment->appointmentTimezone()->format('l, jS F Y h:i A') }} </td>
                        <td>
                            <form action="{{ route('settings.appointment', $appointment->id) }}" method="POST" class="action-confirm">
                                {{ csrf_field() }}

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('settings.appointment', $appointment->id) }}"
                                       class="btn btn-secondary btn-default">
                                        View
                                    </a>
                                    @if($appointment->isDone())
                                        <input type="submit" class="btn btn-secondary btn-success" value="Done">
                                    @else
                                        <input type="submit" class="btn btn-secondary btn-info" value="Mark as done">
                                    @endif
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('scripts.action-confirm')