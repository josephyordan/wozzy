@extends('layouts.client-user-setting')

@section('setting-title', 'User')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">User</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt" id="detail">
            <span>User Credentials</span>
        </h1>

        <form method="POST" action="{{ route('settings.user.user') }}">
            @if(!empty($edit))
                {{ csrf_field() }}
            @endif

            <div class="form-group">
                <label class="control-label">Full name</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Full name" name='name' value="{{ old('name', $user->name) }}" required>
                    @include('inc.help-block-danger', ['error' => 'name'])
                @else
                    <div class="input-lg text-muted">{{ $user->name }}</div>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">Email address</label>
                <div class="input-lg text-muted">{{ $user->email }}</div>
            </div>

            @if(!empty($edit))
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Update Password?
                                <small>Click Here!</small>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label">Old Password</label>
                                <input type="password" class="form-control input-lg" placeholder="Old Password" name="old_password">
                                @include('inc.help-block-danger', ['error' => 'old_password'])
                            </div>
                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" class="form-control input-lg" placeholder="New Password" name="password">
                                @include('inc.help-block-danger', ['error' => 'password'])
                            </div>
                            <div class="form-group">
                                <label class="control-label">Confirm New Password</label>
                                <input type="password" class="form-control input-lg" placeholder="Confirm New Password" name="password_confirmation">
                                @include('inc.help-block-danger', ['error' => 'password_confirmation'])
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary submit">
                    Save changes
                </button>
            @endif
        </form>

        <h1 class="block-header alt" id="contact">
            <span>Contact Info</span>
        </h1>

        <form action="{{ route('settings.user.info') }}" method="POST">
            @if(!empty($edit))
                {{ csrf_field() }}
            @endif

            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="control-label">State</label>
                    @if(!empty($edit))
                        <select class="form-control input-lg" id="addressState" name="state_id" required>
                            <option>Select state</option>
                            @foreach(\GeoLocation::getCountry('US')->getChildren() as $state)
                                @if(old('state_id', $address->state_id) == $state->id)
                                    <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                                @else
                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        @include('inc.help-block-danger', ['error' => 'city_id'])
                    @else
                        <div class="input-lg text-muted">{{ $address->state }}</div>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">City</label>
                    @if(!empty($edit))
                        <select class="form-control input-lg" id="addressCity" name="city_id" required>
                            <option>Select city</option>
                            @if(\GeoLocation::find(old('state_id', $address->state_id)))
                                @foreach(\GeoLocation::find(old('state_id', $address->state_id))->getChildren() as $city)
                                    @if(old('city_id', $address->city_id) == $city->id)
                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                    @else
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        @include('inc.help-block-danger', ['error' => 'city_id'])
                    @else
                        <div class="input-lg text-muted">{{ $address->city }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Postal / Zip Code</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Postal / Zip Code" name="post_code" value="{{ old('post_code', $address->post_code) }}" required>
                    @include('inc.help-block-danger', ['error' => 'post_code'])
                @else
                    <div class="input-lg text-muted">{{ $address->post_code }}</div>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">Address Line1</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Address Line1" name="line1" value="{{ old('line1', $address->line1) }}" required>
                    <span class="help-block small">Street address, P.O. box, company name, c/o</span>
                    @include('inc.help-block-danger', ['error' => 'line1'])
                @else
                    <div class="input-lg text-muted">{{ $address->line1 }}</div>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">Address Line2</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Address Line2" name="line2" value="{{ old('line2', $address->line2) }}">
                    <span class="help-block small">Apartment, suite, unit, building, floor, etc.</span>
                    @include('inc.help-block-danger', ['error' => 'line2'])
                @else
                    <div class="input-lg text-muted">{{ $address->line2 }}</div>
                @endif
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="control-label">Primary Contact</label>
                    @if(!empty($edit))
                        <input type="text" class="form-control input-lg contact1" placeholder="Contact Number" name="primary_contact" value="{{ old('primary_contact', $primaryContact->number) }}" required>
                        @include('inc.help-block-danger', ['error' => 'primary_contact'])
                    @else
                        <div class="input-lg text-muted">{{ $primaryContact->number }}</div>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">Secondary Contact</label>
                    @if(!empty($edit))
                        <input type="text" class="form-control input-lg contact2" placeholder="Contact Number" name="secondary_contact" value="{{ old('secondary_contact', $secondaryContact->number) }}">
                        @include('inc.help-block-danger', ['error' => 'secondary_contact'])
                    @else
                        <div class="input-lg text-muted">{{ $secondaryContact->number }}</div>
                    @endif
                </div>
            </div>


            @if(!empty($edit))
                <button type="submit" class="btn btn-primary submit">
                    Save changes
                </button>
            @endif
        </form>

        <h1 class="block-header alt" id="card">
            <span>Card Credential</span>
        </h1>
        <form action="{{ route('settings.user.card') }}" method="POST" id="payment-form"
              {{ empty($edit) ? 'class=delete-confirm' : '' }}>
            {{ csrf_field() }}
            @if(empty($edit))
                {{ method_field('delete') }}
            @endif

            @if(!empty($edit))
                <h3 class="text-center">
                    <span class="payment-errors label label-danger"></span>
                </h3>

                <div class="row">
                    <div class='form-row'>
                        <div class='col-xs-12 form-group card required'>
                            <label class='control-label'>Card Number</label>
                            <input autocomplete='off' class='form-control card-number input-lg' data-stripe="number" size='20' type='text' required placeholder="{{ !app()->environment('production') ? '4242 4242 4242 4242' : '**** **** **** ****' }}">
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class='col-xs-6 form-group cvc required'>
                            <label class='control-label'>CVC</label>
                            <input autocomplete='off' class='form-control card-cvc input-lg' placeholder='ex. 311' data-stripe="cvc" size='4' type='text' required>
                        </div>
                        <div class='col-xs-6 form-group expiration required'>
                            <label class='control-label'>Expiration MM / YY</label>
                            <input class='form-control card-expiry input-lg' placeholder='MM / YY' data-stripe="exp" type='text' required>
                        </div>
                    </div>
                </div>
            @else
                @if($user->hasCardOnFile())
                    <div class="row">
                        <div class='form-row'>
                            <div class='col-xs-8 form-group card required'>
                                <label class='control-label'>Card Number</label>
                                <div class="input-lg text-muted">**** **** **** {{ $user->card_last_four }}</div>
                            </div>
                        </div>
                        <div class='col-xs-4 form-group cvc required'>
                            <label class='control-label'>Card Brand</label>
                            <div class="input-lg text-muted">{{ $user->card_brand }}</div>
                        </div>
                    </div>
                @else
                    <p class="lead">No Card</p>
                @endif
            @endif

            @if(!empty($edit))
                <button type="submit" class="btn btn-primary submit">
                    Save changes
                </button>
            @else
                @if($user->hasCardOnFile())
                    <input type="hidden" name="delete_card" value="1">
                    <button type="submit" class="btn btn-default submit">
                        Remove Card
                    </button>
                @endif
            @endif
        </form>

        <hr>
        <br>

        @if(empty($edit))
            <a class="btn btn-primary pull-right" href="{{ route('settings.user.edit') }}">
                Edit
            </a>
        @else
            <a class="btn btn-default pull-right" href="{{ route('settings.user') }}">
                Cancel
            </a>
        @endif
    </div>
@endsection

@include('scripts.action-confirm')

@if(!empty($edit))
    @include('scripts.address')
    @include('scripts.billing')
    @include('scripts.form-formatter')
    @push('javascript')
    <script>
        new Cleave('.card-number', {
            creditCard: true
        });

        new Cleave('.card-expiry', {
            date: true,
            datePattern: ['m', 'y']
        });

        new Cleave('.contact1', {
            phone: true,
            phoneRegionCode: 'us'
        });

        new Cleave('.contact2', {
            phone: true,
            phoneRegionCode: 'us'
        });
    </script>
    @endpush
@endif