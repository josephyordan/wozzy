@extends('layouts.client-setting')

@section('setting-title', 'Portfolio')

@section('breadcrumb')
    <li><a href="#">Settings</a></li>
    <li class="active">Portfolio</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Portfolio</span>
        </h1>

        <p class="lead">Business Profile Image</p>
        <div class="row">
            <div class="col-sm-4">
                <p>
                    <a href="{{ $business->picture->url() }}" class="thumbnail alt" data-lightbox="profile">
                        <img src="{{ $business->picture->url() }}" class="img-responsive" id="profileImage" alt="...">
                    </a>
                </p>
            </div>
        </div>

        <div class="picture-upload-content">

            <form action="{{ route('settings.portfolio.upload.picture') }}" method="post">
            {{ csrf_field() }}
            <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-primary fileinput-button">
        <span>Change image...</span>
                    <!-- The file input field used as target for the file upload widget -->
        <input id="pictureUpload" type="file" name="file">
    </span>
                <button class="delete-picture btn btn-default" {{ !$business->picture_file_name ? 'style=display:none' : '' }}>Remove Image</button>
            </form>
            <br>
            <!-- The global progress bar -->
            <div class="progress picture-upload" style="display: none;">
                <div class="progress-bar progress-bar-primary progress-bar-striped"></div>
            </div>
        </div>

        <hr>

        <div class="file-upload-content">

            <form action="{{ route('settings.portfolio.upload') }}" method="post">
            {{ csrf_field() }}
            <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-primary fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Select files...</span>
                    <!-- The file input field used as target for the file upload widget -->
        <input id="fileUpload" type="file" name="files[]" multiple>
    </span>
            </form>
            <br>
            <!-- The global progress bar -->
            <div class="progress file-upload">
                <div class="progress-bar progress-bar-primary progress-bar-striped"></div>
            </div>
            <!-- The container for the uploaded files -->
            <div class="files file-upload hidden"></div>
            <br>
        </div>

        <p class="lead">Files</p>

        <div class="row" id="galleryImage">
            @foreach($business->getImages() as $document)
                @include('logged.settings-portfolio-image', compact('document'))
            @endforeach
        </div> <!-- / .row -->

        <hr>

        <div class="row" id="galleryVideo">
            @foreach($business->getVideos() as $document)
                @include('logged.settings-portfolio-video', compact('document'))
            @endforeach
        </div> <!-- / .row -->
    </div>
@endsection

@push('javascript')
<script src="{{ asset('assets/plugins/lightbox/js/lightbox.min.js') }}"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{{ asset('components/blueimp-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="{{ asset('components/blueimp-file-upload/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('components/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
<script>
    /*jslint unparam: true */
    /*global window, $ */
    $(function () {
        'use strict';

        $('#fileUpload').fileupload({
            dataType: 'json',
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                console.log(data);
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('.file-upload');
                    if (file.type === 'video') {
                        $('#galleryVideo').append(file.view).fadeIn();
                    }
                    if (file.type === 'image') {
                        $('#galleryImage').append(file.view).fadeIn();
                    }
                });
                $('.file-upload .progress-bar').removeClass('active')
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.file-upload .progress-bar').css(
                    'width',
                    progress + '%'
                );
                $('.file-upload .progress-bar').addClass('active')
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $('#pictureUpload').fileupload({
            dataType: 'json',
            add: function (e, data) {
                data.submit();
            },
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('#profileImage').attr('src', file.url);
                });
                $('.picture-upload .progress-bar').removeClass('active');
                $('.picture-upload').hide();
                $('.delete-picture').show();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.picture-upload').show();
                $('.picture-upload .progress-bar').css(
                    'width',
                    progress + '%'
                );
                $('.picture-upload .progress-bar').addClass('active');
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $(document).on('click', '.delete-file', function (e) {
            e.preventDefault();
            var $this = $(this);
            var c = confirm("Are you sure? \nYou won't be able to revert this!");

            if (c) {
                $.ajax({
                    url: '{{ route('settings.portfolio.upload') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        "_method": 'delete',
                        "document": $this.data('id'),
                    }
                }).done(function () {
                        $this.closest('div').fadeOut();
                    }
                )
            }
        });

        $(document).on('click', '.delete-picture', function (e) {
            e.preventDefault();
            var $this = $(this);
            var c = confirm("Are you sure? \nYou won't be able to revert this!");

            if (c) {
                $.ajax({
                    url: '{{ route('settings.portfolio.upload.picture') }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        "_method": 'delete'
                    }
                }).done(function () {
                        $this.hide();
                        $('#profileImage').attr('src', '{{ (new \App\Models\Business())->picture->url()}}')
                    }
                )
            }
        });
    });
</script>
@endpush

@push('css')
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="{{ asset('components/blueimp-file-upload/css/jquery.fileupload.css') }}">
<link href="{{ asset('assets/plugins/lightbox/css/lightbox.css') }}" rel="stylesheet">
@endpush

@include('scripts.html5lightbox')
@include('css.video-wrapper')