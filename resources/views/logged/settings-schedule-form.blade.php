
<div class="form-group">
    <label class="control-label">Days of week available</label>
    @foreach(getDaysArray() as $dayIndex => $dayText)
        <div class="checkbox">
            <input type="checkbox" name="days[]" value="{{ $dayIndex }}" class="form-check-input" id="day{{ $dayIndex }}" {{ in_array($dayIndex, $schedule->days ?: []) ? 'checked' : '' }}>
            <label class="form-check-label" for="day{{ $dayIndex }}">{{ $dayText }}</label>
        </div>
    @endforeach
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label class="control-label">Start Time</label>
        <div class='input-group date' id='startTime'>
            <input type='text' class="form-control" name="starts_at" value="{{ old('starts_at', $schedule->starts_at) }}" required/>
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
        </div>
    </div>

    <div class="form-group col-sm-6">
        <label class="control-label">End Time</label>
        <div class='input-group date' id='endTime'>
            <input type='text' class="form-control" name="ends_at" value="{{ old('ends_at', $schedule->ends_at) }}" required/>
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
        </div>
    </div>
</div>


{{--<h4><label class="control-label">Breaks</label></h4>--}}

{{--<div id="breaks">--}}
    {{--@foreach($schedule->breaks as $break)--}}
        {{--@include('inc.schedule-break-inputs', $break->toArray())--}}
    {{--@endforeach--}}
{{--</div>--}}

{{--<div class="form-group">--}}
    {{--<button id="subBreak" type="button" style="display: none" class="btn btn-default btn-block"><span class="glyphicon glyphicon-minus"></span></button>--}}
    {{--<button id="addBreak" type="button" class="btn btn-default btn-block"><span class="glyphicon glyphicon-plus"></span></button>--}}
{{--</div>--}}