@extends('layouts.client-page')

@section('title')
    Pricing
@endsection

@section('breadcrumb')
    <li><a href="{{ route('home') }}">Home</a></li>
    <li class="active">Pricing</li>
@endsection

@section('content')
    <div class="container">
        @include('logged.settings-plans-content')
    </div>
@overwrite