@extends('logged.settings-schedule')

@section('form-content')
    <form action="{{ route('settings.schedule.store') }}" method="POST">
        {{ csrf_field() }}
        @include('logged.settings-schedule-form')

        <button type="submit" class="btn btn-primary submit">
            Save
        </button>

        <a href="{{ route('settings.schedules') }}" class="btn btn-default">
            Cancel
        </a>
    </form>
@endsection