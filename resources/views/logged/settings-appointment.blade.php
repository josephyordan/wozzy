@extends('layouts.client-setting')

@section('setting-title', 'Appointment Request')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li><a href="{{ route('settings.appointments') }}">Appointments</a></li>
    <li class="active">Appointment Request</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Appointment Request</span>
        </h1>

        <form action="{{ route('settings.appointment', $appointment->id) }}" method="POST" class="action-confirm">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-group col-xs-6">
                    <label class="control-label">Full name</label>
                    <div class="input-lg text-muted">{{ $user->name }}</div>
                </div>
                <div class="form-group">
                    <label class="control-label">Email address</label>
                    <div class="input-lg text-muted">{{ $user->email }}</div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Preffered Time and Date</label>
                <div class="input-lg text-muted">{{ $appointment->appointmentTimezone()->format('l, jS \\of F Y h:i A T') }}</div>
            </div>

            {{--<div class="form-group">--}}
                {{--<label class="control-label">Purpose / Message</label>--}}
                {{--<div class="input-lg text-muted">--}}
                    {{--{{ $appointment->message }}--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="form-group">
                <label class="control-label">Services Availed</label>
                <div class="row">
                    @php($duration = 0)
                    @php($amount = 0)
                    @foreach($appointment->offers as $offer)
                        @php($amount += $offer->getAmountFloat())
                        @php($duration += $offer->getDurationInMinutes())
                        <div class="col-xs-12">
                            <span class="lead">{{ $offer->service->name }}</span>
                            <div class="row">
                                <div class="col-xs-8 text-right">{{ $offer->getAmount(true) }}</div>
                                <div class="col-xs-4 text-right">{{ $offer->getDuration() }}</div>
                            </div>
                            <br>
                        </div>
                    @endforeach
                    @unless(count($appointment->offers))
                        <div class="col-xs-12">
                            <br>
                            <div class="lead">No Service availed</div>
                        </div>
                    @endunless
                    <div class="col-xs-12">
                        <hr>
                    </div>
                    <div class="col-xs-4"><strong>Total</strong></div>
                    <div class="col-xs-4 text-right">{{ intToCurrency($amount) }}</div>
                    <div class="col-xs-4 text-right">{{ durationToString($duration) }}</div>
                </div>
            </div>

            <hr>

            <div class="form-group">
                @if($appointment->isDone())
                    <input type="submit" class="btn btn-secondary btn-success" value="Done">
                @else
                    <input type="submit" class="btn btn-secondary btn-info" value="Mark as done">
                @endif
            </div>
        </form>

    </div>
@endsection

@include('scripts.action-confirm')