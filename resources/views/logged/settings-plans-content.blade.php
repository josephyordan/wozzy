<div class="row">
    <div class="col-sm-12">

        <h1 class="block-header alt">
            <span>Pricing plans</span>
        </h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos pariatur, obcaecati nostrum quam officiis facilis vitae rerum quidem quae dolor ipsum iste, nobis aspernatur suscipit. Quibusdam, dignissimos nihil numquam minima? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam adipisci, perferendis cupiditate ab facilis quis ipsum quibusdam. Aliquid sed eum non aspernatur totam id, deleniti nobis mollitia magnam, recusandae natus!
        </p>

    </div>
</div>
<div class="row">
    @foreach($plans as $plan)
        <div class="col-sm-6">
            <div class="pricing__item
{{ $subscription && $owner->isSubscribedToPlan($plan->id)  ? 'featured' : '' }}
                    ">
                <div class="pricing__header">
                    {{ \App\Models\Plan::getPlan($plan->id, 'name') }} plan
                </div>
                <div class="pricing__price">
                    <i>$</i>{{ $plan->amount / 100 }}<span>/{{ $plan->interval }}</span>
                </div>
                <div class="pricing__features">
                    {{--<ul>--}}
                    {{--<li>--}}
                    {{--Lorem ipsum dolor sit amet, consectetur elit.--}}
                    {{--</li>--}}
                    {{--<li>Beatae reprehenderit</li>--}}
                    {{--<li>Sapiente omnis fuga</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="pricing__btn">
                    @if($subscription && $owner->isSubscribedToPlan($plan->id))
                        <a href="#" class="btn btn-default">
                            Current Plan!
                        </a>
                    @else
                        <form action="{{ route('settings.subscribe.plan') }}" method="POST" id="payment-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="plan" value="{{ $plan->id }}">
                            <button type="submit" class="btn btn-primary">
                                Subscribe!
                            </button>
                        </form>
                    @endif
                </div>
            </div>

        </div>
    @endforeach
</div> <!-- / .row -->

@unless($owner->hasStripeId() && $owner->hasCardOnFile())
    <div class="alert alert-info">
        <p class="text-muted">Please add card info in <a href="{{ route('settings.user.edit') }}#card">User Settings</a>.</p>
    </div>
@endunless
