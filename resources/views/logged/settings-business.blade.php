@extends('layouts.client-setting')

@section('setting-title', 'Business')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Business</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt" id="detail">
            <span>Business</span>
        </h1>

        <form action="{{ route('settings.business.update') }}" method="POST">
            @if(!empty($edit))
                {{ csrf_field() }}
            @endif
            <div class="form-group">
                <label class="control-label">Business Name</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Business Name" name="name" value="{{ old('name', $business->name) }}" required>
                    @include('inc.help-block-danger', ['error' => 'name'])
                @else
                    <div class="input-lg text-muted">{{ $business->name }}</div>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">Categories</label>
                @if(!empty($edit))
                    @foreach(\App\Models\Category::getBySort() as $category)
                        <div class="checkbox">
                            <input type="checkbox" name="categories[]" value="{{ $category->id }}" class="form-check-input" id="category{{ $category->id }}" {{ in_array($category->id, $business->getCategoryIds()) ? 'checked' : '' }}>
                            <label class="form-check-label" for="category{{ $category->id }}">{{ $category->name }}</label>
                        </div>
                    @endforeach
                @else
                    @if($business->categories)
                        <div class="input-lg text-muted">
                            {{ implode(', ', $business->categories->pluck('name')->toArray()) }}
                        </div>
                    @endif
                @endif
            </div>

            <div class="form-group">
                <label class="control-label" id="tinymce_placeholder">Description</label>
                @if(!empty($edit))
                    <textarea id="body" class="richText" name="description" placeholder="">{!! old('description', $business->description) !!}</textarea>
                    @include('inc.help-block-danger', ['error' => 'description'])
                @else
                    <div class="input-lg text-muted">{!! $business->description !!}</div>
                @endif
            </div>


            @if(!empty($edit))
                <button type="submit" class="btn btn-primary submit">
                    Save changes
                </button>
            @endif
        </form>

        <h1 class="block-header alt" id="contact">
            <span>Business Info</span>
        </h1>

        <form action="{{ route('settings.business.info') }}" method="POST">
            @if(!empty($edit))
                {{ csrf_field() }}
            @endif

            <div class="form-group">
                <label class="control-label">Timezone</label>
                @if(!empty($edit))
                    {!! Timezonelist::create(
                    'timezone', $business->timezone, 'class="form-control input-lg"'
                    ) !!}
                    @include('inc.help-block-danger', ['error' => 'timezone'])
                @else
                    <div class="input-lg text-muted">{{ $business->timezone }}</div>
                @endif
            </div>

            <div class="row">

                <div class="form-group col-sm-6">
                    <label class="control-label">State</label>
                    @if(!empty($edit))
                        <select class="form-control input-lg" id="addressState" name="state_id" required>
                            <option>Select state</option>
                            @foreach(\GeoLocation::getCountry('US')->getChildren() as $state)
                                @if(old('state_id', $address->state_id) == $state->id)
                                    <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                                @else
                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        @include('inc.help-block-danger', ['error' => 'city_id'])
                    @else
                        <div class="input-lg text-muted">{{ $address->state }}</div>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">City</label>
                    @if(!empty($edit))
                        <select class="form-control input-lg" id="addressCity" name="city_id" required>
                            <option>Select city</option>
                            @if(\GeoLocation::find(old('state_id', $address->state_id)))
                                @foreach(\GeoLocation::find(old('state_id', $address->state_id))->getChildren() as $city)
                                    @if(old('city_id', $address->city_id) == $city->id)
                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                    @else
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        @include('inc.help-block-danger', ['error' => 'city_id'])
                    @else
                        <div class="input-lg text-muted">{{ $address->city }}</div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Postal / Zip Code</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Postal / Zip Code" name="post_code" value="{{ old('post_code', $address->post_code) }}" required>
                    @include('inc.help-block-danger', ['error' => 'post_code'])
                @else
                    <div class="input-lg text-muted">{{ $address->post_code }}</div>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">Address Line1</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Address Line1" name="line1" value="{{ old('line1', $address->line1) }}" required>
                    <span class="help-block small">Street address, P.O. box, company name, c/o</span>
                    @include('inc.help-block-danger', ['error' => 'line1'])
                @else
                    <div class="input-lg text-muted">{{ $address->line1 }}</div>
                @endif
            </div>

            <div class="form-group">
                <label class="control-label">Address Line2</label>
                @if(!empty($edit))
                    <input type="text" class="form-control input-lg" placeholder="Address Line2" name="line2" value="{{ old('line2', $address->line2) }}">
                    <span class="help-block small">Apartment, suite, unit, building, floor, etc.</span>
                    @include('inc.help-block-danger', ['error' => 'line2'])
                @else
                    <div class="input-lg text-muted">{{ $address->line2 }}</div>
                @endif
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="control-label">Primary Contact</label>
                    @if(!empty($edit))
                        <input type="text" class="form-control input-lg contact1" placeholder="Contact Number" name="primary_contact" value="{{ old('primary_contact', $primaryContact->number) }}" required>
                        @include('inc.help-block-danger', ['error' => 'primary_contact'])
                    @else
                        <div class="input-lg text-muted">{{ $primaryContact->number }}</div>
                    @endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label">Secondary Contact</label>
                    @if(!empty($edit))
                        <input type="text" class="form-control input-lg contact2" placeholder="Contact Number" name="secondary_contact" value="{{ old('secondary_contact', $secondaryContact->number) }}">
                        @include('inc.help-block-danger', ['error' => 'secondary_contact'])
                    @else
                        <div class="input-lg text-muted">{{ $secondaryContact->number }}</div>
                    @endif
                </div>
            </div>


            @if(!empty($edit))
                <button type="submit" class="btn btn-primary submit">
                    Save changes
                </button>
            @endif
        </form>

        @if(empty($edit))
            <h1 class="block-header alt" id="contact">
                <span>Business Publish</span>
            </h1>

            <form action="{{ route('settings.business.publish') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="control-label">Publish to public</label>
                    <span class="help-block small text-muted">Public customer can see your business.</span>
                    <div class="input-lg">
                        <input type="hidden" name="publish" value="{{ !$business->isPublish() ?: '0' }}">
                        @if($business->isPublish())
                            <button type="submit" class="btn btn-default">
                                Unpublish
                            </button>
                        @else
                            <button type="submit" class="btn btn-primary">
                                Publish
                            </button>
                        @endif
                    </div>
                    @if(!$business->isActive())
                        <span class="help-block small text-muted">The publish will not work if not <span class="text-primary">activated / subscribed</span>.</span>
                    @endif
                </div>
                <div class="form-group">
                    <label class="control-label">Status</label>
                    <div class="input-lg">
                        @if($business->isActive())
                            <p class="text-success lead">Activated</p>
                        @else
                            <p class="text-danger lead">Deactivated</p>
                        @endif
                    </div>
                    @if($business->isActive())
                        <span class="help-block small text-muted"><a href="{{ route('settings.subscribe') }}#link">Unsubscribe</a> to deactivate.</span>
                    @else
                        <span class="help-block small text-muted">Please <a href="{{ route('settings.subscribe') }}#link">Subscribe</a> to activate.</span>
                    @endif
                </div>
            </form>
        @endif

        <hr>
        <br>

        @if(empty($edit))
            <a class="btn btn-primary pull-right" href="{{ route('settings.business.edit') }}">
                Edit
            </a>
        @else
            <a class="btn btn-default pull-right" href="{{ route('settings.business') }}">
                Cancel
            </a>
        @endif
    </div>
@endsection

@include('scripts.action-confirm')

@if(!empty($edit))
    @include('scripts.address')
    @include('scripts.form-formatter')
    @push('javascript')
    <script>
        new Cleave('.contact1', {
            phone: true,
            phoneRegionCode: 'us'
        });

        new Cleave('.contact2', {
            phone: true,
            phoneRegionCode: 'us'
        });
    </script>
    @endpush
@endif

@push('javascript')
<script src="{{ asset('components/tinymce/tinymce.min.js') }}"></script>
<script>
    // Initiate the tinymce editor on any textarea with a class of richText
    tinymce.init({
        selector: 'textarea.richText',
        menubar: false,
        statusbar: false,
        height: '220'
    });

    var my_tinymce = tinyMCE;
    $('document').ready(function () {
        $('#tinymce_placeholder').click(function () {
            my_tinymce.activeEditor.focus();
        });
    });
</script>
@endpush

@if(empty($edit))
    @push('css')
    <style>
        .input-lg {
            height: auto;
            min-height: 40px;
        }
    </style>
    @endpush
@endif