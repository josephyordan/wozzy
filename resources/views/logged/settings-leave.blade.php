@extends('layouts.client-setting')

@section('setting-title', 'Leave')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li><a href="{{ route('settings.leaves') }}">Leaves</a></li>
    <li class="active">Service</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Service</span>
        </h1>

        @yield('form-content')
    </div>
@endsection

@push('css')
<link rel="stylesheet" href="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@include('scripts.form-formatter')

@push('javascript')
<script src="{{ asset('components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">
    $(function () {
        var __construct = function () {
            $('#absent').each(function () {
                var dateValue = $(this).find('input').val();
                $(this).datetimepicker(setTimeOptions);
                $(this).data("DateTimePicker").date(dateValue);
            });
        };

        var setTimeOptions = {
            format: 'll',
            defaultDate: moment().startOf('day'),
            allowInputToggle: true
        };

        __construct();
    });
</script>
@endpush