@extends('layouts.client-setting')

@section('setting-title', 'Members')

@section('breadcrumb')
    <li><a href="{{ route('settings') }}">Settings</a></li>
    <li class="active">Members</li>
@endsection

@section('content')
    <div>
        <h1 class="block-header alt">
            <span>Members</span>
        </h1>

        <p>
            <a href="{{ route('settings.member.create') }}" class="btn btn-lg btn-primary">
                Create
            </a>
        </p>

        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                <tr>
                    <th>User</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <!-- Single event in a single day -->
                @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $user->name }}
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                        <td>
                            <form action="{{ route('settings.member.delete', $user->id) }}" method="POST" class="delete-confirm">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}

                                <div class="btn-group btn-group-xs" role="group">
                                    <a href="{{ route('settings.member', $user->id) }}"
                                       class="btn btn-secondary btn-default">
                                        Edit
                                    </a>
                                    <input type="submit" class="btn btn-secondary btn-danger" value="Delete">
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('scripts.action-confirm')