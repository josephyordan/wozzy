@php($deleteFile = orHelper($deleteFile, false))
<div class="col-xs-6 col-sm-4 col-lg-4 isotope-item ">

    @if($deleteFile)
        <a href="#" data-id="{{ $document->id }}" class="btn-sm text-danger delete-file" style="float: right; overflow: hidden"><i class="glyphicon glyphicon-remove"></i></a>
    @endif
    <a href="JavaScript:html5Lightbox.showLightbox(2, '{{ $document->file->url() }}', '', 480, 270, '{{ $document->file->url() }}');" class="thumbnail alt">
        <span class="video-wrapper">
        <img src="{{ $document->getPrimaryImage()->file->url('gallery') }}" alt="..." class="img-responsive">
        </span>
    </a>

</div>