@extends('logged.settings-leave')

@section('form-content')
    <form action="{{ route('settings.leave.store') }}" method="POST">
        {{ csrf_field() }}
        @include('logged.settings-leave-form')
    </form>
@endsection