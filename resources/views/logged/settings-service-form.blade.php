<div class="form-group">
    <label class="control-label">Service Name</label>
    <select class="form-control input-lg" id="service" name="service" required>
        <option value="">Select a service</option>
        @foreach($serviceList as $item)
            @if(old('service', $item->id) == $offer->service_id)
                <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
            @else
                <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endif
        @endforeach
        <option value="other">Other Service</option>
    </select>
    <br>
    <input type='text' class="form-control" id="otherService" name="other_service" value="{{ old('other_service') }}" placeholder="Other Service"/>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label class="control-label">Service Duration</label>
        <div class='input-group date' id='duration'>
            <input type='text' class="form-control" name="duration" value="{{ old('duration', $offer->duration) }}" required/>
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        <label class="control-label">Service Amount</label>
        <input type='text' class="form-control amount text-right" name="amount" value="{{ old('amount', $offer->amount) }}" required/>
    </div>
</div>
</div>

@can('subscribeToTeam', $business)
    <div class="form-group">
        <label class="control-label">Users</label>
        @foreach($business->users as $user)
            <div class="checkbox">
                <input type="checkbox" name="users[]" value="{{ $user->id }}" class="form-check-input" id="user{{ $user->id }}" {{ $offer->users->isNotEmpty() && in_array($user->id, $offer->getUsers()->pluck('id')->toArray() ?: []) ? 'checked' : '' }}>
                <label class="form-check-label" for="user{{ $user->id }}">{{ $user->name }}</label>
            </div>
        @endforeach
    </div>
@endcan

<button type="submit" class="btn btn-primary submit">
    Save
</button>

<a href="{{ route('settings.services') }}" class="btn btn-default">
    Cancel
</a>