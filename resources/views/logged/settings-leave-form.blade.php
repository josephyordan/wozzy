@can('subscribeToTeam', $business)
    <div class="form-group">
        <label class="control-label">User</label>
        <select class="form-control input-lg" name="user" required>
            <option value="">Select a service</option>
            @foreach($users as $item)
                @if(old('user', $item->id) == $absent->user_id)
                    <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endif
            @endforeach
        </select>
    </div>
@endcan

<div class="row">
    <div class="form-group col-sm-6">
        <label class="control-label">Date</label>
        <div class='input-group date' id='absent'>
            <input type='text' class="form-control" name="absent_at" value="{{ old('absent_at', $absent->absent_at ? $absent->absent_at->toFormattedDateString() : null) }}" required/>
            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
        </div>
    </div>
</div>

<button type="submit" class="btn btn-primary submit">
    Save
</button>

<a href="{{ route('settings.leaves') }}" class="btn btn-default">
    Cancel
</a>