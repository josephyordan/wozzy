@extends('logged.settings-member')

@section('form-content')
    <form action="{{ route('settings.member.store') }}" method="POST">
        {{ csrf_field() }}
        @include('logged.settings-member-form')

        <button type="submit" class="btn btn-primary submit">
            Save
        </button>

        <a href="{{ route('settings.members') }}" class="btn btn-default">
            Cancel
        </a>
    </form>
@endsection