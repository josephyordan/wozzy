@extends('logged.settings-step')

@section('step', '1')
@section('content-title', 'Business Info')

@section('content')
    <div class="form-group">
        <label class="control-label">Business Name</label>
        <input type="text" class="form-control input-lg" placeholder="Business Name" name="name" value="{{ old('name', $business->name) }}" required>
        @include('inc.help-block-danger', ['error' => 'name'])
    </div>

    <div class="form-group">
        <label class="control-label">Categories</label>
        @foreach(\App\Models\Category::getBySort() as $category)
            <div class="checkbox">
                <input type="checkbox" name="categories[]" value="{{ $category->id }}" class="form-check-input" id="category{{ $category->id }}" {{ in_array($category->id, $business->getCategoryIds()) ? 'checked' : '' }}>
                <label class="form-check-label" for="category{{ $category->id }}">{{ $category->name }}</label>
            </div>
        @endforeach
    </div>

    <div class="form-group">
        <label class="control-label" id="tinymce_placeholder">Description</label>
        <textarea id="body" class="richText" name="description" placeholder="">{!! old('description', $business->description) !!}</textarea>
        @include('inc.help-block-danger', ['error' => 'description'])
    </div>

    <hr>

    <div class="form-group">
        <label class="control-label">Timezone</label>
        {!! Timezonelist::create(
        'timezone', $business->timezone, 'class="form-control input-lg"'
        ) !!}
        @include('inc.help-block-danger', ['error' => 'timezone'])
    </div>

    <div class="row">

        <div class="form-group col-sm-6">
            <label class="control-label">State</label>
            <select class="form-control input-lg" id="addressState" name="state_id" required>
                <option>Select state</option>
                @foreach(\GeoLocation::getCountry('US')->getChildren() as $state)
                    @if(old('state_id', $address->state_id) == $state->id)
                        <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                    @else
                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                    @endif
                @endforeach
            </select>
            @include('inc.help-block-danger', ['error' => 'city_id'])
        </div>

        <div class="form-group col-sm-6">
            <label class="control-label">City</label>
            <select class="form-control input-lg" id="addressCity" name="city_id" required>
                <option>Select city</option>
                @if(\GeoLocation::find(old('state_id', $address->state_id)))
                    @foreach(\GeoLocation::find(old('state_id', $address->state_id))->getChildren() as $city)
                        @if(old('city_id', $address->city_id) == $city->id)
                            <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                        @else
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endif
                    @endforeach
                @endif
            </select>
            @include('inc.help-block-danger', ['error' => 'city_id'])
        </div>
    </div>

    <div class="form-group">
        <label class="control-label">Postal / Zip Code</label>
        <input type="text" class="form-control input-lg" placeholder="Postal / Zip Code" name="post_code" value="{{ old('post_code', $address->post_code) }}" required>
        @include('inc.help-block-danger', ['error' => 'post_code'])
    </div>

    <div class="form-group">
        <label class="control-label">Address Line1</label>
        <input type="text" class="form-control input-lg" placeholder="Address Line1" name="line1" value="{{ old('line1', $address->line1) }}" required>
        <span class="help-block small">Street address, P.O. box, company name, c/o</span>
        @include('inc.help-block-danger', ['error' => 'line1'])
    </div>

    <div class="form-group">
        <label class="control-label">Address Line2</label>
        <input type="text" class="form-control input-lg" placeholder="Address Line2" name="line2" value="{{ old('line2', $address->line2) }}">
        <span class="help-block small">Apartment, suite, unit, building, floor, etc.</span>
        @include('inc.help-block-danger', ['error' => 'line2'])
    </div>

    <div class="row">
        <div class="form-group col-sm-6">
            <label class="control-label">Primary Contact</label>
            <input type="text" class="form-control input-lg contact1" placeholder="Contact Number" name="primary_contact" value="{{ old('primary_contact', $primaryContact->number) }}" required>
            @include('inc.help-block-danger', ['error' => 'primary_contact'])
        </div>

        <div class="form-group col-sm-6">
            <label class="control-label">Secondary Contact</label>
            <input type="text" class="form-control input-lg contact2" placeholder="Contact Number" name="secondary_contact" value="{{ old('secondary_contact', $secondaryContact->number) }}">
            @include('inc.help-block-danger', ['error' => 'secondary_contact'])
        </div>
    </div>

    <hr>

    <button type="submit" class="btn btn-primary submit">
        Save
    </button>
@endsection

@include('scripts.action-confirm')

@if(!empty($edit))
    @include('scripts.address')
    @include('scripts.form-formatter')
    @push('javascript')
    <script>
        new Cleave('.contact1', {
            phone: true,
            phoneRegionCode: 'us'
        });

        new Cleave('.contact2', {
            phone: true,
            phoneRegionCode: 'us'
        });
    </script>
    @endpush
@endif

@push('javascript')
<script src="{{ asset('components/tinymce/tinymce.min.js') }}"></script>
<script>
    // Initiate the tinymce editor on any textarea with a class of richText
    tinymce.init({
        selector: 'textarea.richText',
        menubar: false,
        statusbar: false,
        height: '220'
    });

    var my_tinymce = tinyMCE;
    $('document').ready(function () {
        $('#tinymce_placeholder').click(function () {
            my_tinymce.activeEditor.focus();
        });
    });
</script>
@endpush