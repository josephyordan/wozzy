<div class="form-group">
    <label class="control-label">Full name</label>
    <input type="text" class="form-control input-lg" placeholder="Full name" name='name' value="{{ old('name', $user->name) }}" required>
    @include('inc.help-block-danger', ['error' => 'name'])
</div>

<div class="form-group">
    <label class="control-label">E-mail</label>
    @if(!empty($edit))
        <input type="email" class="form-control input-lg" placeholder="E-mail" name="email" value="{{ old('email', $user->email) }}" required>
        @include('inc.help-block-danger', ['error' => 'email'])
    @else
        <div class="input-lg text-muted">{{ $user->email }}</div>
    @endif
</div>
