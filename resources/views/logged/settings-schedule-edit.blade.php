@extends('logged.settings-schedule')

@section('form-content')
    <form action="{{ route('settings.schedule.update', ['schedule' => $schedule->id]) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}
        @include('logged.settings-schedule-form')

        <button type="submit" class="btn btn-primary submit">
            Update
        </button>

        <a href="{{ route('settings.schedules') }}" class="btn btn-default">
            Cancel
        </a>
    </form>
@endsection