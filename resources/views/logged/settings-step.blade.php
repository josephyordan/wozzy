@extends('layouts.client-page')

@section('title')
    Business Step @yield('step')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Step @yield('step') - 2: @yield('content-title')
                    </div>
                    <div class="panel-body">
                        <form action="{{ route('settings.business.step.store', Request::route('step')) }}" method="POST">
                            {{ csrf_field() }}
                            @yield('content')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@overwrite