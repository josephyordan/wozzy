@component('mail::message')
# Good Day

You have requested an appointment in **{{ $appointment->business->name }}** at **{{ $appointment->appointment_at->format('l jS \\of F Y h:i A') }}**.

**Location:** {{ implode(', ', $appointment->business->getPrimaryAddress()->address->toArray()) }}

@component('mail::button', ['url' => ''])
View Online
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
