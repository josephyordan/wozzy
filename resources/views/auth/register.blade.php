@extends('layouts.client-page')

@section('title', 'Sign Up')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">

                <!-- Sign In form -->
                <div class="profile__sign-in">

                    <h1 class="block-header alt">
                        <span>Create a new account</span>
                    </h1>

                    <form method="POST" action="{{ route('register') }}" id="payment-form">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="sr-only">Full name</label>
                            <input type="text" class="form-control input-lg" placeholder="Full name" name="name" value="{{ old('name') }}" required autofocus>
                            @include('inc.help-block-danger', ['error' => 'name'])
                        </div>
                        <div class="form-group">
                            <label class="sr-only">E-mail</label>
                            <input type="email" class="form-control input-lg" placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>
                            @include('inc.help-block-danger', ['error' => 'email'])
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Password</label>
                            <input type="password" class="form-control input-lg" placeholder="Password" name="password" required>
                            @include('inc.help-block-danger', ['error' => 'password'])
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Confirm Password</label>
                            <input type="password" class="form-control input-lg" placeholder="Repeat password" name="password_confirmation" required>
                            @include('inc.help-block-danger', ['error' => 'password_confirmation'])
                        </div>

                        <hr>

                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">State</label>
                                        <select class="form-control input-lg" id="addressState" name="state_id" required>
                                            <option>Select state</option>
                                            @foreach(\GeoLocation::getCountry('US')->getChildren() as $state)
                                                @if(old('state_id') == $state->id)
                                                    <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                                                @else
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @include('inc.help-block-danger', ['error' => 'city_id'])
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="control-label">City</label>
                                        <select class="form-control input-lg" id="addressCity" name="city_id" required>
                                            <option>Select city</option>
                                            @if(\GeoLocation::find(old('state_id')))
                                                @foreach(\GeoLocation::find(old('state_id'))->getChildren() as $city)
                                                    @if(old('city_id') == $city->id)
                                                        <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                                    @else
                                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        @include('inc.help-block-danger', ['error' => 'city_id'])
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Postal / Zip Code</label>
                                    <input type="text" class="form-control input-lg" placeholder="Postal / Zip Code" name="post_code" value="{{ old('post_code') }}" required>
                                    @include('inc.help-block-danger', ['error' => 'post_code'])
                            </div>

                            <div class="form-group">
                                <label class="control-label">Address Line1</label>
                                    <input type="text" class="form-control input-lg" placeholder="Address Line1" name="line1" value="{{ old('line1') }}" required>
                                    <span class="help-block small">Street address, P.O. box, company name, c/o</span>
                                    @include('inc.help-block-danger', ['error' => 'line1'])
                            </div>

                            <div class="form-group">
                                <label class="control-label">Address Line2</label>
                                    <input type="text" class="form-control input-lg" placeholder="Address Line2" name="line2" value="{{ old('line2') }}">
                                    <span class="help-block small">Apartment, suite, unit, building, floor, etc.</span>
                                    @include('inc.help-block-danger', ['error' => 'line2'])
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Primary Contact</label>
                                        <input type="text" class="form-control input-lg contact1" placeholder="Contact Number" name="primary_contact" value="{{ old('primary_contact') }}" required>
                                        @include('inc.help-block-danger', ['error' => 'primary_contact'])
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="control-label">Secondary Contact</label>
                                        <input type="text" class="form-control input-lg contact2" placeholder="Contact Number" name="secondary_contact" value="{{ old('secondary_contact') }}">
                                        @include('inc.help-block-danger', ['error' => 'secondary_contact'])
                                </div>
                            </div>

                        <hr>


                                <h3 class="text-center">
                                    <span class="payment-errors label label-danger"></span>
                                </h3>

                                <div class="row">
                                    <div class='form-row'>
                                        <div class='col-xs-12 form-group card required'>
                                            <label class='control-label'>Card Number</label>
                                            <input autocomplete='off' class='form-control card-number input-lg' data-stripe="number" size='20' type='text' required placeholder="{{ !app()->environment('production') ? '4242 4242 4242 4242' : '**** **** **** ****' }}">
                                        </div>
                                    </div>
                                    <div class='form-row'>
                                        <div class='col-xs-6 form-group cvc required'>
                                            <label class='control-label'>CVC</label>
                                            <input autocomplete='off' class='form-control card-cvc input-lg' placeholder='ex. 311' data-stripe="cvc" size='4' type='text' required>
                                        </div>
                                        <div class='col-xs-6 form-group expiration required'>
                                            <label class='control-label'>Expiration MM / YY</label>
                                            <input class='form-control card-expiry input-lg' placeholder='MM / YY' data-stripe="exp" type='text' required>
                                        </div>
                                    </div>
                                </div>


                        <div class="checkbox">
                            <input type="checkbox" id="register-is_merchant" name="merchant" {{ old('merchant') ? 'checked' : '' }}>
                            <label for="register-is_merchant">
                                Register as Business Owner?
                            </label>
                        </div>


                        <br />
                        <button type="submit" class="btn btn-lg btn-primary">
                            Sign Up
                        </button>
                    </form>

                    <hr>

                    <p>
                        Already registered? <a href="{{ route('login') }}">Sign in to your account</a>.
                    </p>

                </div> <!-- / .profile_sign-in -->

            </div>
        </div> <!-- / .row -->
    </div>
@endsection


@include('scripts.address')
@include('scripts.billing')
@include('scripts.form-formatter')
@push('javascript')
<script>
    new Cleave('.card-number', {
        creditCard: true
    });

    new Cleave('.card-expiry', {
        date: true,
        datePattern: ['m', 'y']
    });

    new Cleave('.contact1', {
        phone: true,
        phoneRegionCode: 'us'
    });

    new Cleave('.contact2', {
        phone: true,
        phoneRegionCode: 'us'
    });
</script>
@endpush