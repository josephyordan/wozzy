@extends('layouts.client-page')

@section('title', 'Sign In')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">

                <!-- Sign In form -->
                <div class="profile__sign-in">

                    <h1 class="block-header alt">
                        <span>Sign in to your account</span>
                    </h1>

                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="sr-only">E-mail</label>
                            <input type="email" class="form-control input-lg" placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>
                            @include('inc.help-block-danger', ['error' => 'email'])
                        </div>
                        <div class="form-group">
                            <label class="sr-only">Password</label>
                            <input type="password" class="form-control input-lg" placeholder="Password" name="password" required>
                            @include('inc.help-block-danger', ['error' => 'password'])
                        </div>
                        <div class="checkbox"><br />
                            <input type="checkbox" id="profile-sign-in__remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="profile-sign-in__remember">
                                Remember me
                            </label>
                        </div><br />
                        <button type="submit" class="btn btn-lg btn-primary">
                            Sign In
                        </button>
                    </form>

                    <hr>

                    <p>
                        Not registered? <a href="{{ route('register') }}">Create an account</a>.
                    </p>
                    <p>
                        Lost your password? <a href="#lost-password__form" data-toggle="collapse" aria-expanded="false" aria-controls="lost-password__form" class="collapsed">
                            Click here to recover
                        </a>.
                    </p>

                    <div class="collapse" id="lost-password__form">

                        <p class="text-muted">
                            Enter your email address below and we will send you a link to reset your password.
                        </p>

                        <!-- Lost password -->
                        <form class="form-inline" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="sr-only" for="lost-password__email">Email address</label>
                                <input type="email" class="form-control" id="lost-password__email" placeholder="Enter email" name="email" value="{{ old('email') }}" required>
                                @include('inc.help-block-danger', ['error' => 'email'])
                            </div>
                            <button type="submit" class="btn btn-primary">Send</button>
                        </form>

                    </div> <!-- / #lost-password__form -->

                </div> <!-- / .profile_sign-in -->

            </div>
        </div> <!-- / .row -->
    </div>
@endsection
