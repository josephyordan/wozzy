@extends('layouts.client-page')

@section('title', 'Reset Password')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">

                <!-- Sign In form -->
                <div class="profile__sign-in">

                    <h1 class="block-header alt">
                        <span>Reset Password</span>
                    </h1>

                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="sr-only">E-mail</label>
                            <input type="email" class="form-control input-lg" placeholder="E-mail" name="email" value="{{ old('email') }}" required autofocus>
                            @include('inc.help-block-danger', ['error' => 'email'])
                        </div><br />
                        <button type="submit" class="btn btn-lg btn-primary">
                            Send Password Reset Link
                        </button>
                    </form>

                </div> <!-- / .profile_sign-in -->

            </div>
        </div> <!-- / .row -->
    </div>
@endsection
