<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->delete();
    DB::table('businesses')->delete();

    $faker = Faker\Factory::create();
    $role = Role::firstOrCreate(['name' => 'admin']);
    if (!$role->hasPermissionTo('admin')) {
      $role->givePermissionTo('admin');
    }

    $user = User::firstOrNew(['email' => 'admin@example.com']);
    $user->name = $faker->name('male');
    $user->password = bcrypt('secret');
    $user->save();
    $user->syncRoles([$role->name]);

    $user = User::firstOrNew(['email' => 'user@example.com']);
    $user->name = $faker->name('male');
    $user->password = bcrypt('secret');
    $user->save();

    $user = User::firstOrNew(['email' => 'merchant@example.com']);
    $user->name = $faker->name('male');
    $user->password = bcrypt('secret');
    $user->save();
    $user->asMerchant();
  }
}
