<?php

use Illuminate\Database\Seeder;
use \Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $data = [
      ['name' => 'superadmin'],
      ['name' => 'admin'],
    ];

    foreach ($data as $item) {
      Permission::firstOrCreate($item);
    }
  }
}
