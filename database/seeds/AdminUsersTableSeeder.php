<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;

class AdminUsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();
    $role = Role::firstOrCreate(['name' => 'superadmin']);
    if (!$role->hasPermissionTo('superadmin')) {
      $role->givePermissionTo('superadmin');
    }

    $user = User::firstOrNew(['email' => 'josephy@codev.com']);
    $user->name = $faker->name('male');
    if (App::environment('production')) {
      $user->password = bcrypt($faker->password);
    } else {
      $user->password = bcrypt('secret');
    }
    $user->save();
    $user->syncRoles([$role->name]);
  }
}
