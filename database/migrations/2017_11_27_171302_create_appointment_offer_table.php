<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentOfferTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('appointment_offer', function (Blueprint $table) {
      $table->integer('appointment_id')->unsigned();
      $table->integer('offer_id')->unsigned();

      $table->foreign('appointment_id')->references('id')->on('appointments')
        ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('offer_id')->references('id')->on('offers')
        ->onUpdate('cascade')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('appointment_offer');
  }
}
