<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActiveAndPublichChangeColumnBusinessTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('businesses', function (Blueprint $table) {
      $table->timestamp('activated_at')->nullable();
      $table->timestamp('published_at')->nullable();
      $table->dropColumn('is_active');
      $table->dropColumn('is_publish');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('businesses', function (Blueprint $table) {
      $table->boolean('is_active')->default(false);
      $table->boolean('is_publish')->default(false);
      $table->dropColumn('activated_at');
      $table->dropColumn('published_at');
    });
  }
}
