<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeActivatedAtToEndsAtBusinessTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('businesses', function (Blueprint $table) {
      $table->timestamp('ends_at')->nullable()->default(\DB::raw('CURRENT_TIMESTAMP'));
      $table->dropColumn('activated_at');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('businesses', function (Blueprint $table) {
      $table->timestamp('activated_at')->nullable();
      $table->dropColumn('ends_at');
    });
  }
}
