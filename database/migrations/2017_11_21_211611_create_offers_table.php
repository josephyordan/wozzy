<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('offers', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('business_id')->unsigned();
      $table->integer('service_id')->unsigned();
      $table->string('amount');
      $table->string('duration');

      $table->foreign('service_id')->references('id')->on('services')
        ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('business_id')->references('id')->on('businesses')
        ->onUpdate('cascade')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('offers');
  }
}
