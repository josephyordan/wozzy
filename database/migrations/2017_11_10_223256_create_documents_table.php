<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('documents', function (Blueprint $table) {
      $table->increments('id');
      $table->string('file_file_name')->nullable();
      $table->integer('file_file_size')->nullable();
      $table->string('file_content_type')->nullable();
      $table->timestamp('file_updated_at')->nullable();
      $table->boolean('is_primary')->default(false)->index();
      $table->integer('sort')->nullable();

      $table->nullableMorphs('documentable');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('documents');
  }
}
