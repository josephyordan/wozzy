<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessToUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users', function ($table) {
      $table->integer('business_id')->unsigned()->nullable();

      $table->foreign('business_id')->references('id')->on('businesses')
        ->onUpdate('cascade')->onDelete('set null');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::disableForeignKeyConstraints();
    Schema::table('users', function (Blueprint $table) {
      $table->dropForeign('users_business_id_foreign');
      $table->dropColumn('business_id');
    });
    Schema::enableForeignKeyConstraints();
  }
}
