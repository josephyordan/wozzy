<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('business_schedules', function (Blueprint $table) {
      $table->increments('id');
      $table->text('days')->nullable();
      $table->string('starts_at');
      $table->string('ends_at');
      $table->integer('business_id')->unsigned();

      $table->foreign('business_id')->references('id')->on('businesses')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('business_schedules');
  }
}
