<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToBusinessTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('businesses', function (Blueprint $table) {
      $table->boolean('is_active')->default(false);
      $table->boolean('is_publish')->default(false);
      $table->dropColumn('contact');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('businesses', function (Blueprint $table) {
      $table->dropColumn('is_active');
      $table->dropColumn('is_publish');
      $table->string('contact');
    });
  }
}
