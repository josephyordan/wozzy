<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('addresses', function (Blueprint $table) {
      $table->increments('id');

      $table->string('line1', 100)->nullable();
      $table->string('line2', 100)->nullable();
      $table->integer('city_id')->nullable()->unsigned()->index();
      $table->integer('state_id')->nullable()->unsigned()->index();
      $table->string('post_code', 10)->nullable();
      $table->integer('country_id')->nullable()->unsigned()->index();
      $table->string('note')->nullable();

      $table->nullableMorphs('addressable');

      foreach (['primary', 'billing'] as $flag) {
        $table->boolean('is_' . $flag)->default(false)->index();
      }

      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('addresses');
  }
}
