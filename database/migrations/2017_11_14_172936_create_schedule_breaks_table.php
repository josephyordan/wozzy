<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleBreaksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('business_breaks', function (Blueprint $table) {
      $table->increments('id');
      $table->string('starts_at');
      $table->string('ends_at');
      $table->integer('business_schedule_id')->unsigned();

      $table->foreign('business_schedule_id')->references('id')->on('business_schedules')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('business_breaks');
  }
}
