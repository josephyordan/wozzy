<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessCategoryTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('business_category', function (Blueprint $table) {
      $table->integer('business_id')->unsigned();
      $table->integer('category_id')->unsigned();

      $table->foreign('category_id')->references('id')->on('categories')
        ->onUpdate('cascade')->onDelete('cascade');
      $table->foreign('business_id')->references('id')->on('businesses')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->primary(['business_id', 'category_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('business_category');
  }
}
