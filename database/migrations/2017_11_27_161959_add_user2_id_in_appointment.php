<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUser2IdInAppointment extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('appointments', function (Blueprint $table) {
      $table->integer('user2_id')->unsigned()->nullable();

      $table->foreign('user2_id')->references('id')->on('users')
        ->onUpdate('cascade')->onDelete('set null');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('appointments', function (Blueprint $table) {
      $table->dropColumn('user2_id');
    });
  }
}
