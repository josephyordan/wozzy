<?php

namespace App\Models;

use Jackiedo\Timezonelist\Timezonelist as ParentZone;

class Timezonelist extends ParentZone
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  /**
   * All continents of the world
   *
   * @var array
   */
  protected $continents = [
    'America'    => \DateTimeZone::AMERICA,
  ];
}
