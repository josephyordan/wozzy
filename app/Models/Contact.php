<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $fillable = [
    'number',
    'is_primary',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */
  public function contactable()
  {
    return $this->morphTo();
  }

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function isValid()
  {
    $validate = \Validator::make($this->toArray(), [
      'number' => 'required',
    ]);

    return $validate->passes();
  }
}
