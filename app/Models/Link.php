<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
  const WEBSITE = 0;
  const FACEBOOK = 1;
  const TWITTER = 2;
  const GOOGLE = 3;

  /**
   * Names of days of the week.
   *
   * @var array
   */
  protected static $types = array(
    self::WEBSITE => 'Website',
    self::FACEBOOK => 'Facebook',
    self::TWITTER => 'Twitter',
    self::GOOGLE => 'Google',
  );

  protected static $defaultType = 'Other';

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL FUNCTIONS
   *|--------------------------------------------------------------------------
   */
  protected $fillable = [
    'type',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public static function facebook()
  {
    return array_search(
      strtolower('facebook'),
      array_map('strtolower', self::$types)
    );
  }

  public function getTypes()
  {
    return self::$types;
  }

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */

  public function getTypeAttribute($value)
  {
    if (!empty(self::$types[$value])) {
      return self::$types[$value];
    }
    return $s = self::$defaultType;
  }

  /**
   *|--------------------------------------------------------------------------
   *  MUTATORS
   *|--------------------------------------------------------------------------
   */

  public function setTypeAttribute($value)
  {
    $type = array_search(
      strtolower($value),
      array_map('strtolower', self::$types)
    );

    if ($type !== false) {
      $this->attributes['type'] = $type;
    } elseif (!empty(self::$types[$value])) {
      $this->attributes['type'] = $value;
    } else {
      $this->attributes['type'] = null;
    }
  }
}
