<?php

namespace App\Models;

use App\Traits\HasDocuments;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

class Document extends Model implements StaplerableInterface
{
  use EloquentTrait, HasDocuments;

  /**
   * User constructor.
   * @param array $attributes
   *
   * Reference:
   * https://github.com/CodeSleeve/stapler/blob/v1.2.0/docs/examples.md
   */
  public function __construct(array $attributes = array())
  {
    $this->hasAttachedFile('file', [
      'styles' => [
        'large' => '640x640',
        'featured' => '585x585',
        'gallery' => '500x333#',
        'medium' => '400x400',
        'thumb' => '180x180',
        'admin' => '100x100',
        'tiny' => '75x75',
      ],
    ]);

    parent::__construct($attributes);
  }

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $fillable = [
    'sort',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function setVideoType()
  {
    $this->hasAttachedFile('file');
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function documentable()
  {
    return $this->morphTo();
  }

  /**
   *|--------------------------------------------------------------------------
   *  MUTATORS
   *|--------------------------------------------------------------------------
   */
}
