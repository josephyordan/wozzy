<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $fillable = [
    'line1',
    'line2',
    'city_id',
    'state_id',
    'post_code',
    'country_id',
    'note',
    'is_primary',
    'is_billing',
  ];

  protected $appends = [
    'city',
    'state',
    'country',
  ];

  protected $dates = ['deleted_at'];

  public $validation;

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function addressable()
  {
    return $this->morphTo();
  }

  public function geoCity()
  {
    return $this->belongsTo(GeoLocation::class, 'city_id');
  }

  public function geoState()
  {
    return $this->belongsTo(GeoLocation::class, 'state_id');
  }

  public function geoCountry()
  {
    return $this->belongsTo(GeoLocation::class, 'country_id');
  }

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function isValid()
  {
    $validate = \Validator::make(
      $this->toArray(), [
        'state_id' => 'required|exists:geo,id',
        'city_id' => 'required|exists:geo,id',
        'post_code' => 'required',
        'line1' => 'required_without:line2',
        'line2' => 'required_without:line1',
      ]
    );

    return $validate->passes();
  }

  private function getGeoName($geo)
  {
    if (!$geo) {
      return null;
    }

    return $geo->name;
  }

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */
  public function getCityAttribute()
  {
    return $this->getGeoName($this->geoCity);
  }

  public function getStateAttribute()
  {
    return $this->getGeoName($this->geoState);
  }

  public function getCountryAttribute()
  {
    return $this->getGeoName($this->geoCountry);
  }

  public function getAddressAttribute()
  {
    if (!$this->country_id) {
      $this->country_id = \GeoLocation::getCountry('US')->id;
      $this->save();
    }

    $collect = collect($this->toArray());
    $sorts = ['line1', 'line2', 'city', 'state', 'country', 'post_code'];
    $newOrder = collect();
    foreach ($sorts as $sort) {
      $newOrder->put($sort, $collect->get($sort));
    }

    return $newOrder->only('line1', 'line2', 'city', 'state', 'country', 'post_code')
      ->filter();
  }
}
