<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Category extends Model
{
  use CrudTrait;

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

//  protected $table = 'categories';
//  protected $primaryKey = 'id';
//  public $timestamps = false;
//  protected $guarded = ['id'];
  protected $fillable = [
    'name',
    'sort',
  ];
//  protected $hidden = [];
//  protected $dates = [];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public static function getBySort()
  {
    return self::ordered()->get();
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function businesses()
  {
    return $this->belongsToMany(Business::class);
  }

  /**
   *|--------------------------------------------------------------------------
   *  SCOPES
   *|--------------------------------------------------------------------------
   */

  public function scopeOrdered($query)
  {
    return $query->orderBy('created_at', 'asc');
  }

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */

  /**
   *|--------------------------------------------------------------------------
   *  MUTATORS
   *|--------------------------------------------------------------------------
   */
}
