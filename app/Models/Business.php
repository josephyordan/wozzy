<?php

namespace App\Models;

use App\Traits\HasAddresses;
use App\Traits\HasContacts;
use App\Traits\HasDocuments;
use App\User;
use Carbon\Carbon;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

//ini_set('max_execution_time', 300); //300 seconds = 5 minutes

class Business extends Model implements StaplerableInterface
{
  use CrudTrait,
    HasDocuments,
    HasContacts,
    HasAddresses,
    EloquentTrait;

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

//  protected $table = 'businesss';
//  protected $primaryKey = 'id';
//  public $timestamps = false;
//  protected $guarded = ['id'];
  protected $fillable = [
    'name', 'picture', 'description',
  ];
//  protected $hidden = [];
  protected $dates = [
    'ends_at',
    'published_at',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  /**
   * Business constructor.
   * @param array $attributes
   *
   * Reference:
   * https://github.com/CodeSleeve/stapler/blob/v1.2.0/docs/examples.md
   */
  public function __construct(array $attributes = array())
  {
    $this->hasAttachedFile('picture', [
      'default_url' => '/assets/img/profile.png',
      'styles' => [
        'large' => '640x640',
        'featured' => '585x585',
        'gallery' => '500x333#',
        'medium' => '400x400',
        'thumb' => '180x180',
        'admin' => '100x100',
        'tiny' => '75x75',
      ],
    ]);

    parent::__construct($attributes);
  }

  public function getOwner()
  {
    return $this->owner;
  }

  /**
   * Check if model has an address.
   *
   * @return bool
   */
  public function hasSchedule()
  {
    return (bool)$this->schedules()->count();
  }

  /**
   * Add an schedule to this model.
   *
   * @param  array $attributes
   * @return mixed
   */
  public function addSchedule(array $attributes)
  {
    return $this->schedules()->create($attributes);
  }

  /**
   * Updates the given address.
   *
   * @param  Schedule $schedule
   * @param  array $attributes
   * @return mixed
   */
  public function updateSchedule(Schedule $schedule, array $attributes)
  {
    return $schedule->fill($attributes)->save();
  }

  /**
   * Deletes given address.
   *
   * @param Schedule $schedule
   * @return bool
   */
  public function deleteSchedule(Schedule $schedule)
  {
    if (!$schedule)
      return false;

    return $schedule->delete();
  }

  /**
   * Deletes all the addresses of this model.
   *
   * @return bool
   */
  public function flushSchedule()
  {
    return $this->schedules()->delete();
  }

  /**
   * Get the first schedule.
   *
   * @return Schedule|null
   */
  public function getFirstSchedule()
  {
    if ($this->hasSchedule()) {
      return $this->schedules->first();
    }

    return null;
  }

  public function activate()
  {
    $this->ends_at = null;
    $this->save();
  }

  public function deactivate(Carbon $date)
  {
    $this->ends_at = $date;
    $this->save();
  }

  public function publish($bool = true)
  {
    if ($bool) {
      $this->published_at = new \DateTime();
    } else {
      $this->published_at = null;
    }

    $this->save();
  }

  public function getCategoryIds()
  {
    if ($this->categories) {
      return $this->categories->pluck('id')->toArray();
    }

    return [];
  }

  public function isPublic()
  {
    return $this->isPublish() && $this->isActive();
  }

  public function isPublish()
  {
    return $this->published_at != null;
  }

  public function isActive()
  {
    return $this->ends_at == null || Carbon::now()->lt($this->ends_at);
  }

  public function isValid()
  {
    $validator = \Validator::make($this->toArray(), [
      'name' => 'required',
    ]);

    return $validator->passes();
  }

  public function getJSOffers()
  {
    $collect = collect();
    foreach ($this->offers as $offer) {
      $collect->push([
        'id' => $offer->id,
        'name' => $offer->service->name,
        'amount' => $offer->getAmount(true),
        'amount_number' => $offer->getAmountFloat(),
        'duration' => $offer->getDuration(),
        'duration_minute' => $offer->getDurationInMinutes(),
      ]);
    }

    return $collect;
  }

  public function getSchedules($timestamp = null)
  {
    $timestamp = $timestamp ?: Carbon::now()->timestamp;
    $dates = $this->getWeekSchedules($timestamp);
    $schedules = [];

    foreach ($dates['all'] as $k => $d) {
      $schedule = $this->getDaySchedules($d)->toArray();
      $schedules[] = $schedule;
    }

    return collect(compact('week', 'day'));
  }

  public function getWeekSchedules($timestamp = null)
  {
    Carbon::setWeekStartsAt(Carbon::SUNDAY);
    $timestamp = $timestamp ?: Carbon::now()->timestamp;
    $startDate = Carbon::createFromTimestamp($timestamp);
    $startDate->setTimezone($this->timezone);
    $startDate->startOfWeek();
    $endDate = (clone $startDate)->addDay(6);
    $allDates = [];

    for ($date = clone $startDate; $date->lte($endDate); $date->addDay()) {
      $allDates[] = (clone $date)->timestamp;
    }

    $allDates = collect($allDates);

    $dates = [
      'all' => $allDates,
      'next' => Carbon::createFromTimestamp($allDates->last(), $this->timezone)
        ->addDay()->timestamp,
      'previous' => Carbon::createFromTimestamp($allDates->first(), $this->timezone)
        ->subDay()->timestamp,
    ];

    return collect($dates);
  }

  public function getDaySchedules($yyyymmdd = null, $isTimestamp = true)
  {
    if ($isTimestamp) {
      $yyyymmdd = $yyyymmdd ?: Carbon::now()
        ->setTimezone($this->timezone)
        ->timestamp;
      $startOfDay = Carbon::createFromTimestamp($yyyymmdd, $this->timezone);
    } else {
      $startOfDay = Carbon::parse($yyyymmdd, $this->timezone);
    }

    $startOfDay->startOfDay();
    $endOfDay = (clone $startOfDay)->endOfDay();

    $times = [];
    $unableOffers = collect();
    for (
      $date = clone $startOfDay;
      $date->lte($endOfDay);
      $date->addMinutes(15)
    ) {
      $t = [];
      if ($this->isScheduleAvailable($date)) {
        $t['time'] = (clone $date)->timestamp;
        $t['time_to_string'] = (clone $date)->toDayDateTimeString();
        $unavailable = $this->getOfferUnavailable($date)->pluck('id')->toArray();
        if ($unavailable) {
          $t['unavailable_offers'] = $unavailable;
          foreach ($unavailable as $id) {
            $unableOffers[$id] = $unableOffers->get($id) ?: 0;
            $unableOffers[$id] += 1;
          }
        }
      }
      if ($t) {
        $times[] = $t;
      }
    }

    $schedule = [];
    $unable = [];
    foreach ($unableOffers as $id => $offer) {
      if ($offer >= count($times)) {
        $unable[] = $id;
      }
    }

    $schedule['date'] = $startOfDay->timestamp;
    if ($unable) {
      $schedule['unavailable_offers'] = $unable;
    }
    $schedule['times'] = $times;

    return collect($schedule);
  }

  public function getOfferUnavailable(Carbon $date, $offerIds = null)
  {
    $return = [];
    $offers = $this->offers();
    if ($offerIds) {
      $offerIds = (array)$offerIds;
      $offers = $offers->whereIn('id', $offerIds);
    }
    $offers = $offers->get();
    $occupiedOffers = [];

    foreach ($offers as $offer) {
      $occupiedOffers[$offer->id]['users'] = $this->getUserAvailable($date, $offer->id)
        ->count();

      $occupiedOffers[$offer->id]['appointments'] = $this->getAppointments($date, $offer->id)
        ->count();
    }

    foreach ($occupiedOffers as $id => $items) {
      if ($items['appointments'] >= $items['users']) {
        $return[] = $id;
      }
    }

    return $this->offers()->whereIn('id', $return)
      ->where('business_id', $this->id)
      ->get();
  }

  public function getUserAvailable(Carbon $date, $offerIds)
  {
    $offerIds = (array)$offerIds;
    $c = Carbon::parse($date->toDateString())
      ->startOfDay();

    $users = $this->users()->with('offers')
      ->whereHas('offers', function ($q) use ($offerIds) {
        $q->whereIn('id', $offerIds);
      })
      ->whereDoesntHave('absents', function ($q) use ($c) {
        $q->where(
          'absent_at', $c
        );
      });

    $users = $users->get();

    return $users;
  }

  public function getAppointments(Carbon $date, $offerIds)
  {
    $offerIds = (array)$offerIds;
    $appointmentQuery = [];
    $appointments = $this->appointments()
      ->whereHas('offers', function ($q) use ($offerIds) {
        $q->whereIn('id', $offerIds);
      })
      ->get();
    foreach ($appointments as $appointment) {
      $startTime = clone $appointment->appointment_at;
      $endTime = clone $startTime;
      foreach ($appointment->offers as $o) {
        if (in_array($o->id, $offerIds)) {
          $endTime = clone $startTime;
          $endTime->addMinute($o->getDurationInMinutes());
          break;
        }
      }
      if ($date->between($startTime, $endTime)) {
        $appointmentQuery[] = $appointment;
      }
    }

    return collect($appointmentQuery);
  }

  private function getSchedule(Carbon $date)
  {
    $dayOfWeek = $date->dayOfWeek;
    $schedules = $this->schedules()
      ->where('days', 'like', '%' . $dayOfWeek . '%')
      ->get();

    return $schedules;
  }

  public function isScheduleAvailable(Carbon $date)
  {
    $schedules = $this->getSchedule($date);
    $now = Carbon::now();
    $now->setTimezone($this->timezone);
    $date->setTimezone($this->timezone);

    if ($now->gt($date)) {
      return false;
    }

    foreach ($schedules as $schedule) {
      $starts = (clone $date)->startOfDay();
      $ends = (clone $starts);
      $starts->minute = $schedule->getStartsInMinutes();
      $ends->minute = $schedule->getEndsInMinutes();
      if ($date->between($starts, $ends)) {
        return true;
      }
    }

    return false;
  }

  public function isSubscribeInTeamPlan()
  {
    return $this->getOwner() &&
      $this->getOwner()->subscribed(
        config('stripe-billing.defaults.subscription'),
        config('stripe-billing.plans.team.id')
      );
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */
  public function owner()
  {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function users()
  {
    $users = $this->hasMany(User::class);
    if (!$this->isSubscribeInTeamPlan()) {
      $owner = $this->getOwner() ?: $this;
      $users = $users->where('id', $owner->id);
    }
    return $users;
  }

  public function schedules()
  {
    return $this->hasMany(Schedule::class);
  }

  public function categories()
  {
    return $this->belongsToMany(Category::class);
  }

  public function appointments()
  {
    return $this->hasMany(Appointment::class);
  }

  public function offers()
  {
    $offers = $this->hasMany(Offer::class);
    if (!$this->isSubscribeInTeamPlan()) {
      $owner = $this->getOwner();
      $offers = $offers->whereHas('users', function ($q) use ($owner) {
        $q->where('id', $owner->id);
      });
    }

    return $offers;
  }

  public function absence()
  {
    return $this->hasManyThrough(Absent::class, User::class);
  }

  /**
   *|--------------------------------------------------------------------------
   *  SCOPES
   *|--------------------------------------------------------------------------
   */
  public function scopePublished($query)
  {
    return $query->whereNotNull('published_at')
      ->where(function ($q) {
        $q->whereNull('ends_at');
        $q->orWhereDate('ends_at', '>', Carbon::now());
      });
  }

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */
  public function getTimezoneAttribute($value)
  {
    return $value ?: config('app.timezone');
  }

  /**
   *|--------------------------------------------------------------------------
   *  MUTATORS
   *|--------------------------------------------------------------------------
   */
}
