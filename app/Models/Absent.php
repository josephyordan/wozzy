<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Absent extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $fillable = [
    'absent_at',
  ];

  protected $dates = [
    'absent_at',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
