<?php

namespace App\Models;


use App\User;
use Stripe\Stripe;

class Plan
{
  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public static function getStripePlans()
  {
    // Set the API Key
    Stripe::setApiKey(User::getStripeKey());

    try {
      // Fetch all the Plans and cache it
      return collect(
        \Cache::remember('stripe.plans', 60 * 24, function () {
          return \Stripe\Plan::all()->data;
        })
      );
    } catch (\Exception $e) {
      return false;
    }
  }

  public static function getPlans()
  {
    return collect(config('stripe-billing.plans'))
      ->pluck('id');
  }

  public static function getPlan($id, $attribute)
  {
    return collect(config('stripe-billing.plans'))
      ->where('id', $id)
      ->pluck($attribute)->first();
  }
}