<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $fillable = [
    'days',
    'starts_at',
    'ends_at',
  ];

  protected $casts = [
    'days' => 'array',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function saveBreaks(array $attributes)
  {
    $this->breaks()->delete();
    return $this->breaks()->createMany($attributes);
  }

  public function getStartsInMinutes()
  {
    $time = Carbon::parse($this->starts);
    $today = Carbon::today();
    return $time->diffInMinutes($today);
  }

  public function getEndsInMinutes()
  {
    $time = Carbon::parse($this->ends);
    $today = Carbon::today();
    return $time->diffInMinutes($today);
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function business()
  {
    return $this->belongsTo(Business::class);
  }

  public function breaks()
  {
    return $this->hasMany(ScheduleBreak::class, 'business_schedule_id');
  }

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */

  public function getStartsAttribute()
  {
    return Carbon::parse($this->starts_at);
  }

  public function getEndsAttribute()
  {
    $ends = Carbon::parse($this->ends_at);
    if($this->starts->gte($ends)) {
      $ends = $ends->addDay();
    }

    return $ends;
  }
}
