<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Service extends Model
{
  use CrudTrait;

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  //protected $table = 'services';
  //protected $primaryKey = 'id';
  // public $timestamps = false;
  // protected $guarded = ['id'];
  protected $fillable = [
    'name', 'primary', 'sort',
  ];
  // protected $hidden = [];
  // protected $dates = [];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function isPrimary()
  {
    return $this->primary;
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function offers()
  {
    return $this->hasMany(Offer::class);
  }

  /**
   *|--------------------------------------------------------------------------
   *  SCOPES
   *|--------------------------------------------------------------------------
   */

  public function scopePrimaryList($query, $business = null)
  {
    $query = $query->where('primary', true);

    if ($business) {
      $query = $query->orWhereHas('offers', function ($q) use ($business) {
        $q->where('business_id', $business);
      });
    }

    return $query;
  }

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */

  /**
   *|--------------------------------------------------------------------------
   *  MUTATORS
   *|--------------------------------------------------------------------------
   */
}
