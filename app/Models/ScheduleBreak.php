<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleBreak extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $table = 'breaks';

  protected $fillable = [
    'starts_at',
    'ends_at',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function schedule()
  {
    return $this->belongsTo(Schedule::class, 'business_schedule_id');
  }
}
