<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Appointment extends Model
{
  use CrudTrait;

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  //protected $table = 'appointments';
  //protected $primaryKey = 'id';
  // public $timestamps = false;
  // protected $guarded = ['id'];
  protected $fillable = [
    'message', 'appointment_at',
  ];
  // protected $hidden = [];
  protected $dates = [
    'appointment_at', 'done_at',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function toggleDone()
  {
    if ($this->done_at) {
      $this->done_at = null;
    } else {
      $this->setAsDone();
    }
  }

  public function setAsDone()
  {
    $this->done_at = Carbon::now();
  }

  public function isDone()
  {
    return (bool)$this->done_at;
  }

  public function appointmentTimezone()
  {
    $business = $this->business;
    return $this->appointment_at->setTimezone(
      $business->timezone
    );
  }

  public function offerNames()
  {
    $offerNames = collect();
    foreach ($this->offers as $offer) {
      $offerNames->push($offer->service->name);
    }

    return $offerNames;
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function business()
  {
    return $this->belongsTo(Business::class);
  }

  public function offers()
  {
    return $this->belongsToMany(Offer::class);
  }

  /**
   *|--------------------------------------------------------------------------
   *  SCOPES
   *|--------------------------------------------------------------------------
   */

  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */

  /**
   *|--------------------------------------------------------------------------
   *  MUTATORS
   *|--------------------------------------------------------------------------
   */
}
