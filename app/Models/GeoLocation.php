<?php

namespace App\Models;

use Igaster\LaravelCities\Geo;

class GeoLocation extends Geo
{
  /**
   *|--------------------------------------------------------------------------
   *  ACCESSORS
   *|--------------------------------------------------------------------------
   */

  public function getNameAttribute($value)
  {
    if (self::LEVEL_2 === $this->level) {
      return str_replace('County', 'City', $value);
    }
    return $value;
  }

  /**
   *|--------------------------------------------------------------------------
   *  ROUTES
   *|--------------------------------------------------------------------------
   */

  public static function ApiRoutes()
  {
    \Route::group(['prefix' => 'geo', 'namespace' => 'Vendor'], function () {
      \Route::get('search/{name}/{parent_id?}', 'GeoController@search');
      \Route::get('item/{id}', 'GeoController@item');
      \Route::get('items/{ids}', 'GeoController@items');
      \Route::get('children/{id}', 'GeoController@children');
      \Route::get('parent/{id}', 'GeoController@parent');
      \Route::get('country/{code}', 'GeoController@country');
      \Route::get('countries', 'GeoController@countries');
    });
  }
}
