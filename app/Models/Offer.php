<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $fillable = [
    'amount', 'duration',
  ];

  protected $currency = '$';

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function getCurrency()
  {
    return $this->currency;
  }

  public function getDuration()
  {
    $time = Carbon::parse($this->duration);
    $today = Carbon::today();
    $diff = $time->diff($today);

    $duration = '';
    if ($diff->h) {
      $duration .= $diff->h . 'h ';
    }

    if ($diff->i) {
      $duration .= $diff->i . 'm';
    }

    if (strlen($duration) <= 0) {
      $duration = '0m';
    }

    return $duration;
  }

  public function getDurationInMinutes()
  {
    $time = Carbon::parse($this->duration);
    $today = Carbon::today();
    return $time->diffInMinutes($today);
  }

  public function getAmount($currency = false)
  {
    $amount = number_format($this->getAmountFloat(), 2, '.', ',');
    if ($currency) {
      $amount = $this->currency . $amount;
    }

    return $amount;
  }

  public function getAmountFloat()
  {
    return (float)str_replace(',', '', $this->amount);
  }

  public function getUsers()
  {
    $users = $this->users();
    $owner = $this->business->getOwner();
    if (
      $owner &&
      !$owner->subscribed(
        config('stripe-billing.defaults.subscription'),
        config('stripe-billing.plans.team.id')
      )
    ) {
      $users = $users->where('id', $this->business->getOwner()->id);
    }

    return $users->get();
  }

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function service()
  {
    return $this->belongsTo(Service::class);
  }

  public function business()
  {
    return $this->belongsTo(Business::class);
  }

  public function users()
  {
    return $this->belongsToMany(User::class);
  }

  public function appointments()
  {
    return $this->belongsToMany(Appointment::class);
  }

  /**
   *|--------------------------------------------------------------------------
   *  BOOT
   *|--------------------------------------------------------------------------
   */

  // this is a recommended way to declare event handlers
  protected static function boot()
  {
    parent::boot();

    static::deleting(function ($offer) { // before delete() method call this
      if (!$offer->service->isPrimary()) {
        if ($offer->service->offers->count() <= 1) {
          $offer->service->delete();
        }
      }
      // do the rest of the cleanup...
    });
  }
}
