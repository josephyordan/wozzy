<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offerable extends Model
{
  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  protected $table = 'offer_user';
}
