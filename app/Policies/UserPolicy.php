<?php

namespace App\Policies;

use App\Models\Address;
use App\Models\Contact;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
  use HandlesAuthorization;

  /**
   * Create a new policy instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  public function valid(User $user)
  {
    if ($user->hasAnyRole(['superadmin'])) {
      return true;
    }

    $address = $user->getPrimaryAddress() ?: new Address();
    $contact = $user->getPrimaryContact() ?: new Contact();
    if (
      $address->isValid() &&
      $contact->isValid() &&
      $user->hasStripeId() &&
      $user->hasCardOnFile()
    ) {
      return true;
    }

    return false;
  }
}
