<?php

namespace App\Policies;

use App\User;
use App\Models\Absent;
use Illuminate\Auth\Access\HandlesAuthorization;

class AbsentPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can edit the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Absent $absent
   * @return mixed
   */
  public function edit(User $user, Absent $absent)
  {
    return $this->canAccess($user, $absent);
  }

  /**
   * Determine whether the user can view the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Absent $absent
   * @return mixed
   */
  public function view(User $user, Absent $absent)
  {
    return $this->canAccess($user, $absent);
  }

  /**
   * Determine whether the user can create schedules.
   *
   * @param  \App\User $user
   * @return mixed
   */
  public function create(User $user)
  {
    return $user->hasRole('merchant');
  }

  /**
   * Determine whether the user can store schedules.
   *
   * @param  \App\User $user
   * @return mixed
   */
  public function store(User $user)
  {
    return $user->hasRole('merchant');
  }

  /**
   * Determine whether the user can update the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Absent $absent
   * @return mixed
   */
  public function update(User $user, Absent $absent)
  {
    return $this->canAccess($user, $absent);
  }

  /**
   * Determine whether the user can delete the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Absent $absent
   * @return mixed
   */
  public function delete(User $user, Absent $absent)
  {
    return $this->canAccess($user, $absent);
  }

  private function canAccess(User $user, Absent $absent)
  {
    if ($business = $user->getBusiness()) {
      return $business->id === $absent->user->business->id;
    }

    return false;
  }
}
