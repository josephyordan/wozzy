<?php

namespace App\Policies;

use App\Models\Address;
use App\Models\Business;
use App\Models\Contact;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BusinessPolicy
{
  use HandlesAuthorization;

  /**
   * Create a new policy instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Determine whether the user can edit the business.
   *
   * @param  \App\User $user
   * @param \App\Models\Business $business
   * @return mixed
   */
  public function edit(User $user, Business $business)
  {
    return $user->getBusiness()->id === $business->id;
  }

  /**
   * Determine whether the user can view the business.
   *
   * @param  \App\User $user
   * @param \App\Models\Business $business
   * @return mixed
   */
  public function viewPublic(User $user, Business $business)
  {
    return $user->getBusiness()->id === $business->id;
  }

  /**
   * Determine whether the user can view the business.
   *
   * @param  \App\User $user
   * @param \App\Models\Business $business
   * @return mixed
   */
  public function appointment(User $user, Business $business)
  {
    return $user->getBusiness()->id !== $business->id;
  }

  /**
   * Determine whether the user can view the business.
   *
   * @param  \App\User $user
   * @param \App\Models\Business $business
   * @return mixed
   */
  public function owner(User $user, Business $business)
  {
    return $business->getOwner() &&
      $user->id === $business->getOwner()->id;
  }

  /**
   * Determine whether the user can view the business.
   *
   * @param  \App\User $user
   * @param \App\Models\Business $business
   * @return mixed
   */
  public function teamOwner(User $user, Business $business)
  {
    return $this->subscribeToTeam($user, $business) &&
      $user->id === $business->getOwner()->id;
  }

  /**
   * Determine whether the user can view the business.
   *
   * @param  \App\User $user
   * @param \App\Models\Business $business
   * @return mixed
   */
  public function subscribeToTeam(User $user, Business $business)
  {
    return $business->getOwner() &&
      $business->getOwner()->subscribed(
        config('stripe-billing.defaults.subscription'),
        config('stripe-billing.plans.team.id')
      ) &&
      $business->users()->where('id', $user->id)->exists();
  }

  public function valid(User $user, Business $business)
  {
    $address = $business->getPrimaryAddress() ?: new Address();
    $contact = $business->getPrimaryContact() ?: new Contact();

    return $business->isValid() &&
      $address->isValid() &&
      $contact->isValid() &&
      $business->users()->where('id', $user->id)->exists();
  }
}
