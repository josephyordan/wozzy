<?php

namespace App\Policies;

use App\User;
use App\Models\Schedule;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchedulePolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can edit the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Schedule $schedule
   * @return mixed
   */
  public function edit(User $user, Schedule $schedule)
  {
    return $this->canAccess($user, $schedule);
  }

  /**
   * Determine whether the user can view the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Schedule $schedule
   * @return mixed
   */
  public function view(User $user, Schedule $schedule)
  {
    return $this->canAccess($user, $schedule);
  }

  /**
   * Determine whether the user can create schedules.
   *
   * @param  \App\User $user
   * @return mixed
   */
  public function create(User $user)
  {
    return $user->hasRole('merchant');
  }

  /**
   * Determine whether the user can store schedules.
   *
   * @param  \App\User $user
   * @return mixed
   */
  public function store(User $user)
  {
    return $user->hasRole('merchant');
  }

  /**
   * Determine whether the user can update the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Schedule $schedule
   * @return mixed
   */
  public function update(User $user, Schedule $schedule)
  {
    return $this->canAccess($user, $schedule);
  }

  /**
   * Determine whether the user can delete the schedule.
   *
   * @param  \App\User $user
   * @param  \App\Models\Schedule $schedule
   * @return mixed
   */
  public function delete(User $user, Schedule $schedule)
  {
    return $this->canAccess($user, $schedule);
  }

  private function canAccess(User $user, Schedule $schedule)
  {
    if ($schedule->business && $business = $user->getBusiness()) {
      return $business->id === $schedule->business->id;
    }

    return false;
  }
}
