<?php

namespace App\Policies;

use App\User;
use App\Models\Appointment;
use Illuminate\Auth\Access\HandlesAuthorization;

class AppointmentPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can edit the appointment.
   *
   * @param  \App\User $user
   * @param  \App\Models\Appointment $appointment
   * @return mixed
   */
  public function edit(User $user, Appointment $appointment)
  {
    return $this->canAccess($user, $appointment);
  }

  /**
   * Determine whether the user can view the appointment.
   *
   * @param  \App\User $user
   * @param  \App\Models\Appointment $appointment
   * @return mixed
   */
  public function view(User $user, Appointment $appointment)
  {
    return $this->canAccess($user, $appointment);
  }

  /**
   * Determine whether the user can create appointments.
   *
   * @param  \App\User $user
   * @return mixed
   */
  public function create(User $user)
  {
    return $user->hasRole('merchant');
  }

  /**
   * Determine whether the user can store appointments.
   *
   * @param  \App\User $user
   * @return mixed
   */
  public function store(User $user)
  {
    return $user->hasRole('merchant');
  }

  /**
   * Determine whether the user can update the appointment.
   *
   * @param  \App\User $user
   * @param  \App\Models\Appointment $appointment
   * @return mixed
   */
  public function update(User $user, Appointment $appointment)
  {
    return $this->canAccess($user, $appointment);
  }

  /**
   * Determine whether the user can delete the appointment.
   *
   * @param  \App\User $user
   * @param  \App\Models\Appointment $appointment
   * @return mixed
   */
  public function delete(User $user, Appointment $appointment)
  {
    return $this->canAccess($user, $appointment);
  }

  private function canAccess(User $user, Appointment $appointment)
  {
    if ($appointment->business && $business = $user->getBusiness()) {
      return $business->id === $appointment->business->id;
    }

    return false;
  }
}
