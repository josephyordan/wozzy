<?php
/**
 * Created by PhpStorm.
 * User: josephy
 * Date: 11/14/2017
 * Time: 8:58 AM
 */

use Carbon\Carbon;

function getDaysArray()
{
  Carbon::setWeekStartsAt(Carbon::SUNDAY);
  $startDate = Carbon::now()->startOfWeek();
  $endDate = (clone $startDate)->addWeek();
  $dates = [];

  for ($date = $startDate; $date->lt($endDate); $date->addDay()) {
    $dates[] = $date->format('l');
  }

  return $dates;
}

function getDayByIndex($daysOfWeek)
{
  $daysOfWeek = collect((array)$daysOfWeek)
    ->unique()
    ->filter(function ($value) {
      return is_numeric($value);
    })
    ->map(function ($value) {
      return (int)$value;
    })
    ->sort();
  $weekdays = [1, 2, 3, 4, 5];
  $weekends = [0, 6];

  if ($daysOfWeek->diff($weekdays)->isEmpty()) {
    return ['Weekdays'];
  } elseif ($daysOfWeek->diff($weekends)->isEmpty()) {
    return ['Weekends'];
  }

  $result = [];
  $daysArray = getDaysArray();

  foreach ($daysOfWeek as $dayOfWeek) {
    if (isset($daysArray[$dayOfWeek])) {
      $result[] = $daysArray[$dayOfWeek];
    }
  }

  return $result;
}

function orHelper(&$var, $replace)
{
  return isset($var, $replace) ? $var : $replace;
}

function limitStr($str, $int)
{
  $str = trim(preg_replace('/\s\s+/', ' ', $str));
  $str = preg_replace("/<br\W*?\/>/", "\n", $str);
  $str = strip_tags($str);
  $string = substr($str, 0, $int);

  if (strlen($string) < strlen($str)) {
    $string .= '...';
  }

  return $string;
}

function durationToString($minutes)
{
  if ($minutes < 60) {
    return ($minutes) . 'm';
  } else if (($minutes % 60) == 0) {
    return ($minutes - $minutes % 60) / 60 . 'h';
  } else {
    return (($minutes - $minutes % 60) / 60 . 'h' . ' ' + $minutes % 60 . 'm');
  }
}

function intToCurrency($amount)
{
  $offer = new \App\Models\Offer();
  $amount = number_format($amount, 2, '.', ',');
  return $offer->getCurrency() . $amount;
}