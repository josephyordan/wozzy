<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\Document;
use FFMpeg\Coordinate\TimeCode;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
  private $cacheDisk = 'cache';

  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Portfolio" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function edit(Request $request)
  {
    $business = $request->user()->getBusiness();
    $deleteFile = true;
    return view('logged.settings-portfolio', compact(
      'business', 'deleteFile'
    ));
  }

  public function fileUpload(Request $request)
  {
    $business = $request->user()->getBusiness();
    $deleteFile = true;
    $files = [];
    foreach ($request->files as $items) {
      if ($items[0]->isValid()) {
        $file = $items[0];
        $video = false;

        $document = $business->addDocument();
        if (strpos($file->getMimeType(), 'video') !== false) {
          $document->setVideoType();
          $video = true;
        }
        $document->file = $file;
        $document->save();


        if ($video) {
          $cover = $document->addDocument();
          $cover->file = $this->extractImage(public_path($document->file->url()));
          $cover->save();
        }

        $file_object = new \stdClass();
        $file_object->name = str_replace('photos/', '', $file->getClientOriginalName());
        $file_object->url = $document->file->url();
        if ($video) {
          $file_object->view = view(
            'logged.settings-portfolio-video',
            compact('document', 'deleteFile')
          )->render();
          $file_object->type = 'video';
        } else {
          $file_object->view = view(
            'logged.settings-portfolio-image',
            compact('document', 'deleteFile')
          )->render();
          $file_object->type = 'image';
        }
        $file_object->size = $file->getClientSize();
        $file_object->fileID = $document->id;
        $files[] = $file_object;
      }
    }

    return response()->json(array('files' => $files), 200);
  }

  /**
   * Use this function for Business->picture only
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function pictureUpload(Request $request)
  {
    $this->validate($request, [
      'file' => 'required',
    ]);

    $business = $request->user()->getBusiness();
    $files = [];
    $file = $request->file;
    if ($file->isValid()) {
      $business->picture = $file;
      $business->save();

      $file_object = new \stdClass();
      $file_object->name = str_replace('photos/', '', $file->getClientOriginalName());
      $file_object->url = $business->picture->url();
      $file_object->size = $file->getClientSize();
      $file_object->fileID = $business->id;
      $files[] = $file_object;
    }

    return response()->json(array('files' => $files), 200);
  }

  public function delete(Request $request)
  {
    $this->validate($request, [
      'document' => 'required',
    ]);

    $document = Document::findOrFail($request->document);
    if (get_class($document->documentable) === Business::class) {
      if ($document->documentable->id === $request->user()->getBusiness()->id) {
        $document->delete();
        return response()->json(['success' => true]);
      }
    }

    return response()->json(['success' => false], 401);
  }

  public function deleteProfile(Request $request)
  {
    $business = $request->user()->getBusiness();
    $business->picture = STAPLER_NULL;
    $business->save();

    return response()->json(['success' => true]);
  }

  private function extractImage($file)
  {
    $imageFilename = 'cover.jpg';
    $pPath = \Storage::disk($this->cacheDisk)->path($imageFilename);

    $ffmpeg = app()->make('ffmpeg');

    if ($ffmpeg) {
      $video = $ffmpeg->open($file);
      $duration = $video->getFormat()->get('duration');
      $halfDuration = (int)$duration / 2;
      $ffmpeg->open($file)
        ->frame(TimeCode::fromSeconds($halfDuration))
        ->save($pPath);
    } else {
      $this->ffmpegError($imageFilename);
    }

    return $pPath;
  }

  private function ffmpegError($imageFilename)
  {
    $img = \Image::canvas(1100, 720, '#ffffff');
    $img->insert(public_path('assets/img/watermark-draft.png'), 'center');
    $img->save(\Storage::disk($this->cacheDisk)->path($imageFilename), 60);
  }
}
