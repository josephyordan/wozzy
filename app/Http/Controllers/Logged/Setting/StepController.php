<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Http\Requests\ContactInfoRequest;
use App\Http\Requests\SettingBusinessRequest;
use App\Models\Address;
use App\Models\Contact;
use App\Models\Schedule;
use Illuminate\Http\Request;

class StepController extends BusinessController
{
  protected $step = 0;

  public function stepEdit(Request $request, $step)
  {
    $verify = $this->stepVerify($request);
    if ($verify) {
      return $verify;
    }

    if (1 === (int)$step) {
      return $this->step1($request);
    }
    if (2 === (int)$step) {
      return $this->step2($request);
    }
  }

  public function stepStore(Request $request, $step)
  {
    $verify = $this->stepVerify($request);
    if ($verify) {
      return $verify;
    }


    $this->step = (int)$step;
    if (1 === (int)$step) {
      return $this->step1Store($request);
    }
    if (2 === (int)$step) {
      return $this->step2Store($request);
    }
  }

  public function stepVerify(Request $request)
  {
    $user = $request->user();
    $business = $user->getBusiness();
    if (!$user->subscribed(config('stripe-billing.defaults.subscription'))) {
      if (
        \Gate::allows('valid', $business) &&
        route('settings.business.step', 2) !== $request->url()
      ) {
        return redirect()->route('settings.business.step', 2);
      }

      if ($business->hasSchedule()) {
        return redirect()->route('settings.subscribe.plans');
      }
    }

    if ($user->subscribed(config('stripe-billing.defaults.subscription'))) {
      return redirect()->route('home');
    }

    return false;
  }

  public function step1(Request $request)
  {
    $business = $request->user()->getBusiness();
    $address = $business->getPrimaryAddress() ?: new Address();
    $primaryContact = $business->getPrimaryContact() ?: new Contact();
    $secondaryContact = $business->getSecondaryContact() ?: new Contact();
    $edit = true;

    return view('logged.settings-step-1', compact(
      'business', 'address', 'edit', 'primaryContact', 'secondaryContact'
    ));
  }

  public function step1Store(Request $request)
  {
    app()->make(ContactInfoRequest::class);
    app()->make(SettingBusinessRequest::class);

    $business = $request->user()->getBusiness();
    $businessRequest = $request->only('name', 'description', 'timezone');
    $business->forceFill($businessRequest);
    $business->publish(true);
    $business->save();

    $this->updateContact($request);
    $this->updateAddress($request);

    $business->categories()->sync($request->categories ?: []);
    $business->save();

    return redirect()->route('settings.business.step', $this->step + 1);
  }

  public function step2(Request $request)
  {
    $this->authorize('create', Schedule::class);
    $business = $request->user()->getBusiness();
    $schedule = new Schedule();

    return view('logged.settings-step-2', compact(
      'business', 'schedule'
    ));
  }

  public function step2Store(Request $request)
  {
    $this->authorize('store', Schedule::class);

    $business = $request->user()->getBusiness();
    $schedule = $request->only('days', 'starts_at', 'ends_at');
    $business->addSchedule($schedule);

    \Alert::success('Business Setup Complete!')->flash();

    return redirect()->route('settings.subscribe.plans');
  }
}
