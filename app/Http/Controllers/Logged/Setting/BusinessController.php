<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactInfoRequest;
use App\Http\Requests\SettingBusinessRequest;
use App\Models\Address;
use App\Models\Contact;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Business" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function view(Request $request)
  {
    $business = $request->user()->getBusiness();
    $address = $business->getPrimaryAddress() ?: new Address();
    $primaryContact = $business->getPrimaryContact() ?: new Contact();
    $secondaryContact = $business->getSecondaryContact() ?: new Contact();

    return view('logged.settings-business', compact(
      'business', 'address', 'edit', 'primaryContact', 'secondaryContact'
    ));
  }

  public function edit(Request $request)
  {
    $business = $request->user()->getBusiness();
    $address = $business->getPrimaryAddress() ?: new Address();
    $primaryContact = $business->getPrimaryContact() ?: new Contact();
    $secondaryContact = $business->getSecondaryContact() ?: new Contact();
    $edit = true;

    return view('logged.settings-business', compact(
      'business', 'address', 'edit', 'primaryContact', 'secondaryContact'
    ));
  }

  public function update(SettingBusinessRequest $request)
  {
    $business = $request->user()->getBusiness();
    $businessRequest = $request->only('name', 'description');
    $business->update($businessRequest);

    $business->categories()->sync($request->categories ?: []);

    \Alert::success('Business is successfully updated.')->flash();

    return redirect()->route('settings.business');
  }

  public function updateInfo(ContactInfoRequest $request)
  {
    $this->updateContact($request);
    $this->updateAddress($request);
    $business = $request->user()->getBusiness();
    $business->timezone = $request->timezone;
    $business->save();

    \Alert::success('Address is successfully updated.')->flash();

    return redirect()->route('settings.business');
  }

  public function updatePublish(Request $request)
  {
    $this->validate($request, [
      'publish' => 'required|boolean',
    ]);

    $business = $request->user()->getBusiness();

    if (\Gate::denies('valid', $business)) {
      \Alert::add('danger', 'Invalid Business Information.')->flash();
      return redirect()->back();
    }

    $business->publish($request->publish);

    if ($request->publish) {
      \Alert::success('Business is now published.')->flash();
    } else {
      \Alert::success('Business is unpublished.')->flash();
    }

    return redirect()->route('settings.business');
  }

  protected function updateContact(Request $request)
  {
    $business = $request->user()->getBusiness();

    if ($current_contact1 = $business->getPrimaryContact()) {
      $business->updateContact($current_contact1, [
        'number' => $request->primary_contact,
        'is_primary' => true,
      ]);
    } else {
      $business->addContact(['number' => $request->primary_contact]);
    }

    if ($request->secondary_contact) {
      if ($current_contact2 = $business->getSecondaryContact()) {
        $business->updateContact($current_contact2, ['number' => $request->secondary_contact]);
      } else {
        $business->addContact(['number' => $request->secondary_contact]);
      }
    }
  }

  protected function updateAddress(Request $request)
  {
    $business = $request->user()->getBusiness();
    $address = $request->only([
      'line1', 'line2', 'post_code', 'city_id', 'state_id',
    ]);

    if ($current_address = $business->getPrimaryAddress()) {
      $business->updateAddress($current_address, $address);
    } else {
      $business->addAddress($address);
    }
  }
}
