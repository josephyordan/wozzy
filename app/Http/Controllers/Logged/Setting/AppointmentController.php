<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Models\Address;
use App\Models\Appointment;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Appointment" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function index(Request $request)
  {
    $business = $request->user()->getBusiness();
    $appointments = $business->appointments;

    return view('logged.settings-appointments', compact(
      'business', 'appointments'
    ));
  }

  public function view(Request $request, Appointment $appointment)
  {
    $this->authorize('view', $appointment);

    $business = $request->user()->getBusiness();
    $user = $appointment->user;
    $address = $user->getPrimaryAddress() ?: new Address();
    $primaryContact = $user->getPrimaryContact() ?: new Contact();
    $secondaryContact = $user->getSecondaryContact() ?: new Contact();

    return view('logged.settings-appointment', compact(
      'business', 'appointment', 'user', 'address',
      'primaryContact', 'secondaryContact'
    ));
  }

  public function markDone(Appointment $appointment)
  {
    $this->authorize('view', $appointment);

    $appointment->toggleDone();
    $appointment->save();

    if ($appointment->isDone()) {
      $message = 'DONE';
    } else {
      $message = 'Not Done';
    }

    \Alert::success('Appointment request marked as '. $message .'.')->flash();

    return redirect()->back();
  }
}
