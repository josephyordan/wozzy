<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Models\Offer;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Schedule" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function index(Request $request)
  {
    $business = $request->user()->getBusiness();
    $offers = $business->offers;

    return view('logged.settings-services', compact(
      'offers'
    ));
  }

  public function create(Request $request, Offer $offer)
  {
    $business = $request->user()->getBusiness();
    $serviceList = Service::primaryList($business->id)->get();

    return view('logged.settings-service-create', compact(
      'serviceList', 'offer', 'business'
    ));
  }

  public function edit(Request $request, Offer $offer)
  {
    $business = $request->user()->getBusiness();
    $serviceList = Service::primaryList($business->id)->get();

    return view('logged.settings-service-edit', compact(
      'serviceList', 'offer', 'business'
    ));
  }

  public function store(Request $request)
  {
    $business = $request->user()->getBusiness();

    if (\Gate::denies('subscribeToTeam', $business)) {
      $request->request->set('users', [$request->user()->id]);
    }

    $service = Service::find($request->service);
    $offer = new Offer();
    $offer->amount = $request->amount;
    $offer->duration = $request->duration;
    $offer->business()->associate($business);

    if ($service) {
      $offer->service()->associate($service);
    } else {
      $newService = Service::create(['name' => $request->other_service]);
      $offer->service()->associate($newService);
    }
    $offer->save();
    $offer->users()->sync($request->users);

    \Alert::success('Service is successfully created.')->flash();
    return redirect()->route('settings.service', ['offer' => $offer->id]);
  }

  public function update(Request $request, Offer $offer)
  {
    $service = Service::find($request->service);
    $offer->amount = $request->amount;
    $offer->duration = $request->duration;
    $offer->users()->sync($request->users);
    if ($service) {
      $offer->service()->associate($service);
    } else {
      $service = Service::findOrFail($offer->service_id);
      $service->update(['name' => $request->other_service]);
    }
    $offer->save();

    \Alert::success('Service is successfully updated.')->flash();
    return redirect()->route('settings.service', ['offer' => $offer->id]);
  }

  public function delete(Offer $offer)
  {
    $offer->delete();

    \Alert::success('Service is successfully deleted.')->flash();
    return redirect()->route('settings.services');
  }
}
