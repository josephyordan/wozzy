<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Events\NewMemberAdded;
use App\Http\Controllers\Controller;
use App\Http\Requests\MemberRequest;
use App\User;
use Faker\Factory;
use Illuminate\Http\Request;

class MemberController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      $business = $request->user()->getBusiness();
      if (\Gate::denies('teamOwner', $business)) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function index(Request $request)
  {
    $business = $request->user()->getBusiness();
    $users = User::where('business_id', $business->id)
      ->where('id', '<>', $business->getOwner()->id)
      ->get();

    return view('logged.settings-members', compact(
      'business', 'users'
    ));
  }

  public function create(Request $request)
  {
    $business = $request->user()->getBusiness();
    $this->authorize('teamOwner', $business);
    $user = new User();
    $edit = true;

    return view('logged.settings-member-create', compact(
      'business', 'user', 'edit'
    ));
  }

  public function edit(Request $request, User $user)
  {
    $business = $request->user()->getBusiness();
    $this->authorize('teamOwner', $business);

    return view('logged.settings-member-edit', compact(
      'business', 'user'
    ));
  }

  public function store(MemberRequest $request)
  {
    $business = $request->user()->getBusiness();
    $userFields = $request->only('name', 'email');
    $user = new User();
    $user->fill($userFields);
    $password = Factory::create()->password(15);
    $user->password = bcrypt($password);
    $user = $business->users()->save($user);
    $user->sendNewMemberNotification($business, $password);
    $user->asMerchant();

    event(new NewMemberAdded($user));
    \Alert::success('Member is successfully created.')->flash();

    return redirect()->route('settings.members');
  }

  public function update(MemberRequest $request, User $user)
  {
    $business = $request->user()->getBusiness();
    $this->authorize('teamOwner', $business);
    $userFields = $request->only('name');
    $user->update($userFields);

    \Alert::success('Member is successfully updated.')->flash();

    return redirect()->route('settings.members');
  }

  public function delete(Request $request, User $user)
  {
    $business = $request->user()->getBusiness();
    $this->authorize('teamOwner', $business);
    $user->delete();
    \Alert::success('Member is successfully deleted.')->flash();
    return redirect()->route('settings.members');
  }
}
