<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingScheduleRequest;
use App\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Schedule" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function index(Request $request)
  {
    $business = $request->user()->getBusiness();
    $schedules = $business->schedules;
    $days = getDaysArray();

    return view('logged.settings-schedules', compact(
      'business', 'schedules', 'days'
    ));
  }

  public function create(Request $request)
  {
    $this->authorize(Schedule::class);
    $business = $request->user()->getBusiness();
    $schedule = new Schedule();
    $breaks = [];

    return view('logged.settings-schedule-create', compact(
      'business', 'schedule', 'breaks'
    ));
  }

  public function edit(Request $request, Schedule $schedule)
  {
    $this->authorize('edit', $schedule);

    $business = $request->user()->getBusiness();
    $breaks = $schedule->breaks;

    return view('logged.settings-schedule-edit', compact(
      'business', 'schedule', 'breaks'
    ));
  }

  public function store(SettingScheduleRequest $request)
  {
    $this->authorize('store', Schedule::class);

    $business = $request->user()->getBusiness();
    $schedule = $request->only('days', 'starts_at', 'ends_at');
    $breaks = $this->formatBreaks($request);
    $business->addSchedule($schedule);

    $business->getFirstSchedule()->saveBreaks($breaks);

    \Alert::success('Schedule is successfully updated.')->flash();

    return redirect()->route('settings.schedules');
  }

  public function update(SettingScheduleRequest $request, Schedule $schedule)
  {
    $this->authorize('update', $schedule);

    $business = $request->user()->getBusiness();
    $newSchedule = $request->only('days', 'starts_at', 'ends_at');
    $breaks = $this->formatBreaks($request);
    $business->updateSchedule($schedule, $newSchedule);
    $business->getFirstSchedule()->saveBreaks($breaks);

    \Alert::success('Schedule is successfully updated.')->flash();

    return redirect()->route('settings.schedules');
  }

  public function delete(Schedule $schedule)
  {
    $this->authorize($schedule);
    $schedule->delete();
    \Alert::success('Schedule is successfully deleted.')->flash();
    return redirect()->route('settings.schedules');
  }

  private function formatBreaks(Request $request)
  {
    $breaks = [];
    $breakForm = [
      'starts_at' => $request->break_from,
      'ends_at' => $request->break_to,
    ];

    foreach ($breakForm as $key => $items) {
      if ($items) {
        foreach ($items as $index => $value) {
          $breaks[$index][$key] = $value;
        }
      }
    }

    \Validator::make([
      'breaks_from' => count($request->break_from),
      'breaks_to' => count($request->break_to),
    ], [
      'breaks_from' => 'same:breaks_to',
    ])->validate();

    return $breaks;
  }
}
