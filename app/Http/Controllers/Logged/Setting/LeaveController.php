<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Models\Absent;
use App\Models\Offer;
use App\Models\Service;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaveController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Schedule" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function index(Request $request)
  {
    $business = $request->user()->getBusiness();
    $absence = $business->absence;

    return view('logged.settings-leaves', compact(
      'absence'
    ));
  }

  public function create(Request $request)
  {
    $this->authorize(Absent::class);
    $business = $request->user()->getBusiness();
    $users = $business->users;
    $absent = new Absent();

    return view('logged.settings-leave-create', compact(
      'absent', 'users', 'business'
    ));
  }

  public function edit(Request $request, Absent $absent)
  {
    $business = $request->user()->getBusiness();
    $this->authorize('edit', $absent);
    $users = $business->users;

    return view('logged.settings-leave-edit', compact(
      'absent', 'users', 'business'
    ));
  }

  public function store(Request $request)
  {
    $this->authorize(Absent::class);
    $business = $request->user()->getBusiness();

    if (\Gate::denies('subscribeToTeam', $business)) {
      $request->request->set('user', $request->user()->id);
    }

    $user = User::where('business_id', $business->id)
      ->where('id', $request->user)
      ->firstOrFail();
    $absentAt = Carbon::parse($request->absent_at);
    $absent = $user->absents()->create(['absent_at' => $absentAt]);

    \Alert::success('Leave is successfully created.')->flash();
    return redirect()->route('settings.leave', ['absent' => $absent->id]);
  }

  public function update(Request $request, Absent $absent)
  {
    $this->authorize('update', $absent);
    $absent->absent_at = Carbon::parse($request->absent_at);
    $absent->save();

    \Alert::success('Leave is successfully updated.')->flash();
    return redirect()->route('settings.leave', ['absent' => $absent->id]);
  }

  public function delete(Absent $absent)
  {
    $this->authorize('delete', $absent);
    $absent->delete();

    \Alert::success('Leave is successfully deleted.')->flash();
    return redirect()->route('settings.leaves');
  }
}
