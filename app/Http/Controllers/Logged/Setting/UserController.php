<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Http\Requests\SettingUserRequest;
use App\Models\Address;
use App\Models\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests\CardRequest;
use App\Http\Requests\ContactInfoRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function view(Request $request)
  {
    $user = $request->user();
    $address = $user->getPrimaryAddress() ?: new Address();
    $primaryContact = $user->getPrimaryContact() ?: new Contact();
    $secondaryContact = $user->getSecondaryContact() ?: new Contact();
    return view('logged.settings-user', compact(
      'user', 'address', 'primaryContact', 'secondaryContact'
    ));
  }

  public function edit(Request $request)
  {
    $user = $request->user();
    $address = $user->getPrimaryAddress() ?: new Address();
    $primaryContact = $user->getPrimaryContact() ?: new Contact();
    $secondaryContact = $user->getSecondaryContact() ?: new Contact();
    $edit = true;
    return view('logged.settings-user', compact(
      'user', 'address', 'edit', 'primaryContact', 'secondaryContact'
    ));
  }

  public function update(SettingUserRequest $request)
  {
    $user = $request->user();
    $fields = [
      'name' => $request->get('name'),
    ];

    if (
      $request->get('password') ||
      $request->get('old_password')
    ) {
      if (!\Hash::check($request->get('old_password'), $user->password)) {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
          return response()->json($errors, 422);
        }

        return redirect()->back()
          ->withInput($request->only($this->username()))
          ->withErrors($errors);
      } else {
        $fields['password'] = \Hash::make($request->get('password'));
      }
    }

    $user->update($fields);

    \Alert::success('User Credentials is successfully updated.')->flash();

    return redirect()->route('settings.user');
  }

  public function updateCard(CardRequest $request)
  {
    $user = $request->user();

    if ($user->hasStripeId()) {
      $user->updateCard($request->stripeToken);
    } else {
      $user->createAsStripeCustomer($request->stripeToken);
    }
    \Alert::success('Card is successfully updated.')->flash();

    return redirect()->route('settings.user');
  }

  public function deleteCard(Request $request)
  {
    $user = $request->user();
    $subscription = $user->subscription(config('stripe-billing.defaults.subscription'));

    if (
      $user->subscribed(config('stripe-billing.defaults.subscription')) &&
      !$subscription->cancelled()
    ) {
      \Alert::add('danger', 'Card is still subscribe to a service.')->flash();
    } else {
      if ($user->hasStripeId()) {
        $user->deleteCards();
        $user->updateCardFromStripe();
      }
      \Alert::success('Card is successfully remove.')->flash();
    }

    return redirect()->route('settings.user');
  }

  public function updateInfo(ContactInfoRequest $request)
  {
    $this->updateContact($request);
    $this->updateAddress($request);

    \Alert::success('Address is successfully updated.')->flash();

    return redirect()->route('settings.user');
  }

  /**
   * Get the login username to be used by the controller.
   *
   * @return string
   */
  public function username()
  {
    return 'email';
  }

  private function updateContact(Request $request)
  {
    $user = $request->user();

    if ($current_contact1 = $user->getPrimaryContact()) {
      $user->updateContact($current_contact1, [
        'number' => $request->primary_contact,
        'is_primary' => true,
      ]);
    } else {
      $user->addContact(['number' => $request->primary_contact]);
    }

    if ($request->secondary_contact) {
      if ($current_contact2 = $user->getSecondaryContact()) {
        $user->updateContact($current_contact2, ['number' => $request->secondary_contact]);
      } else {
        $user->addContact(['number' => $request->secondary_contact]);
      }
    }
  }

  private function updateAddress(Request $request)
  {
    $user = $request->user();
    $address = $request->only([
      'line1', 'line2', 'post_code', 'city_id', 'state_id',
    ]);

    if ($current_address = $user->getPrimaryAddress()) {
      $user->updateAddress($current_address, $address);
    } else {
      $user->addAddress($address);
    }
  }
}
