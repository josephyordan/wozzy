<?php

namespace App\Http\Controllers\Logged\Setting;

use App\Events\Subscribed;
use App\Events\Unsubscribed;
use App\Http\Controllers\Controller;
use App\Models\Plan;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware(function ($request, $next) {
      if (\Auth::user()->hasAnyRole(['admin', 'superadmin'])) {
        \Alert::info('Only Merchant is allowed in "Subscription" page.')->flash();
        return redirect()->back();
      }

      if (!\Auth::user()->hasRole('merchant')) {
        return abort(404);
      }

      return $next($request);
    });

    $this->middleware(function ($request, $next) {
      $business = $request->user()->getBusiness();
      if (\Gate::denies('owner', $business)) {
        return abort(404);
      }

      return $next($request);
    });
  }

  public function plans(Request $request)
  {
    $owner = $request->user()->getBusinessOwner();
    $subscription = $owner->subscription(
      config('stripe-billing.defaults.subscription')
    );
    $plans = Plan::getStripePlans();
    $plans = $plans->whereIn(
      'id', Plan::getPlans()->toArray()
    )->sortBy('amount');

    return view('logged.settings-plans', compact(
      'owner', 'subscription', 'plans'
    ));
  }

  public function edit(Request $request)
  {
    $owner = $request->user()->getBusinessOwner();
    $subscription = $owner->subscription(
      config('stripe-billing.defaults.subscription')
    );
    $plans = Plan::getStripePlans();
    $plans = $plans->whereIn(
      'id', Plan::getPlans()->toArray()
    )->sortBy('amount');

    return view('logged.settings-subscribe', compact(
      'owner', 'subscription', 'plans'
    ));
  }

  public function subscribe(Request $request)
  {
    $user = $request->user();
    $business = $user->getBusiness();

    if (!$user->hasStripeId() || !$user->hasCardOnFile()) {
      $this->validate($request, ['stripeToken' => 'required']);
    }

    $pickedPlan = $this->getDefaultPlan();
    $subscription = $this->getDefaultSubscription();

    if (!$user->subscribedToPlan($pickedPlan, $subscription)) {
      try {
        // check already subscribed and if already subscribed with picked plan
        if ($user->subscribed($subscription) && !$user->subscribedToPlan($pickedPlan, $subscription)) {
          // swap if different plan attempt
          $user->subscription($subscription)->swap($pickedPlan);
        } else {
          // Its new subscription
          // if user has a coupon, create new subscription with coupon applied
          if ($coupon = $request->get('coupon')) {
            $user->newSubscription($subscription, $pickedPlan)
              ->withCoupon($coupon)
              ->create($request->get('stripeToken'), [
                'email' => $user->email,
              ]);
          } else {
            // Create subscription
            $user->newSubscription($subscription, $pickedPlan)->create($request->get('stripeToken'), [
              'email' => $user->email,
              'description' => $user->name,
            ]);
          }
        }

        $business->owner()->associate($user);
        $business->save();

        event(new Subscribed($user));
      } catch (\Exception $e) {
        // Catch any error from Stripe API request and show
        return redirect()->back()->withErrors(['status' => $e->getMessage()]);
      }
    }

    \Alert::success('You are now subscribed')->flash();

    return redirect()->route('settings.subscribe');
  }

  public function subscribeToPlan(Request $request)
  {
    $user = $request->user();
    $business = $user->getBusiness();

    $pickedPlan = $request->get('plan', $this->getDefaultPlan());
    $subscription = $this->getDefaultSubscription();

    if (!$user->subscribedToPlan($pickedPlan, $subscription)) {
      try {
        // check already subscribed and if already subscribed with picked plan
        if ($user->subscribed($subscription) && !$user->subscribedToPlan($pickedPlan, $subscription)) {
          // swap if different plan attempt
          $user->subscription($subscription)->swap($pickedPlan);
        } else {
          // Its new subscription
          // if user has a coupon, create new subscription with coupon applied
          if ($coupon = $request->get('coupon')) {
            $user->newSubscription($subscription, $pickedPlan)
              ->withCoupon($coupon)
              ->create($request->get('stripeToken'), [
                'email' => $user->email,
              ]);
          } else {
            // Create subscription
            $user->newSubscription($subscription, $pickedPlan)->create($request->get('stripeToken'), [
              'email' => $user->email,
              'description' => $user->name,
            ]);
          }
        }

        $business->owner()->associate($user);
        $business->save();

        event(new Subscribed($user));
      } catch (\Exception $e) {
        // Catch any error from Stripe API request and show
        return redirect()->back()->withErrors(['status' => $e->getMessage()]);
      }
    }

    \Alert::success('You are now subscribed')->flash();

    return redirect()->route('settings.subscribe');
  }

  public function cancelSubscription(Request $request)
  {
    $owner = $request->user()->getBusinessOwner();
    try {
      $owner->subscription($this->getDefaultSubscription())->cancel();
    } catch (\Exception $e) {
      \Alert::add('danger', $e->getMessage())->flash();
      return redirect()->back();
    }

    event(new Unsubscribed($owner));
    \Alert::success('Your Subscription has been canceled.')->flash();

    return redirect()->route('settings.subscribe');
  }

  /**
   * Handle Resume subscription
   *
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function resumeSubscription(Request $request)
  {
    $owner = $request->user()->getBusinessOwner();
    try {
      $owner->subscription($this->getDefaultSubscription())->resume();
    } catch (\Exception $e) {
      \Alert::add('danger', $e->getMessage())->flash();
      return redirect()->back();
    }

    \Alert::success('Glad to see you back. Your Subscription has been resumed.')->flash();

    return redirect()->route('settings.subscribe');
  }

  private function getDefaultPlan()
  {
    return config('stripe-billing.defaults.plan');
  }

  private function getDefaultSubscription()
  {
    return config('stripe-billing.defaults.subscription');
  }
}
