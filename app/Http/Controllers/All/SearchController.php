<?php

namespace App\Http\Controllers\All;

use App\Http\Controllers\Controller;
use App\Models\Business;
use Illuminate\Http\Request;

class SearchController extends Controller
{
  public function index(Request $request)
  {
    $business = Business::published();
    $locationTypes = ['city', 'state'];
    $search = collect($request->all())->filter();

    if ($search->has('name')) {
      $business = $business->whereRaw(
        'LOWER(name) LIKE ?', [
          $this->sString($search->get('name')),
        ]
      );
    }

    if ($search->only(['categories', 'categories_id'])->isNotEmpty()) {
      $business = $business->whereHas('categories', function ($query) use ($search) {
        if ($search->has('categories_id')) {
          $query->whereIn('id', (array)$search->get('categories_id'));
        }
        if ($search->has('categories')) {
          $query->whereIn('id', (array)$search->get('categories'));
        }
      });
    }

    if ($search->only(['city', 'state', 'city_id', 'state_id'])->isNotEmpty()) {
      $locationIds = [];
      $locations = collect();

      if ($search->only($locationTypes)->isNotEmpty()) {
        $locationIds = $this->getAddress($search->toArray());
      }

      foreach ($locationTypes as $type) {
        if ($search->has($type)) {
          $locations = $locations->merge(
            \GeoLocation::search($search->get($type))->whereIn('id', array_unique($locationIds))
              ->get()
          );
        }
      }

      foreach ($locationTypes as $type) {
        if ($search->has($type . '_id')) {
          $locations = $locations->merge(
            \GeoLocation::getByIds((array)$search->get($type . '_id'))
          );
        }
      }

      $business = $business->whereHas('addresses', function ($query) use ($locations) {
        $query->whereIn('city_id', $locations->pluck('id')->toArray())
          ->orWhereIn('state_id', $locations->pluck('id')->toArray());
      });
    }

    if ($search->has('all')) {
      $business = Business::published()->where(function ($query) use ($search){
        $query->whereRaw(
          'LOWER(name) LIKE ?', [
            $this->sString($search->get('all')),
          ]
        )->orWhereHas('addresses', function ($query) use ($search) {
          $locations = \GeoLocation::search($search->get('all'))->whereIn('id', array_unique($this->getAddress()))
            ->get();

          $query->whereIn('city_id', $locations->pluck('id')->toArray())
            ->orWhereIn('state_id', $locations->pluck('id')->toArray());
        });
      });
    }

    $businesses = $business->paginate();

    return view('all.search', compact('businesses', 'search'));
  }

  private function sString($search)
  {
    return '%' . mb_strtolower($search) . '%';
  }

  private function getAddress(array $options = [])
  {
    $locationIds = [];
    $options = collect($options);
    $businesses = Business::get();
    foreach ($businesses as $business) {
      if ($address = $business->getPrimaryAddress()) {
        if ($options->has('city') || $options->isEmpty()) {
          $locationIds[] = $address->city_id;
        }
        if ($options->has('state') || $options->isEmpty()) {
          $locationIds[] = $address->state_id;
        }
      }
    }

    return $locationIds;
  }
}
