<?php

namespace App\Http\Controllers\all;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardRequest;
use App\Http\Requests\ContactInfoRequest;
use App\Models\Address;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\Contact;
use App\Models\Offer;
use App\Notifications\AppointmentPaid;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth')->only('postAppointment');
  }

  public function index()
  {
    $businesses = Business::published()->paginate();
    return view('all.businesses', compact('businesses'));
  }

  public function view(Business $business)
  {
    if (\Auth::check()) {
      $user = \Auth::user();
      if (!($user->hasStripeId() && $user->hasCardOnFile())) {
        \Alert::info('Please add card info in User Settings.');
      }
    }
    $this->alert($business);
    $businessMember = false;
    if (!$business->isPublic()) {
      if (\Gate::allows('viewPublic', $business)) {
        return redirect()->route('settings.business');
      }

      return abort(404);
    }

    if (\Auth::check() && \Gate::denies('appointment', $business)) {
      $businessMember = true;
    }

    return view('all.business', compact('business', 'businessMember'));
  }

  public function getSchedules(Request $request, $business)
  {
    $business = Business::findOrFail($business);
    $type = mb_strtolower($request->type);

    if ($type === 'day') {
      return $business->getDaySchedules($request->date, false);
    } elseif ($type === 'week') {
      return $business->getWeekSchedules($request->date);
    } elseif ($type === 'all') {
      return $business->getSchedules($request->date);
    } else {
      $day = $business->getDaySchedules($request->date);
      $week = $business->getWeekSchedules($request->date);
      return compact('week', 'day');
    }
  }

  public function appointment($business)
  {
    $business = Business::published()->findOrFail($business);

    if (!\Auth::guest() && \Gate::denies('appointment', $business)) {
      return abort(404);
    }

    $user = \Auth::user() ?: new User();
    $address = $user->getPrimaryAddress() ?: new Address();
    $primaryContact = $user->getPrimaryContact() ?: new Contact();
    $secondaryContact = $user->getSecondaryContact() ?: new Contact();
    $valid = (
      $primaryContact->isValid() && $address->isValid()
    );

    return view('all.business-appointment', compact(
      'business', 'address', 'primaryContact', 'secondaryContact',
      'user', 'valid'
    ));
  }

  public function postAppointment(Request $request, $business)
  {
    $business = Business::published()->findOrFail($business);

    if (\Gate::denies('appointment', $business)) {
      return abort(403);
    }

    $appointmentAt = Carbon::createFromTimestamp(
      $request->appointment_at, $business->timezone
    );

    $offers = Offer::whereIn('id', $request->offers)
      ->where('business_id', $business->id)
      ->get();

    if ($offers->isEmpty()) {
      \Alert::add('danger', 'Services is required')->flash();
      return redirect()->back();
    }

    if (!$business->isScheduleAvailable($appointmentAt)) {
      \Alert::add('danger', 'Selected date is not available')->flash();
      return redirect()->back();
    }

    $baseTime = clone $appointmentAt;
    foreach ($offers as $offer) {
      if (
      $business->getOfferUnavailable($baseTime, $offer->id)->isNotEmpty()
      ) {
        \Alert::add('danger', 'Selected date is not available')->flash();
        return redirect()->back();
      }
      $baseTime->addMinute($offer->getDurationInMinutes());

      if (!$business->isScheduleAvailable($baseTime)) {
        \Alert::add('danger', 'Selected date is not available')->flash();
        return redirect()->back();
      }
    }

    $validate = $this->validateAppointment($request);

    if ($validate) {
      return $validate;
    }

    $user = $request->user();
    $user->appointment_token = $user->appointment_token ?: strtolower(
      str_random(64)
    );
    $user->save();

    try {
      $user->invoiceFor('Booking Fee', 3000);
    } catch (\Exception $e) {
      \Alert::info($e->getMessage())->flash();
      return redirect()->back();
    }

    $appointment = new Appointment();
    $appointment->message = $request->get('message');
    $appointment->appointment_at = $appointmentAt;
    $appointment->user()->associate($user);
    $appointment->business()->associate($business);
    $appointment->save();

    $appointment->offers()->sync($request->offers);

    $user->notify(new AppointmentPaid($appointment));


    \Alert::success('You now have an appointment in ' . $business->name)->flash();

    return redirect()->route('app.appointment.public', [
      'appointment' => $appointment->id,
      'id' => $user->id,
      'token' => $user->appointment_token,
    ]);
  }

  private function updateContact(Request $request, User $user)
  {
    if ($current_contact = $user->getPrimaryContact()) {
      $user->updateContact($current_contact, [
        'number' => $request->primary_contact,
        'is_primary' => true,
      ]);
    } else {
      $user->addContact(['number' => $request->primary_contact]);
    }

    if ($request->secondary_contact) {
      if ($current_contact2 = $user->getSecondaryContact()) {
        $user->updateContact($current_contact2, ['number' => $request->secondary_contact]);
      } else {
        $user->addContact(['number' => $request->secondary_contact]);
      }
    }
  }

  private function updateAddress(Request $request, User $user)
  {
    $address = $request->only([
      'line1', 'line2', 'post_code', 'city_id', 'state_id',
    ]);

    if ($current_address = $user->getPrimaryAddress()) {
      $user->updateAddress($current_address, $address);
    } else {
      $user->addAddress($address);
    }
  }

  private function validateAppointment(Request $request)
  {
    $user = $request->user();
    $address = $user->getPrimaryAddress() ?: new Address();
    $contact = $user->getPrimaryContact() ?: new Contact();

    $this->validate($request, [
      'appointment_at' => 'required',
      'offers' => 'required|array',
      'offers.*' => 'required|integer',
    ]);

    if (!($address->isValid() && $contact->isValid())) {
      \Alert::add('danger', 'Your address and contact information is invalid')->flash();
      return redirect()->back();
    }

    if (!($user->hasStripeId() && $user->hasCardOnFile())) {
      \Alert::add('danger', 'Valid card information is needed')->flash();
      return redirect()->back();
    }

    return false;
  }

  private function getSchedule(Business $business, Carbon $appointment)
  {
    $dayOfWeek = $appointment->dayOfWeek;
    $schedules = $business->schedules()
      ->where('days', 'like', '%' . $dayOfWeek . '%')
      ->get();

    return $schedules;
  }

  private function isScheduleAvailable(Business $business, Carbon $appointment)
  {
    $schedules = $this->getSchedule($business, $appointment);
    $now = Carbon::now();

    if ($now->gt($appointment)) {
      return false;
    }

    foreach ($schedules as $schedule) {
      $starts = (clone $appointment)->startOfDay();
      $starts->minute = $schedule->getStartsInMinutes();
      $ends = (clone $appointment)->startOfDay();
      $ends->minute = $schedule->getEndsInMinutes();
      if ($appointment->between($starts, $ends)) {
        return true;
      }
    }

    return false;
  }

  private function alert(Business $business)
  {
    if (!$business->isPublish()) {
      \Alert::warning('Business is still not published')->flash();
    } elseif (!$business->isActive()) {
      \Alert::warning('Business is still not active')->flash();
    }
  }
}
