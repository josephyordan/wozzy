<?php

namespace App\Http\Controllers\All;

use App\Http\Requests\AppointmentPublicViewRequest;
use App\Models\Appointment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth')->except('viewPublic');
  }

  public function viewPublic(
    Request $request,
    $appointment
  )
  {
    if (\Auth::guest()) {
      \Validator::make($request->all(), [
        'token' => 'required',
        'id' => 'required',
      ])->validate();

      $user = User::where('appointment_token', $request->token)
        ->where('id', $request->id)
        ->firstOrFail();
    } else {
      $user = $request->user();
    }

    $appointment = Appointment::where('id', $appointment)
      ->where('user_id', $user->id)
      ->firstOrFail();
    $appointments = $user->appointments()
      ->orderBy('appointment_at', 'desc')
      ->paginate(10);

    return view('all.appointment-public', compact('appointment', 'appointments'));
  }

  public function index(Request $request)
  {
    $user = $request->user();
    $appointment = $user->appointments()
      ->orderBy('appointment_at', 'desc')
      ->first();

    if (!$appointment) {
      return abort(403);
    }

    return redirect()
      ->route('app.appointment.public', $appointment->id);
  }
}
