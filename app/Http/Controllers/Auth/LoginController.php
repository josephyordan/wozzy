<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  protected $redirectTo = '/';

  /**
   * Create a new controller instance.
   *
   */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  /**
   * The user has been authenticated.
   *
   * @param \Illuminate\Http\Request $request
   * @param  mixed $user
   * @return mixed
   */
  protected function authenticated(Request $request, $user)
  {
    if (\Gate::forUser($user)->denies('valid')) {
      \Alert::info('Please complete your credentials first.')->flash();
      return redirect()->route('settings.user');
    }

    if ($user->hasRole('merchant') && !$user->isBusinessOwner()) {
      if (
      !$user->getBusinessOwner()->subscribed(
        config('stripe-billing.defaults.subscription'),
        config('stripe-billing.plans.team.id')
      )
      ) {
        $this->guard()->logout();
        $request->session()->invalidate();
        return $this->sendFailedLoginResponse($request);
      }
    }

    if ($user->hasAnyPermission('admin', 'superadmin')) {
      return redirect()->intended(config('backpack.base.route_prefix', 'admin'));
    } else {
      return redirect()->intended($this->redirectTo);
    }
  }
}
