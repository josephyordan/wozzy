<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\CardRequest;
use App\Http\Requests\ContactInfoRequest;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    app()->make(ContactInfoRequest::class);
    app()->make(CardRequest::class);
    return Validator::make($data, [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
    ]);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array $data
   * @return \App\User
   */
  protected function create(array $data)
  {
    $request = collect($data);
    $user = User::create([
      'name' => $data['name'],
      'email' => $data['email'],
      'password' => bcrypt($data['password']),
    ]);

    if ($request->has('merchant')) {
      $user->asMerchant();
    }

    if ($user->hasStripeId()) {
      $user->updateCard($request->get('stripeToken'));
    } else {
      $user->createAsStripeCustomer($request->get('stripeToken'));
    }

    $user->addContact([
      'number' => $request->get('primary_contact'),
      'is_primary' => true,
    ]);

    if ($request->get('secondary_contact')) {
      $user->addContact(['number' => $request->get('secondary_contact')]);
    }

    $address = $request->only([
      'line1', 'line2', 'post_code', 'city_id', 'state_id',
    ]);
    $user->addAddress($address->toArray());

    return $user;
  }
}
