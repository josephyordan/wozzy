<?php

namespace App\Http\Controllers;

use Backpack\PageManager\app\Models\Page;

class PageController extends Controller
{
  /**
   * @param $slug
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function index($slug)
  {
    $page = Page::findBySlug($slug);

    if (!$page) {
      abort(404, 'Please go back to our <a href="' . url('') . '">homepage</a>.');
    }

    $this->data['title'] = $page->title;
    $this->data['page'] = $page->withFakes();

    return view('pages.' . $page->template, $this->data);
  }
}