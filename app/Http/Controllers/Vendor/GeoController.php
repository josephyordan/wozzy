<?php

namespace App\Http\Controllers\Vendor;

use App\Models\GeoLocation;
use Igaster\LaravelCities\GeoController as ParentController;

class GeoController extends ParentController
{
  // Apply Filter from request to json representation of an item or a collection
  // api/call?fields=field1,field2
  protected function applyFilter($geo){
    if (request()->has('fields')){

      if(get_class($geo) == \Illuminate\Database\Eloquent\Collection::class){
        foreach ($geo as $item) {
          $this->applyFilter($item);
        };
        return $geo;
      }

      $fields = request()->input('fields');
      if($fields == 'all'){
        $geo->fliterFields();
      } else {
        $fields = explode(',', $fields);
        array_walk($fields, function(&$item){
          $item = strtolower(trim($item));
        });
        $geo->fliterFields($fields);
      }
    }
    return $geo;
  }

  // [GeoLocation] Get an item by $id
  public function item($id){
    $geo = GeoLocation::find($id);
    $this->applyFilter($geo);
    return \Response::json($geo);
  }

  // [Collection] Get multiple items by ids (comma seperated string or array)
  public function items($ids = []){
    if(is_string($ids)){
      $ids=explode(',', $ids);
    }

    $items = GeoLocation::getByIds($ids);
    return \Response::json($items);
  }

  // [Collection] Get children of $id
  public function children($id){
    return $this->applyFilter(GeoLocation::find($id)->getChildren());
  }

  // [GeoLocation] Get parent of  $id
  public function parent($id){
    $geo = GeoLocation::find($id)->getParent();
    $this->applyFilter($geo);
    return \Response::json($geo);
  }

  // [GeoLocation] Get country by $code (two letter code)
  public function country($code){
    $geo = GeoLocation::getCountry($code);
    $this->applyFilter($geo);
    return \Response::json($geo);
  }

  // [Collection] Get all countries
  public function countries(){
    return $this->applyFilter(GeoLocation::level(GeoLocation::LEVEL_COUNTRY)->get());
  }

  // [Collection] Search for %$name% in 'name' and 'alternames'. Optional filter to children of $parent_id
  public function search($name,$parent_id = null){
    if ($parent_id)
      return $this->applyFilter(GeoLocation::searchNames($name, GeoLocation::find($parent_id)));
    else
      return $this->applyFilter(GeoLocation::searchNames($name));
  }
}
