<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Stripe\Plan;
use Stripe\Stripe;

class TestController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function subscribe()
  {
    // Get all plans from stripe api
    Stripe::setApiKey(User::getStripeKey());
    try {
      dd(Plan::retrieve('subscribe'));
    } catch (\Exception $e) {
    }
    $plans = Plan::all()->data;

    // Check is subscribed
    $is_subscribed = \Auth::user()->subscribed('main');

    // If subscribed get the subscription
    $subscription = \Auth::user()->subscription('main');

    $validator = \Validator::make([], [
      'title' => 'required|unique:posts|max:255',
      'body' => 'required',
    ]);

    if ($validator->fails()) {
      return view('test.subscribe', compact('plans', 'is_subscribed', 'subscription'))->withErrors($validator);
    }

    return view('test.subscribe', compact('plans', 'is_subscribed', 'subscription'))->withErrors($validator);
  }

  public function geolocation()
  {

  }
}
