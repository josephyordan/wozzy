<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    $business = $this->user()->getBusiness();
    return \Gate::allows('owner', $business);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $rules = [
      'name' => 'required|string|max:255',
    ];

    if ($this->isMethod('post')) {
      $rules['email'] = 'required|string|email|max:255|unique:users';
    }

    return $rules;
  }
}
