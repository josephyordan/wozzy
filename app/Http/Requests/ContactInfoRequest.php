<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactInfoRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'state_id' => 'required|exists:geo,id',
      'city_id' => 'required|exists:geo,id',
      'post_code' => 'required',
      'line1' => 'required_without:line2',
      'line2' => 'required_without:line1',
      'primary_contact' => 'required',
    ];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes()
  {
    return [
      'state_id' => 'state',
      'city_id' => 'city',
      'line1' => 'address line 1',
      'line2' => 'address line 2',
    ];
  }
}
