<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingUserRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $rules = [
      'name' => 'required|string|max:255',
    ];

    if (
      $this->request->get('password') ||
      $this->request->get('old_password')
    ) {
      $rules['old_password'] = 'required_with:password|string|min:6';
      $rules['password'] = 'required|string|min:6|confirmed|different:old_password';
    }

    return $rules;
  }
}
