<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingScheduleRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return \Auth::check() && \Auth::user()
        ->hasAnyRole(['merchant']);
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'days' => 'required|array',
      'days.*' => 'integer',
      'starts_at' => 'required',
      'ends_at' => 'required',
      'break_from' => 'array',
//      'break_from.*' => 'date_format:"h:i a"',
      'break_to' => 'array',
//      'break_to.*' => 'date_format:"h:i a"',
    ];
  }
}
