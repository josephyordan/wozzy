<?php

namespace App\Http\Middleware;

use App\Models\Address;
use App\Models\Contact;
use Closure;

class UserValidation
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = null)
  {
    if(\Gate::allows('valid')) {
      return $next($request);
    }
    \Alert::info('Please complete your credentials first.')->flash();

    return redirect()->route('settings.user');
  }
}
