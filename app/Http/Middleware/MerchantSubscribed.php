<?php

namespace App\Http\Middleware;

use App\Models\Address;
use App\Models\Contact;
use Closure;
use Illuminate\Support\Facades\Auth;
use \App\User;

class MerchantSubscribed
{
  /**
   * Handle an incoming request.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Closure $next
   * @param string|null $guard
   *
   * @return mixed
   */
  public function handle($request, Closure $next, $guard = null)
  {
    if (\Auth::guard($guard)->check()) {
      $user = \Auth::guard($guard)->user();
      $user = User::find($user->id);
      if ($user->hasRole('merchant')) {
        $business = $user->getBusiness();
        $owner = $business->getOwner();
        if (
          !$owner ||
          !$owner->subscribed(config('stripe-billing.defaults.subscription'))
        ) {
          if (\Gate::denies('valid', $business)) {
            return redirect()->route('settings.business.step', 1);
          }

          if (!$business->hasSchedule()) {
            return redirect()->route('settings.business.step', 2);
          }

          if ($request->url() !== route('settings.subscribe.plans')) {
            return redirect()->route('settings.subscribe.plans');
          }
        }

        return $next($request);
      }
    }

    return abort(401);
  }
}