<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    'App\Model' => 'App\Policies\ModelPolicy',
    'App\Models\Schedule' => 'App\Policies\SchedulePolicy',
    'App\Models\Business' => 'App\Policies\BusinessPolicy',
    'App\Models\Appointment' => 'App\Policies\AppointmentPolicy',
    'App\Models\Absent' => 'App\Policies\AbsentPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot()
  {
    foreach (get_class_methods(new \App\Policies\UserPolicy) as $method) {
      \Gate::define($method, "\App\Policies\UserPolicy@{$method}");
    }

    $this->registerPolicies();
  }
}
