<?php

namespace App\Providers;

use FFMpeg\FFMpeg;
use Illuminate\Support\ServiceProvider;

class FFMpegServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    //
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->bind('ffmpeg', function () {
      $conf = $this->config();

      if (!$this->app->environment('local')) {
        return $this->create($conf);
      } else {
        try {
          return $this->create($conf);
        } catch (\Exception $e) {
          return null;
        }
      }
    });
  }

  private function config()
  {
    return array(
      'ffmpeg.binaries' => config('ffmpeg.ffmpegBinaries'),
      'ffprobe.binaries' => config('ffmpeg.ffprobeBinaries'),
      'timeout' => config('ffmpeg.ffmpegTimeout'),
      'ffmpeg.threads' => config('ffmpeg.ffmpegThreads'),
    );
  }

  private function create(array $conf)
  {
    return FFMpeg::create($conf);
  }
}
