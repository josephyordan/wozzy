<?php

namespace App\Providers;

use App\Models\Timezonelist;
use Illuminate\Foundation\AliasLoader;
use Jackiedo\Timezonelist\TimezonelistServiceProvider as ServiceProvider;

class TimezonelistServiceProvider extends ServiceProvider
{
  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton('timezonelist', function ($app) {
      return new Timezonelist;
    });

    $this->app->booting(function () {
      $loader = AliasLoader::getInstance();
      $loader->alias('Timezonelist', 'Jackiedo\Timezonelist\Facades\Timezonelist');
    });
  }
}
