<?php

namespace App\Providers;

use App\Http\Middleware\Admin;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
  /**
   * This namespace is applied to your controller routes.
   *
   * In addition, it is set as the URL generator's root namespace.
   *
   * @var string
   */
  protected $namespace = 'App\Http\Controllers';

  /**
   * Define your route model bindings, pattern filters, etc.
   *
   * @return void
   */
  public function boot()
  {
    //

    parent::boot();
  }

  /**
   * Define the routes for the application.
   *
   * @return void
   */
  public function map()
  {
    $this->mapApiRoutes();

    $this->mapAdminRoutes();

    $this->mapWebRoutes();

    $this->mapVendorRoutes();
  }

  /**
   * Define the "api" routes for the application.
   *
   * These routes are typically stateless.
   *
   * @return void
   */
  protected function mapApiRoutes()
  {
    Route::prefix('api')
      ->middleware('api')
      ->namespace($this->namespace)
      ->group(base_path('routes/api.php'));
  }

  /**
   * Define the "web" routes for the application.
   *
   * These routes all receive session state, CSRF protection, etc.
   *
   * @return void
   */
  protected function mapWebRoutes()
  {
    Route::middleware('web')
      ->namespace($this->namespace)
      ->group(base_path('routes/web.php'));

    if (!\App::environment('production')) {
      Route::middleware('web')
        ->namespace($this->namespace)
        ->prefix('test')
        ->group(base_path('routes/test.php'));
    }
  }

  /**
   * Define the "web" routes for the application.
   *
   * These routes all receive session state, CSRF protection, etc.
   *
   * @return void
   */
  protected function mapAdminRoutes()
  {
    Route::aliasMiddleware('admin', Admin::class);

    Route::middleware(['web', 'admin'])
      ->namespace($this->namespace . '\Admin')
      ->prefix(config('backpack.base.route_prefix'))
      ->group(base_path('routes/admin.php'));
  }

  /**
   * Define the "web" routes for the application.
   *
   * These routes overwrite the vendor routes
   *
   * @return void
   */
  protected function mapVendorRoutes()
  {
    $this->loadRoutesFrom(base_path('routes/vendor.php'));
  }
}
