<?php

namespace App\Providers;

use App\Models\Timezonelist;
use Illuminate\Foundation\AliasLoader;
use Jackiedo\Timezonelist\TimezonelistServiceProvider as ServiceProvider;
use Igaster\LaravelCities\GeoServiceProvider as ParentService;

class GeoServiceProvider extends ParentService
{
  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {

    // Register Commands
    if ($this->app->runningInConsole()) {
      $this->commands([
        \Igaster\LaravelCities\commands\seedGeoFile::class,
        \Igaster\LaravelCities\commands\seedJsonFile::class,
      ]);
    }

  }
}
