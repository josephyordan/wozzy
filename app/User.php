<?php

namespace App;

use App\Models\Absent;
use App\Models\Appointment;
use App\Models\Business;
use App\Models\Offer;
use App\Notifications\NewMember;
use App\Traits\HasAddresses;
use App\Traits\HasContacts;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements StaplerableInterface
{
  use Notifiable,
    Billable,
    EloquentTrait,
    CrudTrait,
    HasRoles,
    HasAddresses,
    HasContacts;

  /**
   * User constructor.
   * @param array $attributes
   *
   * Reference:
   * https://github.com/CodeSleeve/stapler/blob/v1.2.0/docs/examples.md
   */
  public function __construct(array $attributes = array())
  {
    $this->hasAttachedFile('picture');

    parent::__construct($attributes);
  }

  /**
   *|--------------------------------------------------------------------------
   *  GLOBAL VARIABLES
   *|--------------------------------------------------------------------------
   */

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password', 'picture',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   *|--------------------------------------------------------------------------
   *  RELATIONS
   *|--------------------------------------------------------------------------
   */

  public function business()
  {
    return $this->belongsTo(Business::class);
  }

  public function offers()
  {
    return $this->belongsToMany(Offer::class);
  }

  public function absents()
  {
    return $this->hasMany(Absent::class);
  }

  public function appointments()
  {
    return $this->hasMany(Appointment::class);
  }

  /**
   *|--------------------------------------------------------------------------
   *  FUNCTIONS
   *|--------------------------------------------------------------------------
   */

  public function asMerchant()
  {
    $role = Role::firstOrCreate(['name' => 'merchant']);
    return $this->assignRole($role->name);
  }

  public function getBusiness()
  {
    $business = $this->business;
    if (!$business) {
      $business = new Business;
      $business->save();
      $this->business()->associate($business);
      $this->save();
    }

    if(!$business->owner) {
      $business->owner()->associate($this);
      $business->save();
    }

    return $business;
  }

  public function getBusinessOwner()
  {
    return $this->getBusiness()->getOwner() ?: $this;
  }

  public function isBusinessOwner()
  {
    return $this->getBusinessOwner()->id === $this->id;
  }

  public function isSubscribedToPlan($plan)
  {
    return $this->subscribed(
      config('stripe-billing.defaults.subscription'),
      $plan
    );
  }

  public function sendNewMemberNotification(Business $business, $password, $token = null)
  {
    $token = $token ?: \Password::broker()->createToken($this);
    $this->notify(new NewMember($business, $password, $token));
  }
}
