<?php

namespace App\Notifications;

use App\Models\Business;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewMember extends Notification
{
  use Queueable;

  /**
   * The password reset token.
   *
   * @var string
   */
  public $token;
  public $business;
  public $password;

  /**
   * Create a notification instance.
   *
   * @param Business $business
   * @param $password
   * @param  string $token
   * @internal param User $user
   * @internal param User $owner
   */
  public function __construct(Business $business, $password, $token)
  {
    $this->business = $business;
    $this->token = $token;
    $this->password = $password;
  }

  /**
   * Get the notification's channels.
   *
   * @param  mixed $notifiable
   * @return array|string
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Build the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    return (new MailMessage)
      ->subject('Invitation from ' . $this->business->name)
      ->line('You are receiving this email because you are invited by '. $this->business->name .' as a member of there team.')
      ->line('Email: ' . $notifiable->email)
      ->line('Temporary Password: ' . $this->password)
      ->action('Change Password', url(config('app.url') . route('password.reset', $this->token, false)))
      ->line('Please change your password immediately.');
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    return [
      //
    ];
  }
}
