<?php

namespace App\Notifications;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AppointmentPaid extends Notification
{
  use Queueable;

  protected $appointment;

  /**
   * Create a new notification instance.
   *
   * @param Appointment $appointment
   */
  public function __construct(Appointment $appointment)
  {
    $this->appointment = $appointment;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    $url = route('app.appointment.public', [
      'appointment' => $this->appointment->id,
      'id' => $notifiable->id,
      'token' => $notifiable->appointment_token,
    ]);
    $appointment = $this->appointment;

    return (new MailMessage)
      ->subject('Appointment Request')
      ->greeting('Good day!')
      ->markdown('mail.appointment.paid', compact('url', 'appointment'));
  }

  /**
   * Get the array representation of the notification.
   *
   * @param  mixed $notifiable
   * @return array
   */
  public function toArray($notifiable)
  {
    return [
      //
    ];
  }
}
