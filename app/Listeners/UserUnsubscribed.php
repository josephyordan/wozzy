<?php

namespace App\Listeners;

use App\Events\Unsubscribed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserUnsubscribed
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  Unsubscribed $event
   * @return void
   */
  public function handle(Unsubscribed $event)
  {
    $business = $event->user->getBusiness();
    $owner = $business->getOwner();
    if (
      $owner &&
      $owner->subscribed(config('stripe-billing.defaults.subscription')) &&
      $owner->subscription(config('stripe-billing.defaults.subscription'))->ends_at
    ) {
      $business->deactivate(
        $owner->subscription(config('stripe-billing.defaults.subscription'))->ends_at
      );
    }
  }
}
