<?php

namespace App\Traits;

use App\Models\Address;

/**
 * Class HasAddresses
 * @package Lecturize\Addresses\Traits
 */
trait HasAddresses
{
  /**
   * Get all addresses for this model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function addresses()
  {
    return $this->morphMany(Address::class, 'addressable');
  }

  /**
   * Check if model has an address.
   *
   * @return bool
   */
  public function hasAddress()
  {
    return (bool)$this->addresses()->count();
  }

  /**
   * Add an address to this model.
   *
   * @param  array $attributes
   * @return mixed
   */
  public function addAddress(array $attributes)
  {
    return $this->addresses()->updateOrCreate($attributes);
  }

  /**
   * Updates the given address.
   *
   * @param  Address  $address
   * @param  array    $attributes
   * @return mixed
   */
  public function updateAddress(Address $address, array $attributes)
  {
    return $address->fill($attributes)->save();
  }

  /**
   * Deletes given address.
   *
   * @param  Address $address
   * @return bool
   */
  public function deleteAddress(Address $address)
  {
    if (!$address)
      return false;

    return $address->delete();
  }

  /**
   * Deletes all the addresses of this model.
   *
   * @return bool
   */
  public function flushAddresses()
  {
    return $this->addresses()->delete();
  }

  /**
   * Get the primary address.
   *
   * @return Address|null
   */
  public function getPrimaryAddress()
  {
    return $this->addresses()->orderBy('is_primary', 'DESC')->first();
  }

  /**
   * Get the billing address.
   *
   * @return Address|null
   */
  public function getBillingAddress()
  {
    return $this->addresses()->orderBy('is_billing', 'DESC')->first();
  }
}