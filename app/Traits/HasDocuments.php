<?php

namespace App\Traits;

use App\Models\Document;

/**
 * Class HasDocuments
 * @package Lecturize\Documents\Traits
 */
trait HasDocuments
{
  /**
   * Get all documents for this model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function documents()
  {
    return $this->morphMany(Document::class, 'documentable');
  }

  /**
   * Check if model has an address.
   *
   * @return bool
   */
  public function hasDocument()
  {
    return (bool)$this->documents()->count();
  }

  /**
   * Add an address to this model.
   *
   * @param  array $attributes
   * @return mixed
   */
  public function addDocument(array $attributes = [])
  {
    return $this->documents()->create($attributes);
  }

  /**
   * Updates the given address.
   *
   * @param  Document  $document
   * @param  array    $attributes
   * @return mixed
   */
  public function updateDocument(Document $document, array $attributes = [])
  {
    return $document->fill($attributes)->save();
  }

  /**
   * Deletes given address.
   *
   * @param  Document $document
   * @return bool
   */
  public function deleteDocument(Document $document)
  {
    if (!$document)
      return false;

    return $document->delete();
  }

  /**
   * Deletes all the documents of this model.
   *
   * @return bool
   */
  public function flushDocuments()
  {
    return $this->documents()->delete();
  }

  /**
   * Get the primary document.
   *
   * @return Document|null
   */
  public function getPrimaryDocument()
  {
    return $this->documents()->orderBy('is_primary', 'DESC')->first();
  }

  /**
   * Get the primary image.
   *
   * @return Document|null
   */
  public function getPrimaryImage()
  {
    return $this->documents()->orderBy('is_primary', 'DESC')
      ->where('file_content_type', 'like', 'image/%')
      ->first();
  }

  /**
   * Get the primary image.
   *
   * @return Document|null
   */
  public function getImages()
  {
    return $this->documents()
      ->where('file_content_type', 'like', 'image/%')
      ->get();
  }

  /**
   * Get the primary image.
   *
   * @return Document|null
   */
  public function getVideos()
  {
    return $this->documents()
      ->where('file_content_type', 'like', 'video/%')
      ->get();
  }
}