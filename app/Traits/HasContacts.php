<?php

namespace App\Traits;

use App\Models\Contact;

/**
 * Class HasContacts
 * @package Lecturize\Contacts\Traits
 */
trait HasContacts
{
  /**
   * Get all addresses for this model.
   *
   * @return \Illuminate\Database\Eloquent\Relations\MorphMany
   */
  public function contacts()
  {
    return $this->morphMany(Contact::class, 'contactable');
  }

  /**
   * Check if model has an address.
   *
   * @return bool
   */
  public function hasContact()
  {
    return (bool)$this->contacts()->count();
  }

  /**
   * Add an address to this model.
   *
   * @param  array $attributes
   * @return mixed
   */
  public function addContact(array $attributes)
  {
    return $this->contacts()->updateOrCreate($attributes);
  }

  /**
   * Updates the given address.
   *
   * @param  Contact  $contact
   * @param  array    $attributes
   * @return mixed
   */
  public function updateContact(Contact $contact, array $attributes)
  {
    return $contact->fill($attributes)->save();
  }

  /**
   * Deletes given address.
   *
   * @param  Contact $contact
   * @return bool
   */
  public function deleteContact(Contact $contact)
  {
    if (!$contact)
      return false;

    return $contact->delete();
  }

  /**
   * Deletes all the addresses of this model.
   *
   * @return bool
   */
  public function flushContacts()
  {
    return $this->contacts()->delete();
  }

  /**
   * Get the primary address.
   *
   * @return Contact|null
   */
  public function getPrimaryContact()
  {
    return $this->contacts()->orderBy('is_primary', 'DESC')->first();
  }

  /**
   * Get the secondary address.
   *
   * @return Contact|null
   */
  public function getSecondaryContact()
  {
    return $this->contacts()->orderBy('is_primary', 'DESC')->offset(1)->first();
  }

  /**
   * Get the billing address.
   *
   * @return Contact|null
   */
  public function getBillingContact()
  {
    return $this->contacts()->orderBy('is_billing', 'DESC')->first();
  }
}