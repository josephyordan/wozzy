<?php

/**
 * This file is part of Entrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Zizaco\Entrust
 */

return [

  'ffmpegBinaries' => env('FFMPEG', '/usr/bin/ffmpeg'),

  'ffprobeBinaries' => env('FFPROBE', '/usr/bin/ffprobe'),

  // The timeout for the underlying process
  'ffmpegTimeout' => env('FFMPEG_TIMEOUT', 3600),

  // The number of threads that FFMpeg should use
  'ffmpegThreads' => env('FFMPEG_THREADS', 12),

];
