<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Defaults
  |--------------------------------------------------------------------------
  |
  | This option controls the default authentication "guard" and password
  | reset options for your application. You may change these defaults
  | as required, but they're a perfect start for most applications.
  |
  */

  'defaults' => [
    'plan' => env('STRIPE_PLAN_PERSONAL'),
    'subscription' => env('STRIPE_SUBSCRIPTION', 'main'),
  ],

  'plans' => [
    'personal' => [
      'name' => 'Personal',
      'id' => env('STRIPE_PLAN_PERSONAL'),
    ],
    'team' => [
      'name' => 'Team',
      'id' => env('STRIPE_PLAN_TEAM'),
    ],
  ],

];
