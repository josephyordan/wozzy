<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$copyFile = function ($filename, $name = null) {
  if (!$name) {
    $name = \Faker\Factory::create()->isbn10;
  }

  $sampleFile = Storage::path($filename);
  $file = Storage::path($name . '.' . File::extension($sampleFile));
  File::copy($sampleFile, $file);
//  sleep(5);

  return $file;
};

$faker = Faker\Factory::create();

Route::get('image', function () use ($copyFile) {
  $user = \App\User::firstOrNew(['email' => 'user1@example.com']);
  $user->name = 'user1';
  $user->password = bcrypt('secret');
  $user->picture = $copyFile('image.jpg');

  $user->save();
  dump($user->picture->url());
  dump($user->picture->contentType());
  dd($user->picture);
});

Route::get('video', function () use ($copyFile) {
  $user = \App\User::firstOrNew(['email' => 'admin@example.com']);
  $user->name = 'admin';
  $user->password = bcrypt('secret');
  $user->picture = $copyFile('video.m4v');

  $user->save();
  dump($user->picture->url());
  dump($user->picture->contentType());
  dd($user->picture);
});

Route::get('business', function () use ($copyFile) {
  dd(\App\Models\Business::published()->get());
  $business = \App\Models\Business::first();
  $owner = $business->getOwner();
  dd($owner->subscribed(config('stripe-billing.defaults.subscription')));
  dd($owner->subscription(config('stripe-billing.defaults.subscription'))->ends_at);
//  $business->picture = $copyFile('image.jpg');
//  $business->save();
  $business->picture = STAPLER_NULL;
  $business->save();
  dd($business->picture_file_name);
  dd($business->getPrimaryImage());
  $document = $business->addDocument();
  $document->file = $copyFile('image.jpg');
  $document->save();
  dd($business->hasDocument());
});

Route::get('contact', function () use ($faker) {
  $user = \App\User::firstOrNew(['email' => 'user1@example.com']);
  $user->addContact(['number' => $faker->phoneNumber]);
  dd($user);
});

Route::get('subscribe', 'TestController@subscribe');

Route::get('subscribe/redirect', function () {
  Alert::success('You have successfully logged in')->flash();
  return redirect('test/subscribe');
});

Route::get('address/{business?}', function ($business = null) {
  $faker = \Faker\Factory::create();
  if ($business) {
    $user = \App\Models\Business::first();
  } else {
    $user = \App\User::first();
  }
  $country = \GeoLocation::getCountry('US');
  $state = $country->getChildren()->first();
  $city = $state->getChildren()->first();
  $address = [
    'line1' => $faker->streetAddress,
    'state_id' => $state->id,
    'city_id' => $city->id,
    'country_id' => $country->id,
    'post_code' => $faker->postcode,
  ];

  if ($current_address = $user->getPrimaryAddress()) {
    $user->updateAddress($current_address, $address);
  } else {
    $user->addAddress($address);
  }
  dd($user->getPrimaryAddress()->toArray());
});

Route::get('subscribe', function () {
  $user = \App\User::find(25);
  Event::fire(new \App\Events\Subscribed($user));
});

Route::get('category', function () {
  $business = \App\Models\Business::first();
  $category = \App\Models\Category2::firstOrCreate(['name' => 'Others']);

  $business->categories()->detach();
  $business->categories()->save($category);
  dd($business->categories);
});

Route::get('deletecard', function () {
  $user = Auth::user();
  $user->deleteCards();
});


Route::get('/home', function () {
  $link = new \App\Models\Link;
  $link->forceFill(['type' => 's']);
//  $link->type = 'twitter';
  dd($link);
  dd($link->type);
  dd(Gate::allows('superadmin'));
});