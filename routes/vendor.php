<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
  'prefix' => config('backpack.base.route_prefix', 'admin'),
  'middleware' => ['web', 'admin'],
], function () {

  Route::group(['namespace' => '\Backpack\PermissionManager\app\Http\Controllers'], function () {
    Route::group([
      'middleware' => ['can:superadmin'],
    ], function () {
      CRUD::resource('permission', 'PermissionCrudController');
      CRUD::resource('role', 'RoleCrudController');
    });
  });

  Route::group(['namespace' => $this->namespace . '\Admin'], function () {
    CRUD::resource('user', 'UserCrudController');

    Route::group([
      'middleware' => ['can:superadmin'],
    ], function () {
      CRUD::resource('setting', 'SettingCrudController');
    });
  });

});