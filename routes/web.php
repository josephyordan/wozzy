<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['namespace' => 'Auth'], function () {
  Route::get('logout', 'LoginController@logout')->name('logout2');
});

Route::get('/', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Logged', 'middleware' => 'auth'], function () {
  Route::get('settings', function () {
    return redirect()->route('settings.business');
  })->name('settings');

  Route::group(['namespace' => 'Setting', 'prefix' => 'settings'], function () {
    Route::get('user', 'UserController@view')->name('settings.user');
    Route::post('user/user', 'UserController@update')->name('settings.user.user');
    Route::get('user/edit', 'UserController@edit')->name('settings.user.edit');
    Route::post('user/card', 'UserController@updateCard')->name('settings.user.card');
    Route::delete('user/card', 'UserController@deleteCard');
    Route::post('user/info', 'UserController@updateInfo')->name('settings.user.info');

    Route::group(['middleware' => 'user.validate'], function () {
      Route::get('step/{step}', 'StepController@stepEdit')->name('settings.business.step');
      Route::post('step/{step}', 'StepController@stepStore')->name('settings.business.step.store');

      Route::post('subscribe/plan', 'SubscribeController@subscribeToPlan')->name('settings.subscribe.plan');
      Route::post('subscribe/resume', 'SubscribeController@resumeSubscription')->name('settings.subscribe.resume');
      Route::get('subscribe/plans', 'SubscribeController@plans')->name('settings.subscribe.plans');

      Route::group(['middleware' => 'merchant'], function () {
        Route::get('subscribe', 'SubscribeController@edit')->name('settings.subscribe');
        Route::post('subscribe/cancel', 'SubscribeController@cancelSubscription')->name('settings.subscribe.cancel');

        Route::get('portfolio', 'PortfolioController@edit')->name('settings.portfolio');
        Route::post('portfolio/upload', 'PortfolioController@fileUpload')->name('settings.portfolio.upload');
        Route::post('portfolio/picture', 'PortfolioController@pictureUpload')->name('settings.portfolio.upload.picture');
        Route::delete('portfolio/upload', 'PortfolioController@delete');
        Route::delete('portfolio/picture', 'PortfolioController@deleteProfile');

        Route::get('schedules', 'ScheduleController@index')->name('settings.schedules');
        Route::get('schedule/{schedule}', 'ScheduleController@edit')->name('settings.schedule');
        Route::get('schedule', 'ScheduleController@create')->name('settings.schedule.create');
        Route::post('schedule', 'ScheduleController@store')->name('settings.schedule.store');
        Route::patch('schedule/{schedule}', 'ScheduleController@update')->name('settings.schedule.update');
        Route::delete('schedule/{schedule}', 'ScheduleController@delete')->name('settings.schedule.delete');

        Route::get('business', 'BusinessController@view')->name('settings.business');
        Route::get('business/edit', 'BusinessController@edit')->name('settings.business.edit');
        Route::post('business/edit', 'BusinessController@update')->name('settings.business.update');
        Route::post('business/info', 'BusinessController@updateInfo')->name('settings.business.info');
        Route::post('business/publish', 'BusinessController@updatePublish')->name('settings.business.publish');

        Route::get('appointments', 'AppointmentController@index')->name('settings.appointments');
        Route::get('appointment/{appointment}', 'AppointmentController@view')->name('settings.appointment');
        Route::post('appointment/{appointment}', 'AppointmentController@markDone')->name('settings.appointment.done');

        Route::get('services', 'ServiceController@index')->name('settings.services');
        Route::get('service/create', 'ServiceController@create')->name('settings.service.create');
        Route::get('service/{offer}', 'ServiceController@edit')->name('settings.service');
        Route::patch('service/{offer}', 'ServiceController@update')->name('settings.service.update');
        Route::post('services', 'ServiceController@store')->name('settings.service.store');
        Route::delete('service/{offer}', 'ServiceController@delete')->name('settings.service.delete');

        Route::get('leaves', 'LeaveController@index')->name('settings.leaves');
        Route::get('leaves/{absent}', 'LeaveController@edit')->name('settings.leave');
        Route::get('leave/create', 'LeaveController@create')->name('settings.leave.create');
        Route::patch('leave/{absent}', 'LeaveController@update')->name('settings.leave.update');
        Route::post('leaves', 'LeaveController@store')->name('settings.leave.store');
        Route::delete('leave/{absent}', 'LeaveController@delete')->name('settings.leave.delete');

        Route::get('members', 'MemberController@index')->name('settings.members');
        Route::get('member/{user}', 'MemberController@edit')->name('settings.member');
        Route::get('member', 'MemberController@create')->name('settings.member.create');
        Route::post('member', 'MemberController@store')->name('settings.member.store');
        Route::patch('member/{user}', 'MemberController@update')->name('settings.member.update');
        Route::delete('member/{user}', 'MemberController@delete')->name('settings.member.delete');
      });
    });
    });
});


Route::group(['namespace' => 'All'], function () {
  Route::get('search', 'SearchController@index')->name('app.search');

  Route::get('businesses', 'BusinessController@index')->name('app.businesses');
  Route::get('business/{business}', 'BusinessController@view')->name('app.business');
  Route::get('business/{business}/appointment', 'BusinessController@appointment')->name('app.business.appointment');
  Route::post('business/{business}', 'BusinessController@postAppointment')->name('app.business.appointment.store');

  Route::get('appointment/{appointment}', 'AppointmentController@viewPublic')->name('app.appointment.public');
  Route::get('appointments', 'AppointmentController@index')->name('app.appointments');
});