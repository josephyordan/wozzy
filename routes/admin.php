<?php

// Backpack\CRUD: Define the resources for the entities you want to CRUD.
CRUD::resource('business', 'BusinessCrudController');
CRUD::resource('category', 'CategoryCrudController');
CRUD::resource('service', 'ServiceCrudController');